# Blaster 3D

First-person shooter game powered by raycasting engine.

Click on the image to view gameplay on Youtube:

[![Blaster 3D gameplay video](https://i.imgur.com/iMSOBpv.png)](https://www.youtube.com/watch?v=ak4KaTlBy4Y "Blaster 3D gameplay video")

Game implemented in C++, uses SDL2 (windowing, input handling and screen rendering), SDL2_ttf (font rendering) and SDL2_mixer (sound and music) libraries.