/*
	Author: Jan Tatarynowicz
*/

#include "Projectile.h"

Projectile::Projectile(	Map *map,
						enum PROJECTILE_TYPES type,
						float x,
						float y,
						float dx,
						float dy,
						float angle,
						float width,
						float height,
						Uint32 lifetime,
						int damage )
: Thing(	THING_TYPE_PROJECTILE,
			ENEMY_TYPE_NONE,
			PICKUP_NONE,
			map,
			x,
			y,
			dx,
			dy,
			0.0f,	// speed
			angle,
			width,
			height )
{
	this->projectileType = type;
	
	this->damage = damage;
	
	this->z = 0.25f;
	
	this->lifetime = lifetime;
	this->actionTime = SDL_GetTicks();
}

int Projectile::CheckCollision()
{
	int collides = 0;
	
	const int PROJECTILE_COLLISION_DISTANCE = 0.01f;
	
	float rx = this->x + PROJECTILE_COLLISION_DISTANCE;
	float lx = this->x - PROJECTILE_COLLISION_DISTANCE;
	float uy = this->y - PROJECTILE_COLLISION_DISTANCE;
	float dy = this->y + PROJECTILE_COLLISION_DISTANCE;
	
	if( this->dy < 0.0f )
	{
		if
		(		this->map->Blocks( lx / GRID_SIZE, ( uy + this->dy ) / GRID_SIZE )
			||	this->map->Blocks( rx / GRID_SIZE, ( uy + this->dy ) / GRID_SIZE )
		)
		{
			this->dy = 0.0f;
			collides = 1;
		}
	}
	else if( this->dy > 0.0f )
	{
		if
		(		this->map->Blocks( lx / GRID_SIZE, ( dy + this->dy ) / GRID_SIZE )
			||	this->map->Blocks( rx / GRID_SIZE, ( dy + this->dy ) / GRID_SIZE )
		)
		{
			this->dy = 0.0f;
			collides = 1;
		}
	}
	
	if( this->dx < 0.0f )
	{
		if
		(		this->map->Blocks( ( lx + this->dx ) / GRID_SIZE, uy / GRID_SIZE )
			||	this->map->Blocks( ( lx + this->dx ) / GRID_SIZE, dy / GRID_SIZE )
		)
		{
			this->dx = 0.0f;
			collides = 1;
		}
	}
	else if( this->dx > 0.0f )
	{
		if
		(		this->map->Blocks( ( rx + this->dx ) / GRID_SIZE, uy / GRID_SIZE )
			||	this->map->Blocks( ( rx + this->dx ) / GRID_SIZE, dy / GRID_SIZE )
		)
		{
			this->dx = 0.0f;
			collides = 1;
		}
	}
	
	return collides;
}

void Projectile::Update( Player *player, Thing *observer, std::vector< Thing* > *things )
{
	Thing::Update( player, observer, things );
	
	if( CheckCollision() )
	{
		this->dispose = true;
		return;
	}
	
	this->x += dx;
	this->y += dy;
	
	if( SDL_GetTicks() - this->actionTime >= this->lifetime )
		this->dispose = true;
}