/*
	Author: Jan Tatarynowicz
*/

#include "LookupTables.h"

float LookupTables::table_atan[ NUM_ATAN_LOOKUP_SAMPLES ];
float LookupTables::table_sin[ NUM_LOOKUP_SAMPLES ];
float LookupTables::table_cos[ NUM_LOOKUP_SAMPLES ];

int LookupTables::LookupIndex( float x )
{
	while( x < 0 )
		x += FULL_ANGLE;
	while( x >= FULL_ANGLE )
		x -= FULL_ANGLE;
	
	int i = ( int ) ( x / FULL_ANGLE * ( float ) NUM_LOOKUP_SAMPLES );
	
	if( i <= 0 || i >= NUM_LOOKUP_SAMPLES )
		return 0;
	
	return i;
}

int LookupTables::AtanLookupIndex( float x )
{
	int i = ( int ) ( x / MAX_ATAN_ARG * ( float ) NUM_ATAN_LOOKUP_SAMPLES );
	
	if( i <= 0 || i >= NUM_ATAN_LOOKUP_SAMPLES )
		return 0;
	
	return i;
}

void LookupTables::InitLookupTables()
{
	float angle = 0.0f;
	float atanarg = 0.0f;
	for( int i = 0; i < NUM_LOOKUP_SAMPLES; i++ )
	{
		table_sin[ i ] = sin( angle );
		table_cos[ i ] = cos( angle );
		angle += ANGLE_STEP;
	}
	
	for( int i = 0; i < NUM_ATAN_LOOKUP_SAMPLES; i++ )
	{
		table_atan[ i ] = atan( atanarg );
		atanarg += ATAN_ARG_STEP;
	}
}

float LookupTables::fastsin( float a )
{
	int i = LookupIndex( a );
	
	return table_sin[ i ];
}

float LookupTables::fastcos( float a )
{
	int i = LookupIndex( a );
	
	if( i < 0 || i >= NUM_LOOKUP_SAMPLES )
		exit( -1 );
	
	return table_cos[ i ];
}

float LookupTables::fastatan( float a )
{
	if( a < 0.0f )
	{
		a = -a;
		
		int i = AtanLookupIndex( a );
		
		return - table_atan[ i ];
	}
	else
	{
		int i = AtanLookupIndex( a );
		
		return table_atan[ i ];
	}
}

float LookupTables::fastatan2( float y, float x )
{
	if( x == 0 )
	{
		if( y > 0 )
			return PI / 2.0f;
		if( y < 0 )
			return - PI / 2.0f;
		// Undefined
		else
			return 0.0f;
	}
	
	float r = fastatan( y / x );
	
	if( x > 0 )
		return r;
	else
	{
		if( y >= 0 )
			return r + PI;
		else
			return r - PI;
	}
}