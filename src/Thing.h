/*
	Defines things' attributes. Things are players, enemies, plants, pickups, projectiles.
	Every visible object in the level map, except for level geometry - walls, floor and ceilings.
	Author: Jan Tatarynowicz
*/

#ifndef _THING_H_
#define _THING_H_

#include "Def.h"
#include "Weapon.h"

class Thing
{
	public:
		static const unsigned int RESPAWN_TIME = 5000; // millis
		static const unsigned int ACTION_TIME_HIDE_WEAPON = 250;
		static const unsigned int ACTION_TIME_DRAW_WEAPON = 250;
		
		static const int MAX_HEALTH = 100;
		
		enum THING_TYPES
		{
			THING_TYPE_PLAYER,
			THING_TYPE_PLANT,
			THING_TYPE_ENEMY,
			THING_TYPE_PICKUP,
			THING_TYPE_PROJECTILE,
			THING_TYPE_SPAWNER,
			
			NUM_THING_TYPES
		};
		
		enum ENEMY_TYPES
		{
			ENEMY_TYPE_NONE,
			
			ENEMY_TYPE_SMARINE,
			
			NUM_ENEMY_TYPES
		};
		
		enum PICKUPS
		{
			PICKUP_NONE,
			BULLETBOX,
			SHELLBOX,
			CELLS,
			SMALLMEDKIT,
			LARGEMEDKIT,
			MACHINEGUN,
			SHOTGUN,
			PLASMAGUN,
			
			NUM_PICKUPS
		};
		
		static constexpr std::pair< float, float > PICKUP_SIZES[ NUM_PICKUPS ] =
		{
			{ 0.0f, 0.0f },
			{ 0.3f, 0.2f },
			{ 0.4f, 0.15f },
			{ 0.5f, 0.2f },
			{ 0.15f, 0.15f },
			{ 0.3f, 0.2f },
			{ 0.5f, 0.25f },
			{ 0.6f, 0.15f },
			{ 0.6f, 0.2f }
		};
		
		enum ACTIONS
		{
			ACTION_NONE,
			ACTION_IDLE,
			ACTION_WALK,
			ACTION_DIE,
			ACTION_SHOOT,
			ACTION_HIDE_WEAPON,
			ACTION_DRAW_WEAPON,
			ACTION_RESPAWN,
			
			NUM_ACTIONS
		};
		
		const int ACTION_LENGTHS[ NUM_ACTIONS ] =
		{
			0,
			0,
			0,
			150,
			150,
			250,
			250,
			5000
		};
		
		Thing(	enum THING_TYPES type,
				enum ENEMY_TYPES enemyType,
				enum PICKUPS pickup,
				Map *map,
				float x,
				float y,
				float dx = 0.0f,
				float dy = 0.0f,
				float speed = 0.0f,
				float angle = 0.0f,
				float width = 0.0f,
				float height = 0.0f );
        virtual ~Thing();
		
		/*
			Updates thing's position, velocity, angle, actions.
			Gets:
				- Pointer to player,
				- Pointer to current observer,
				- Pointer to things a thing can interact with (shoot to).
		*/
		virtual void Update( Player *player, Thing *observer, std::vector< Thing* > *things );
		
		/*
			Tells whether there is an intersection between thing and cast ray.
			Returns:
				- True if there is an intersection,
				- False otherwise.
		*/
		bool Intersects( Ray *ray );
		
		/*
			Tells whether there is a collision between two things.
			Returns:
				- True if there is a collision,
				- False otherwise.
		*/
		bool Collides( Thing *thing );
		
		/*
			Checks for collision between thing and level geometry (walls).
			Returns:
				- 1 if there is a collision,
				- 0 otherwise.
		*/
		int CheckCollision();
		
		void DealDamage( int damage );
		
		/*
			Respawns things - sets it's new position and may reset some attributes (i.e. weapons).
			Gets:
				- New position X coordinate,
				- New position Y coordinate,
				- New looking/moving angle.
		*/
		virtual void Respawn( float x, float y, float angle ){};
		
		/*
			Adds to the health pool.
			Gets:
				- A value to add to the health pool,
				- Flag telling whether max health limit may be exceeded.
		*/
		void Heal( int hp, bool super = false )
		{
			this->health += hp;
			if( !super && this->health > MAX_HEALTH )
				this->health = MAX_HEALTH;
			
			if( this->health > 2 * MAX_HEALTH )
				this->health = 2 * MAX_HEALTH;
		}
		
		/*
			Returns damage value this thing may deal (i.e. projectiles).
		*/
		int GetDamage() { return this->damage; }
		
		int GetHealth() { return this->health; }
		
		/*
			Returns kill count.
		*/
		int GetFrags() { return this->frags; }
		
		enum THING_TYPES GetType() { return this->type; }
		enum ENEMY_TYPES GetEnemyType() { return this->enemyType; }
		enum PICKUPS WhichPickup() { return this->pickup; }
		
		std::tuple< float, float > GetPosition()
		{ return std::make_tuple( this->x, this->y ); }
		
		std::tuple< float, float > GetVelocity()
		{ return std::make_tuple( this->dx, this->dy ); }
		
		void SetPosition( float x, float y ) { this->x = x, this->y = y; }
		
		float GetZ() { return this->z; }
		float GetSpeed() { return this->speed; }
		float GetAngle() { return this->angle; }
		float GetMoveAngle() { return this->moveAngle; }
		float GetWidth() { return this->width; }
		float GetHeight() { return this->height; }
		float GetDistance() { return this->pdistance; }
		float GetCameraDistance() { return this->cdistance; }
		float GetOpDistance() { return this->opdistance; }
		
		void SetAngle( float angle ) { this->angle = angle; }
		
		void SetOpDistance( float x, float y ) { this->opdistance = sqrt( ( this->x - x ) * ( this->x - x ) + ( this->y - y ) * ( this->y - y ) ); }
		
		/*
			Handles things that can be picked up (ammo, medkits, weapons).
		*/
		void PickUp();
		
		void GiveFrag() { this->frags = this->frags + 1; }
		bool Dispose() { return this->dispose; }
		void SetHit() { this->dispose = true; }
		
		float GetSpriteWidth() { return this->spriteWidth; }
		float GetSpriteHeight() { return this->spriteHeight; }
		
		float GetRelativeAngle() { return this->relativeAngle; }
		
		enum ACTIONS GetAction() { return this->action; }
		
		Uint32 GetActionTime() { return this->actionTime; }
		
		void SetShot( bool shot = true ) { this->shot = shot; }
		
		void GiveAmmo( enum Weapon::WEAPONS weapon, int ammo );
		virtual void GiveWeapon( enum Weapon::WEAPONS weapon );
		void ResetWeapons();
		
		Weapon *GetWeapon() { return this->weapons[ this->weapon ]; }
		
		bool MaxAmmo( enum Weapon::WEAPONS weapon ) { return this->weapons[ weapon ]->MaxAmmo(); }
		bool HasShot() { return this->shot; }
		virtual bool IsMoving() { return !( this->dx == 0.0f && this->dy == 0.0f ); }
		
		bool IsDead() { return this->health <= 0; }
		
		Thing *GetOriginator() { return this->originator; }
		
		void SetOriginator( Thing *originator ) { this->originator = originator; }
		
		void SetName( const char *name ) { this->name = name; }
		const char *GetName() { return this->name; }
		
		void SwitchDestructibility() { this->indestructible = !this->indestructible; }
	
	protected:
		/*
			Handles things' shooting.
		*/
		void Shoot();
		
		void HideWeapon( enum Weapon::WEAPONS weapon );
		void DrawWeapon();
		
		enum THING_TYPES type;
		enum ENEMY_TYPES enemyType;
		enum PICKUPS pickup;
		enum ACTIONS action;
		
		// The moment current action began
		Uint32 actionTime;
		
		Map *map;
		
		const char *name = NULL;
		
		// Position, z coordinate for rendering purposes only
		float x, y, z;
		// Velocity
		float dx, dy;
		float speed;
		float angle;
		float moveAngle;
		float width, height;
		float spriteWidth, spriteHeight;
		
		bool shot = false;
		
		enum Weapon::WEAPONS weapon;
		// Weapon to be switched to
		enum Weapon::WEAPONS nextWeapon;
		
		Weapon *weapons[ Weapon::WEAPONS::NUM_WEAPONS ];
		
		// Distance to Player
		float pdistance;
		// Distance to observer (camera)
		float cdistance;
		// Operational distance to a given thing - may change several times during a single update tick
		// thus should be used as soon as it is set (used mainly for sorting)
		float opdistance;
		// Angle relative to observer
		float relativeAngle;
		
		// Describes how far a thing is from death - drop it to zero and it's dead
		int health;
		
		bool indestructible = false;
		
		// Projectile things deal damage
		int damage;
		
		// Kill count
		int frags = 0;
		
		bool dispose;
		
		Uint32 deathTime;
		
		// A thing which "created" this thing (i.e. plasma projectiles are created by player or enemies)
		// Used when one's to tell who killed who
		Thing *originator = NULL;
};

#endif // _THING_H_
