/*
	Author: Jan Tatarynowicz
*/

#include "Map.h"

void Map::InitWaypoints( std::vector< std::pair< int, int > > *wpcoords )
{
	this->waypoints = new Waypoint[ numwaypoints ];
	
	for( int i = 0; i < this->numwaypoints; i++ )
	{
		this->waypoints[ i ].paths = new std::vector< std::pair< int, int > >*[ this->numwaypoints ];
		
		std::pair< int, int > pos = ( *wpcoords )[ i ];
		
		this->waypoints[ i ].x = pos.first;
		this->waypoints[ i ].y = pos.second;
	}
	
	for( int i = 0; i < this->numwaypoints; i++ )
	{
		int x1 = this->waypoints[ i ].x, y1 = this->waypoints[ i ].y;
		
		for( int j = 0; j < this->numwaypoints; j++ )
		{
			if( i == j )
			{
				this->waypoints[ i ].paths[ j ] = NULL;
				continue;
			}
			
			int x2 = this->waypoints[ j ].x, y2 = this->waypoints[ j ].y;
			
			this->waypoints[ i ].paths[ j ] = GetPath( x1, y1, x2, y2 );
		}
	}
}

int Map::Inspect( int x, int y )
{
	if( x < 0 || x >= this->width || y < 0 || y >= this->height )
		return 1;
	
	return this->data[ y * this->width + x ];
}

int Map::Blocks( int x, int y )
{
	if( x < 0 || x >= this->width || y < 0 || y >= this->height )
		return 1;
	
	return this->blocks[ y * this->width + x ];
}

void Map::SetBlocks( int x, int y )
{
	if( x < 0 || x >= this->width || y < 0 || y >= this->height )
		return;
	
	this->blocks[ y * this->width + x ] = 1;
}

std::vector< std::pair< int, int > > * Map::GetPath( int x1, int y1, int x2, int y2 )
{
	// Initialize nodes and closed list flags
	int numnodes = this->width * this->height;
	for( int i = 0; i < numnodes; i++ )
	{
		this->nodes[ i ].px = -1;
		this->nodes[ i ].py = -1;
		this->nodes[ i ].c = MAX_COST;
		this->nodes[ i ].m = MAX_COST;
		this->nodes[ i ].h = MAX_COST;
		this->nodes[ i ].closed = false;
		this->nodes[ i ].open = false;
	}
	
	// Initialize open list
	auto cmp = []( AStarNode *n1, AStarNode *n2 ) { return n1->c <= n2->c; };
	std::set< AStarNode*, decltype( cmp ) > open( cmp );
	
	// Starting node goes to open list
	int index = y1 * this->width + x1;
	this->nodes[ index ].c = 0;
	this->nodes[ index ].m = 0;
	this->nodes[ index ].h = 0;
	this->nodes[ index ].open = true;
	
	open.insert( &( this->nodes[ index ] ) );
	
	while( !open.empty() )
	{
		// Get node with lowest c cost and mark it closed
		AStarNode *node = *open.begin();
		open.erase( open.begin() );
		node->closed = true;
		node->open = false;
		
		// Path found
		if( node->x == x2 && node->y == y2 )
		{
			break;
		}
		
		// Node's neighbours' coordinates
		std::vector< std::pair< int, int > > childCoords =
		{ 
			{ node->x - 1, node->y - 1 },
			{ node->x, node->y - 1 },
			{ node->x + 1, node->y - 1 },
			{ node->x - 1, node->y },
			{ node->x + 1, node->y },
			{ node->x - 1, node->y + 1 },
			{ node->x, node->y + 1 },
			{ node->x + 1, node->y + 1 }
		};
		
		// Check each neighbour (child)
		for( int i = 0; i < ( int ) childCoords.size(); i++ )
		{
			auto coords = childCoords[ i ];
			int cx = coords.first, cy = coords.second;
			int cindex = cy * this->width + cx;
			AStarNode *cnode = &( this->nodes[ cindex ] );
			
			if( cnode->closed || Blocks( cx, cy ) )
				continue;
			
			// Avoid cuting through wall corners
			if( 		( i == 0 && ( Blocks( node->x - 1, node->y ) || Blocks( node->x, node->y - 1 ) ) )
					||	( i == 2 && ( Blocks( node->x + 1, node->y ) || Blocks( node->x, node->y - 1 ) ) )
					||	( i == 5 && ( Blocks( node->x - 1, node->y ) || Blocks( node->x, node->y + 1 ) ) )
					||	( i == 7 && ( Blocks( node->x + 1, node->y ) || Blocks( node->x, node->y + 1 ) ) ) )
					continue;
			
			int movecost = ( cx == node->x || cy == node->y ) ? VH_COST : DIAG_COST;
			int m = node->m + movecost;
			int h = GetHeuristicCost( cx, cy, x2, y2 );
			int c = m + h;
			
			// If not in open list - add it
			// Else if new path to this node is better - update
			if( cnode->open )
			{
				if( c >= cnode->c )
				{
					cnode->m = m;
					cnode->h = h;
					cnode->c = c;
					
					continue;
				}
				
				open.erase( cnode );
			}
			
			cnode->px = node->x;
			cnode->py = node->y;
			cnode->m = m;
			cnode->h = h;
			cnode->c = c;
			
			cnode->open = true;
			
			open.insert( cnode );
		}
	}
	
	// Construct path
	std::stack< std::pair< int, int > > st;
	int x = x2, y = y2;
	
	while( x != x1 || y != y1 )
	{
		int index = y * this->width + x;
		st.push( std::make_pair( x, y ) );
		x = this->nodes[ index ].px, y = this->nodes[ index ].py;
	}
	
	std::vector< std::pair< int, int > > *path = new std::vector< std::pair< int, int > >();
	
	while( !st.empty() )
	{
		path->push_back( st.top() );
		st.pop();
	}
	
	return path;
}

std::vector< std::pair< int, int > > * Map::GetRandomWaypointPath( int x, int y )
{
	int from = -1;
	for( int i = 0; i < this->numwaypoints; i++ )
	{
		Waypoint wp = this->waypoints[ i ];
		
		if( x == wp.x && y == wp.y )
			from = i;
	}
	
	if( from == -1 )
		return NULL;
	
	int roll = rand() % this->numwaypoints;
	
	if( from == roll )
	{
		if( roll < this->numwaypoints - 1 )
			roll++;
		else if( roll > 0 )
			roll--;
	}
	
	return this->waypoints[ from ].paths[ roll ];
}

std::vector< std::pair< int, int > > * Map::GetPathToRandomWaypoint( int x, int y )
{
	int roll = rand() % this->numwaypoints;
	
	Waypoint wp = this->waypoints[ roll ];
	
	return GetPath( x, y, wp.x, wp.y );
}

int Map::GetHeuristicCost( int x1, int y1, int x2, int y2 )
{
	//return 10 * ( abs( x1 - x2 ) + abs( y1 - y2 ) );
	return 10 * sqrt( ( x1 - x2 ) * ( x1 - x2 ) + ( y1 - y2 ) * ( y1 - y2 ) );
}

Map::~Map()
{
	delete [] this->blocks;
	delete [] this->data;
	delete [] this->nodes;
	
	for( int i = 0; i < this->numwaypoints; i++ )
	{
		for( int j = 0; j < this->numwaypoints; j++ )
		{
			if( i == j )
			{
				continue;
			}
			
			delete this->waypoints[ i ].paths[ j ];
		}
	}
	
	for( int i = 0; i < this->numwaypoints; i++ )
	{
		delete [] this->waypoints[ i ].paths;
	}
	
	delete [] this->waypoints;
}