/*
	Handles player's keyboard and mouse input.
	Author: Jan Tatarynowicz
*/

#ifndef _INPUT_H_
#define _INPUT_H_

#include "Def.h"

class Input
{
	public:
		enum ACTIONS
		{
			ACTION_MOVE_FORWARD,
			ACTION_MOVE_BACKWARD,
			ACTION_MOVE_RIGHT,
			ACTION_MOVE_LEFT,
			ACTION_SHOOT,
			ACTION_MAIN_MENU,
			ACTION_MENU_UP,
			ACTION_MENU_DOWN,
			ACTION_MENU_SELECT,
			ACTION_SELECT_WEAPON1,
			ACTION_SELECT_WEAPON3,
			ACTION_SELECT_WEAPON4,
			ACTION_SELECT_WEAPON6,
			
			NUM_ACTIONS
		};
		
		const int DEFAULT_MAPPING[ NUM_ACTIONS ] =
		{
			SDL_SCANCODE_W,
			SDL_SCANCODE_S,
			SDL_SCANCODE_D,
			SDL_SCANCODE_A,
			SDL_SCANCODE_LCTRL,
			SDL_SCANCODE_ESCAPE,
			SDL_SCANCODE_UP,
			SDL_SCANCODE_DOWN,
			SDL_SCANCODE_RETURN,
			SDL_SCANCODE_1,
			SDL_SCANCODE_3,
			SDL_SCANCODE_4,
			SDL_SCANCODE_6,
		};
		
		Input();
		
		void Init();
		
		/*
			Pumps SDL events - updates keyboard state - and reads mouse position change.
		*/
		void Update();
		
		/*
			Checks if a key on keyboard corresponding to given action is being pressed.
			Gets action code.
			Returns:
				- True if a key on keyboard corresponding to given action is being pressed,
				- False otherwise.
		*/
		bool ActionPerformed( enum ACTIONS action );
		
		/*
			Returns the difference between current and previous mouse position.
		*/
		int GetMouseState() { return this->mx; }
	
	private:
		// Pointer to keyboard state
		const Uint8 *kbState;
		int keyMapping[ NUM_ACTIONS ];
		// Recent mouse position change
		int mx, my;
		bool leftMouseClicked = false;
};

#endif // _INPUT_H_
