/*
	Author: Jan Tatarynowicz
*/

#include "MessageChannel.h"

MessageChannel::MessageChannel()
{
}

void MessageChannel::Update()
{
	Uint32 time = SDL_GetTicks();
	
	if( this->messages.size() > 0 )
		if( time - this->messages[ 0 ].arriveTime >= DURATION )
		{
			delete this->messages[ 0 ].msg;
			this->messages.erase( this->messages.begin() );
		}
	
	while( this->messages.size() > MAX_MESSAGES )
	{
		delete this->messages[ 0 ].msg;
		this->messages.erase( this->messages.begin() );
	}
}

void MessageChannel::PutMessage( char *msg )
{
    msg[ MESSAGE_LENGTH - 1 ] = '\0';
	MessageItem item;
	char *newmsg = new char[ MESSAGE_LENGTH ];
	strcpy( newmsg, msg );
	item.msg = newmsg;
	item.arriveTime = SDL_GetTicks();
	this->messages.push_back( item );
}

MessageChannel::~MessageChannel()
{
	this->messages.clear();
}
