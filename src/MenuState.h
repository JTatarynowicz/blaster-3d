/*
	Methods implementing main menu game state.
	Author: Jan Tatarynowicz
*/

#ifndef _MENU_STATE_H_
#define _MENU_STATE_H_

#include "Def.h"
#include "GameState.h"
#include "GameEngine.h"
#include "Map.h"

class MenuState : public GameState
{
	public:
		enum MENU_OPTIONS
		{
			OPTION_NEW_GAME,
			OPTION_OPTIONS,
			OPTION_DEMO,
			OPTION_CREDITS,
			OPTION_QUIT,
			
			NUM_OPTIONS
		};
		
		const char *OPTIONS_NAMES[ NUM_OPTIONS ] =
		{
			"New Game",
			"Options",
			"Demo",
			"Credits",
			"Quit"
		};
		
		MenuState( GameEngine *game );
		MenuState( GameEngine *game, GameState *playingState );
		~MenuState();
		
		void Init();
		
		void HandleEvent( SDL_Event *event );
		void HandleInput( Input *input );
		void Update();
		void Render( Renderer *renderer );
		
		/*
			Sets state playing in the background while menu state is active.
			Gets pointer to game state to be played in the background.
		*/
		void SetPlayingState( GameState *state ) { this->playingState = state; }
		
		/*
			Returns current game state that is played in the background
			while menu state is active.
		*/
		GameState *GetPlayingState() { return this->playingState; }
		
		unsigned char GetMenuIndex() { return this->menuIndex; }
		
		/*
			Returns vector of menu items' names.
		*/
		std::vector< const char* > GetMenu() { return ( this->menu ); }
		
		Uint32 GetStartTime() { return this->startTime; }
	
	private:
		/*
			Handles events that happen after a menu item is chosen.
		*/
		void MenuSelect();
		
		unsigned char menuIndex;
		
		// Game state that is playing in the background while menu state is active
		GameState *playingState = NULL;
		
		// Menu items' names
		std::vector< const char* > menu;
		
		// Menu state start time
		Uint32 startTime;
};

#endif // _MENU_STATE_H_
