/*
	Author: Jan Tatarynowicz
*/

#include "Input.h"

Input::Input()
{
}

bool Input::ActionPerformed( enum ACTIONS action )
{
	if( action == ACTIONS::ACTION_SHOOT )
		return this->kbState[ keyMapping[ action ] ] || this->leftMouseClicked;
	
	return this->kbState[ keyMapping[ action ] ]; 
}

void Input::Init()
{
	this->kbState = SDL_GetKeyboardState( NULL );
	for( int i = 0; i < ACTIONS::NUM_ACTIONS; i++ )
		keyMapping[ i ] = DEFAULT_MAPPING[ i ];
}

void Input::Update()
{
	// Updates keyboard state
	SDL_PumpEvents();
	this->leftMouseClicked = SDL_GetRelativeMouseState( &( this->mx ), &( this->my ) ) & SDL_BUTTON( SDL_BUTTON_LEFT );
}