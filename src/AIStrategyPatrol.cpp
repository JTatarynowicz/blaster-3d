/*
	Author: Jan Tatarynowicz
*/

#include "AIStrategyPatrol.h"
#include "Enemy.h"
#include "Player.h"
#include "Map.h"
#include "Def.h"
#include "Thing.h"

#include <algorithm>

AIStrategyPatrol::AIStrategyPatrol( Enemy *enemy ) : AIStrategy( enemy )
{
	SetPath( this->enemy->map->GetPathToRandomWaypoint( ( int ) this->enemy->x, ( int ) this->enemy->y ), true );
}

void AIStrategyPatrol::SetPath( std::vector< std::pair< int, int > > *path, bool deletePath = false )
{
	if( this->deletePath )
	{
		this->path->clear();
		delete this->path;
	}
	
	this->deletePath = deletePath;
	
	this->currentNode = 0;
	this->path = path;
}

void AIStrategyPatrol::SetGoal( std::pair< int, int > &newGoal )
{
	this->currentGoal.reached = false;
	this->currentGoal.x = ( float ) newGoal.first * GRID_SIZE + ( float ) GRID_SIZE / 2;
	this->currentGoal.y = ( float ) newGoal.second * GRID_SIZE + ( float ) GRID_SIZE / 2;
}

void AIStrategyPatrol::Move()
{
	if( this->currentGoal.reached )
		this->enemy->move = false;
	else
	{
		this->enemy->move = true;
		
		float dist = sqrt(	( this->enemy->x - this->currentGoal.x ) * ( this->enemy->x - this->currentGoal.x )
						+ ( this->enemy->y - this->currentGoal.y ) * ( this->enemy->y - this->currentGoal.y ) );
	
		if( dist < this->enemy->speed )
		{
			this->currentGoal.reached = true;
			this->enemy->x = this->currentGoal.x;
			this->enemy->y = this->currentGoal.y;
		}
		else
		{
			// Relative thing's angle to current goal
			float angle = atan2( this->enemy->y - this->currentGoal.y, this->currentGoal.x - this->enemy->x ) * 180.0f / PI;
			this->enemy->moveAngle = angle;
		}
	}
}

void AIStrategyPatrol::SelectTarget( std::vector< Thing* > *things )
{
	// Sort things by distance to this enemy
	for( int i = 0; i < ( int ) things->size(); i++ )
	{
		Thing *thing = ( *things )[ i ];
		thing->SetOpDistance( this->enemy->x, this->enemy->y );
	}
	
	std::sort(	things->begin(),
				things->end(),
				[]( Thing *a, Thing *b ) -> bool { return a->GetOpDistance() > b->GetOpDistance(); } );
	
	for( int i = 0; i < ( int ) things->size(); i++ )
	{
		Thing *thing = ( *things )[ i ];
		// Enemy ignores itself
		if( this->enemy == thing )
			continue;
		if( thing->GetType() != Thing::THING_TYPES::THING_TYPE_ENEMY
			&& thing->GetType() != Thing::THING_TYPES::THING_TYPE_PLAYER )
			continue;
		if( thing->IsDead() )
			continue;
		auto pos = thing->GetPosition();
		float tx = std::get< 0 >( pos ), ty = std::get< 1 >( pos );
		float targetAngle = atan2( this->enemy->y - ty, tx - this->enemy->x ) * 180.0f / PI;
		
		// If ray distance is bigger than distance to a thing
		// it means that no wall occludes that thing - this thing is visible
		// Closest visible thing is chosen as a target
		Ray ray( this->enemy->map );
		ray.Cast( this->enemy->x, this->enemy->y, targetAngle, this->enemy );
		float dist = sqrt( ( this->enemy->x - tx ) * ( this->enemy->x - tx ) + ( this->enemy->y - ty ) * ( this->enemy->y - ty ) );
		if( ray.GetDistance() >= dist )
		{
			this->target = thing;
			break;
		}
	}
}

void AIStrategyPatrol::Update( Player *player, std::vector< Thing* > *things )
{
	if( this->enemy->action == Thing::ACTIONS::ACTION_DIE )
		return;
	
	// Try to switch to another weapon if out of ammo
	if( this->enemy->GetWeapon()->GetAmmo() <= 0 )
	{
		for( int i = Weapon::WEAPONS::NUM_WEAPONS - 1; i >= 0; i-- )
		{
			Weapon *weapon = this->enemy->weapons[ i ];
			if( ( weapon->GetAmmo() > 0 ) && weapon->GotWeapon() )
				this->enemy->HideWeapon( ( enum Weapon::WEAPONS ) i );
		}
	}
	
	// Get new path if the old one is reached
	if( this->pathReached )
	{
		SetPath( this->enemy->map->GetRandomWaypointPath( ( int ) this->enemy->x, ( int ) this->enemy->y ) );
		
		this->pathReached = false;
		
		if( this->currentNode < ( int ) this->path->size() )
		{
			std::pair< int, int > newGoal = ( *this->path )[ this->currentNode++ ];
			SetGoal( newGoal );
		}
	}
	
	// Get next goal if current is reached
	if( this->currentGoal.reached )
	{
		if( this->currentNode < ( int ) this->path->size() )
		{
			std::pair< int, int > newGoal = ( *this->path )[ this->currentNode++ ];
			SetGoal( newGoal );
		}
		else
			this->pathReached = true;
	}
	
	Move();
	
	float anglediff, targetAngle;
	if( this->target == NULL )
	{
		SelectTarget( things );
		targetAngle = this->enemy->moveAngle;
	}
	else if( this->target->IsDead() )
	{
		this->target = NULL;
		SelectTarget( things );
		targetAngle = this->enemy->moveAngle;
	}
	else
	{
		auto pos = this->target->GetPosition();
		float tx = std::get< 0 >( pos ), ty = std::get< 1 >( pos );
		float distance = sqrt( ( this->enemy->x - tx ) * ( this->enemy->x - tx ) + ( this->enemy->y - ty ) * ( this->enemy->y - ty ) );
		targetAngle = atan2( this->enemy->y - ty, tx - this->enemy->x ) * 180.0f / PI;
		
		// If ray distance is bigger than distance to a thing
		// it means that no wall occludes that thing - this thing is visible
		Ray ray( this->enemy->map );
		ray.Cast( this->enemy->x, this->enemy->y, targetAngle, this->enemy );
		
		// If target is still visible
		if( ray.GetDistance() >= distance )
		{
			if( this->enemy->angle - targetAngle >= 0.0f )
			{
				if( this->enemy->angle - targetAngle >= 180.0f )
					anglediff = ( targetAngle + 360.0f - this->enemy->angle );
				else
					anglediff = -( this->enemy->angle - targetAngle );
			}
			else
			{
				if( targetAngle - this->enemy->angle >= 180.0f )
					anglediff = -( this->enemy->angle + 360.0f - targetAngle );
				else
					anglediff = targetAngle - this->enemy->angle;
			}
			
			// Shoot at the thing if it's aiming at it or close
			float EPS = 10.0f;
			if( abs( anglediff ) <= EPS )
				this->enemy->Shoot();
		}
		else
		{
			targetAngle = this->enemy->moveAngle;
			this->target = NULL;
		}
	}
	
	// Rotate to face target
	if( this->enemy->angle - targetAngle >= 0.0f )
	{
		if( this->enemy->angle - targetAngle >= 180.0f )
			anglediff = ( targetAngle + 360.0f - this->enemy->angle );
		else
			anglediff = -( this->enemy->angle - targetAngle );
	}
	else
	{
		if( targetAngle - this->enemy->angle >= 180.0f )
			anglediff = -( this->enemy->angle + 360.0f - targetAngle );
		else
			anglediff = targetAngle - this->enemy->angle;
	}
	
	this->enemy->angle += anglediff * 0.2f / ( FPS / 30.0f );
}
