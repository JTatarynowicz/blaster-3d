/*
	Methods that define player interactions with level state.
	In addition to methods inherited from Thing, describes
	controls and player specific actions.
	Author: Jan Tatarynowicz
*/

#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "Def.h"
#include "Thing.h"
#include "Weapon.h"

const float PLAYER_SPEED = 0.015;//0.02f;

class Player : public Thing
{
	public:
		Player( Map *map, float x, float y, float angle );
		
		void HandleInput( Input *input );
		void Update( Thing *observer );
		
		void Respawn( float x, float y, float angle );
		
		/*
			Tells whether player is moving.
			Returns:
				- True if player moves forward/backward/right/left,
				- False otherwise.
		*/
		bool IsMoving() { return	controls.forward
									|| controls.backward
									|| controls.right
									|| controls.left; }
	
	private:
		// Player's controls->actions state
		struct
		{
			unsigned char	forward : 1,
							backward : 1,
							right : 1,
							left : 1,
							shoot : 1;
		} controls;
		
		/*
			Checks whether there is a collision between player and map's geometry (walls).
			Returns:
				- 1 if there is collision,
				- 0 otherwise.
		*/
		void CheckCollision();
		
		/*
			Handles player's actions and sets new ones.
		*/
		void PerformAction();
};

#endif // _PLAYER_H_