/*
	Methods that handle sounds and music.
	Author: Jan Tatarynowicz
*/

#ifndef _MIXER_H_
#define _MIXER_H_

#include "Def.h"

#define FREQUENCY 44100
#define STEREO 2
#define SAMPLE_SIZE 2048

class Mixer
{
	public:
		static constexpr float MAX_DISTANCE = 10.0f;
	    
		enum SOUNDS
		{
		    NONE,
			SHOT_PISTOL,
			SHOT_MACHINEGUN,
			SHOT_SHOTGUN,
			SHOT_PLASMAGUN,
			PICKUP,
			PICKUP_WEAPON,
			
			NUM_SOUNDS
		};
		
		Mixer();
		
		void Init( int soundVolume, int musicVolume );
		void Quit();
		
		/*
			Loads MIDI music track from file.
			Gets path to the file.
		*/
		void LoadMIDITrack( const char *t );
		
		/*
			Plays loaded music MIDI track.
			Requires a MIDI track to be loaded.
		*/
		void PlayMusic();
		void PauseMusic();
		void StopMusic();
		
		/*
			Plays given sound.
			Requires that sound file to be loaded.
			Gets:
				- Sound code,
				- Distance between observer (listener) and sound source.
		*/
		void PlaySound( enum SOUNDS sound, float distance );
	
	private:
		// Range 0 - 255
		int soundVolume = 0;
		int musicVolume = 0;
		
		Mix_Music *music = NULL;
		
		Mix_Chunk *shotBlasterChunk = NULL;
		Mix_Chunk *shotMachinegunChunk = NULL;
		Mix_Chunk *shotShotgunChunk = NULL;
		Mix_Chunk *shotPlasmagunChunk = NULL;
		Mix_Chunk *pickupChunk = NULL;
		Mix_Chunk *pickupGunChunk = NULL;
		
		/*
			Loads all sound files.
		*/
		void LoadChunks();
};

#endif // _MIXER_H_
