/*
	Author: Jan Tatarynowicz
*/

#include "Ray.h"

#include "Map.h"
#include "Thing.h"
#include "Def.h"
#include "LookupTables.h"

#include <cmath>

Ray::Ray( Map *map )
{
	this->map = map;
}

void Ray::Cast( float ox, float oy, float angle, Thing *thing )
{
	this->ox = ox;
	this->oy = oy;
	
	if( angle < 0.0f )
		angle = 360.0f + angle;
	if( angle > 360.0f )
		angle = angle - 360.0f;
	
	// Divide into quadrants
	float ang;
	
	if( angle < 90.0f )
		ang = angle;
	else if( angle < 180.0f )
		ang = 180.0f - angle;
	else if( angle < 270.0f )
		ang = angle - 180.0f;
	else
		ang = 360.0f - angle;
	
	float cs = LookupTables::fastcos( Radians( ang ) );
	float sn = LookupTables::fastsin( Radians( ang ) );
	
	// Avoid division by zero
	if( cs == 0.0f )
		cs = 0.000125f;
	if( sn == 0.0f )
		sn = 0.000125f;
	
	// Perform Amanatides & Woo voxel traversal algorithm, similar to DDA
	// Ray traverses the grid to find vertical/horizontal intersections with grid cells
	// until a wall block is found
	// For details see paper: "A Fast Voxel Traversal Algorithm for Ray Tracing"
	// by John Amanatides and Andrew Woo (1987)
	
	// Horizontal/vertical distances ray has to move to reach neighbouring grid cell
	float dx = GRID_SIZE / cs;
	float dy = GRID_SIZE / sn;
	
	// Distance total traversed in x/y axis
	float hdist, vdist;
	
	// Map's column/row cell coords to inspect
	int column = ( int ) ox / GRID_SIZE;
	int row = ( int ) oy / GRID_SIZE;
	
	// Grid cells traverse direction in x/y axis
	int columndir, rowdir;
	
	// Initialize
	if( angle < 180.0f )
	{
		vdist = ( oy - row * GRID_SIZE ) / GRID_SIZE * dy;
		rowdir = -1;
	}
	else
	{
		vdist = ( row * GRID_SIZE + GRID_SIZE - oy ) / GRID_SIZE * dy;
		rowdir = 1;
	}
	
	if( angle > 90.0f && angle < 270.0f )
	{
		hdist = ( ox - column * GRID_SIZE ) / GRID_SIZE * dx;
		columndir = -1;
	}
	else
	{
		hdist = ( column * GRID_SIZE + GRID_SIZE - ox ) / GRID_SIZE * dx;
		columndir = 1;
	}
	
	this->hitside = HIT_SIDE_HORIZONTAL;
	
	// Traverse the grid
	while( !( ( this->type = map->Inspect( column, row ) ) > 0 ) )
	{
		if( hdist < vdist )
		{
			hdist += dx;
			column += columndir;
			this->hitside = HIT_SIDE_VERTICAL;
		}
		else
		{
			vdist += dy;
			row += rowdir;
			this->hitside = HIT_SIDE_HORIZONTAL;
		}
	}
	
	if( this->hitside == HIT_SIDE_VERTICAL )
		this->distance = hdist - dx;
	else
		this->distance = vdist - dy;
	
	this->hx = this->distance * LookupTables::fastcos( Radians( angle ) ) + ox;
	this->hy = this->distance * -LookupTables::fastsin( Radians( angle ) ) + oy;
	
	this->projectedDistance = this->distance * LookupTables::fastcos( Radians( angle - thing->GetAngle() ) );
	this->angle = angle;
}