/*
	Author: Jan Tatarynowicz
*/

#include "GameEngine.h"

int main( int argc, char *argv[] )
{
	GameEngine game;
	game.Init();
	game.Run();
	
	return 0;
}