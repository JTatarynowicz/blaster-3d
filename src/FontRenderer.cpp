/*
	Author: Jan Tatarynowicz
*/

#include "FontRenderer.h"

#include <stdio.h>

FontRenderer::FontRenderer( SDL_Surface *screen )
{
	this->screen = screen;
}

void FontRenderer::Quit()
{
	TTF_CloseFont( this->font );
	TTF_Quit();
}

void FontRenderer::Init()
{
	if( TTF_Init() == -1 )
	{
		printf( "Could not initialize TTF!\n" );
		
		exit( -1 );
	}
	
	this->font = TTF_OpenFont( "data/font/myfont.ttf", FONT_SIZE );
	
	if( this->font == NULL )
	{
	    printf( "Could not open font: %s\n", TTF_GetError() );
	    exit( -1 );
	}
}

void FontRenderer::RenderText( const char *text, int x, int y, SDL_Color color, bool centered )
{
	SDL_Surface *surf = TTF_RenderText_Solid( this->font, text, color );
	SDL_Rect r = { x, y, surf->w, surf->h };
	
	if( centered )
		r.x -= surf->w / 2, r.y -= surf->h / 2;
	
	SDL_SetSurfaceBlendMode( surf, SDL_BLENDMODE_BLEND );
	SDL_SetSurfaceAlphaMod( surf, color.a );
	SDL_BlitSurface( surf, NULL, this->screen, &r );
	
	SDL_FreeSurface( surf );
}
