/*
	Contains methods for font rendering.
	Author: Jan Tatarynowicz
*/

#ifndef _FONT_RENDERER_H_
#define _FONT_RENDERER_H_

#include "Def.h"

class FontRenderer
{
	public:
		static const unsigned FONT_SIZE = 18;
		
		FontRenderer( SDL_Surface *screen );
		
		void Init();
		void Quit();
		
		/*
			Renders text on screen.
			Gets:
				- A pointer to string of characters,
				- X text position coordinate,
				- Y text position coordinate,
				- Color of rendered text,
				- A flag which tells whether the text should be centered
				(x and y coordinates describe center of text when centered)
		*/
		void RenderText( const char *text, int x, int y, SDL_Color color, bool centered = false );
	
	private:
		SDL_Surface *screen;
		
		TTF_Font *font = NULL;
};

#endif // _FONT_RENDERER_H_
