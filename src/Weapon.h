/*
	Describes weapons' attributes and behaviour.
	Author: Jan Tatarynowicz
*/

#ifndef _WEAPON_H_
#define _WEAPON_H_

#include "Def.h"
#include "Ray.h"
#include "Map.h"

class Weapon
{
	public:
		static const int INFINITE_AMMO = 999999;
		
		enum WEAPONS
		{
			PISTOL,
			SHOTGUN,
			MACHINEGUN,
			PLASMAGUN,
			
			NUM_WEAPONS
		};
		
		enum SHOT_TYPES
		{
			HITSCAN,
			PROJECTILE,
			
			NUM_SHOT_TYPES
		};
		
		int DAMAGES[ NUM_WEAPONS ]
		{
			15,
			10,	// per pellet
			10,
			15	// per plasma
		};
		
		int MAX_AMMO[ NUM_WEAPONS ]
		{
			INFINITE_AMMO,
			100,
			500,
			500
		};
		
		static constexpr int PICKUP_AMMO[ NUM_WEAPONS ]
		{
			0,
			16,
			50,
			100
		};
		
		static constexpr int STARTING_AMMO[ NUM_WEAPONS ]
		{
			INFINITE_AMMO,
			8,
			50,
			100
		};
		
		Uint32 SHOT_TIMES[ NUM_WEAPONS ] =
		{
			400,
			1000,
			100,
			50
		};
		
		static const int NUM_SHOTGUN_PELLETS = 12;
		// Angle difference between each neighbouring shotgun pellet
		static const int SHOTGUN_ANGLE_DIFF = 2;
		
		Weapon( enum WEAPONS which, bool gotWeapon = false );
		
		void Update();
		
		void GiveAmmo( int ammo );
		
		std::vector< Ray > *ShootRays( Map *map, float ox, float oy, float angle, Thing *thing );
		std::vector< Projectile* > *ShootProjectiles( Map *map, float ox, float oy, float angle );
		
		bool isShooting() { return this->shooting; }
		bool MaxAmmo() { return this->ammo == MAX_AMMO[ this->which ]; }
		bool GotWeapon() { return this->gotWeapon; }
		
		enum SHOT_TYPES ShotType() { if( this->which == PLASMAGUN ) return PROJECTILE; else return HITSCAN; }
		
		enum WEAPONS Which() { return this->which; }
		
		int GetAmmo() { return this->ammo; }
		
		int GetDamage() { return DAMAGES[ this->which ]; }
		
		int GetShotTime() { return SHOT_TIMES[ this->which ]; }
		
		void GiveWeapon()
		{
			this->gotWeapon = true;
			this->ammo = this->STARTING_AMMO[ this->which ];
		}
		
		void Reset()
		{
			this->ammo = 0;
			this->gotWeapon = false;
		}
	
	private:
		enum WEAPONS which;
		
		int ammo;
		
		bool shooting;
		bool gotWeapon;
		
		Uint32 shotStartTime;
};

#endif // _WEAPON_H_