/*
	Methods that implement a message channel.
	Messages about picking up items and fragging pop up
	in the upper left corner of the screen.
	Author: Jan Tatarynowicz
*/

#ifndef _MESSAGE_CHANNEL_H_
#define _MESSAGE_CHANNEL_H_

#include "Def.h"

#define MESSAGE_LENGTH 100

typedef struct
{
	char *msg;
	
	Uint32 arriveTime;
} MessageItem;

class MessageChannel
{
	public:
		static const unsigned int DURATION = 10000; // millis
		static const unsigned int FADEAWAY_TIME = DURATION - 2000; // millis, after that time msg fades away
		static const unsigned int MAX_MESSAGES = 10;
		
		MessageChannel();
		~MessageChannel();
		
		/*
			Updates all message items. Old messages begin to fade away,
			outdated messages get deleted.
		*/
		void Update();
		
		/*
			Puts new message item to the channel.
			Gets string of characters - message text.
		*/
		void PutMessage( char *msg );
		
		std::vector< MessageItem > *GetMessages() { return &( this->messages ); }
	
	private:
		std::vector< MessageItem > messages;
};

#endif // _MESSAGE_CHANNEL_H_
