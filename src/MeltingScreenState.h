/*
	Special game state that just introduces melting screen graphical effect,
	when switching between other game states.
	Idea is similar to that of original DOOM and DOOM 2 games by id Software.
	Starting state is rendered and being screen-shot serving as a foreground.
	Same happens to target state which serves as a background.
	Foreground is being divided into columns and each column has
	it's initial height set individually, that makes image distorted making
	that melting effect. Then each column goes down revealing the background.
	Author: Jan Tatarynowicz
*/

#ifndef _MELITNG_SCREEN_STATE_H_
#define _MELTING_SCREEN_STATE_H_

#include "Def.h"
#include "GameState.h"
#include "Map.h"

class MeltingScreenState : public GameState
{
	public:
		// Duration of this state
		static const int MELTDOWN_DURATION = 500;
		// Column width
		static const int PIXELS_PER_COLUMN = 2;
		// Minimal column's height
		static const int MIN_VALUE = -400;
		// Deviation between columns' heights
		static const int MAX_DEVIATION = 40;
		
		MeltingScreenState( GameEngine *game );
		~MeltingScreenState();
		
		/*
			Initializes melting screen state.
			Gets:
				- Pointer to pixels of screen-shot melting state,
				- Pointer to pixels of screen-shot target state,
				- Screen width in pixels,
				- Screen height in pixels.
		*/
		void Init( Uint32 *startFrame, Uint32 *endFrame, int width, int height );
		
		void HandleEvent( SDL_Event *event );
		void HandleInput( Input *input );
		void Update();
		void Render( Renderer *renderer );
		
		Uint32 GetStartTime() { return this->startTime; }
		
		Uint32 *GetStartFrame() { return this->startFrame; }
		Uint32 *GetEndFrame() { return this->endFrame; }
		
		int *GetColumns() { return this->columns; }
	
	private:
		Uint32 startTime;
		
		Uint32 *startFrame = NULL;
		Uint32 *endFrame = NULL;
		
		int *columns, *initial;
		
		int width, height;
};

#endif // _MELTING_SCREEN_STATE_H_
