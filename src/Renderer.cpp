/*
	Author: Jan Tatarynowicz
*/

#include "Renderer.h"
#include "GameState.h"
#include "MenuState.h"
#include "LevelState.h"
#include "MeltingScreenState.h"
#include "Map.h"

#include "Player.h"
#include "Enemy.h"
#include "FontRenderer.h"
#include "Ray.h"
#include "MessageChannel.h"
#include "Camera.h"
#include "Thing.h"
#include "LookupTables.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <vector>
#include <algorithm>

Renderer::Renderer()
{
}

void Renderer::Quit()
{
	SDL_FreeSurface( walltextures[ 0 ] );
	SDL_FreeSurface( walltextures[ 1 ] );
	SDL_FreeSurface( walltextures[ 2 ] );
	SDL_FreeSurface( walltextures[ 3 ] );
	SDL_FreeSurface( walltextures[ 4 ] );
	SDL_FreeSurface( walltextures[ 5 ] );
	
	SDL_FreeSurface( flattextures[ 0 ] );
	SDL_FreeSurface( flattextures[ 1 ] );
	SDL_FreeSurface( flattextures[ 2 ] );
	SDL_FreeSurface( flattextures[ 3 ] );
	SDL_FreeSurface( flattextures[ 4 ] );
	SDL_FreeSurface( flattextures[ 5 ] );
	
	SDL_FreeSurface( thingtextures[ 0 ] );
	
	SDL_FreeSurface( smarineSprites.walk[ 0 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.walk[ 0 ][ 1 ] );
	SDL_FreeSurface( smarineSprites.walk[ 0 ][ 2 ] );
	SDL_FreeSurface( smarineSprites.walk[ 0 ][ 3 ] );
	
	SDL_FreeSurface( smarineSprites.walk[ 1 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.walk[ 1 ][ 1 ] );
	SDL_FreeSurface( smarineSprites.walk[ 1 ][ 2 ] );
	SDL_FreeSurface( smarineSprites.walk[ 1 ][ 3 ] );
	
	SDL_FreeSurface( smarineSprites.walk[ 2 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.walk[ 2 ][ 1 ] );
	SDL_FreeSurface( smarineSprites.walk[ 2 ][ 2 ] );
	SDL_FreeSurface( smarineSprites.walk[ 2 ][ 3 ] );
	
	SDL_FreeSurface( smarineSprites.walk[ 3 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.walk[ 3 ][ 1 ] );
	SDL_FreeSurface( smarineSprites.walk[ 3 ][ 2 ] );
	SDL_FreeSurface( smarineSprites.walk[ 3 ][ 3 ] );
	
	SDL_FreeSurface( smarineSprites.walk[ 4 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.walk[ 4 ][ 1 ] );
	SDL_FreeSurface( smarineSprites.walk[ 4 ][ 2 ] );
	SDL_FreeSurface( smarineSprites.walk[ 4 ][ 3 ] );
	
	SDL_FreeSurface( smarineSprites.walk[ 5 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.walk[ 5 ][ 1 ] );
	SDL_FreeSurface( smarineSprites.walk[ 5 ][ 2 ] );
	SDL_FreeSurface( smarineSprites.walk[ 5 ][ 3 ] );
	
	SDL_FreeSurface( smarineSprites.walk[ 6 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.walk[ 6 ][ 1 ] );
	SDL_FreeSurface( smarineSprites.walk[ 6 ][ 2 ] );
	SDL_FreeSurface( smarineSprites.walk[ 6 ][ 3 ] );
	
	SDL_FreeSurface( smarineSprites.walk[ 7 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.walk[ 7 ][ 1 ] );
	SDL_FreeSurface( smarineSprites.walk[ 7 ][ 2 ] );
	SDL_FreeSurface( smarineSprites.walk[ 7 ][ 3 ] );
	
	SDL_FreeSurface( smarineSprites.idle[ 0 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.idle[ 1 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.idle[ 2 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.idle[ 3 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.idle[ 4 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.idle[ 5 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.idle[ 6 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.idle[ 7 ][ 0 ] );
	
	SDL_FreeSurface( smarineSprites.die[ 0 ][ 0 ] );
	SDL_FreeSurface( smarineSprites.die[ 0 ][ 1 ] );
	SDL_FreeSurface( smarineSprites.die[ 0 ][ 2 ] );
	SDL_FreeSurface( smarineSprites.die[ 0 ][ 3 ] );
	
	SDL_FreeSurface( plasmaSprites.sprites[ 0 ] );
	SDL_FreeSurface( plasmaSprites.sprites[ 1 ] );
	SDL_FreeSurface( plasmaSprites.sprites[ 2 ] );
	SDL_FreeSurface( plasmaSprites.sprites[ 3 ] );
	
	SDL_FreeSurface( shotgunSprites.weaponSprites[ 0 ] );
	SDL_FreeSurface( shotgunSprites.shootSprites[ 0 ] );
	SDL_FreeSurface( shotgunSprites.shootSprites[ 1 ] );
	SDL_FreeSurface( shotgunSprites.shootSprites[ 2 ] );
	SDL_FreeSurface( shotgunSprites.shootSprites[ 3 ] );
	SDL_FreeSurface( shotgunSprites.shootSprites[ 4 ] );
	SDL_FreeSurface( shotgunSprites.shootSprites[ 5 ] );
	SDL_FreeSurface( shotgunSprites.shootSprites[ 6 ] );
	
	SDL_FreeSurface( machinegunSprites.weaponSprites[ 0 ] );
	SDL_FreeSurface( machinegunSprites.shootSprites[ 0 ] );
	SDL_FreeSurface( machinegunSprites.shootSprites[ 1 ] );
	
	SDL_FreeSurface( plasmagunSprites.weaponSprites[ 0 ] );
	SDL_FreeSurface( plasmagunSprites.shootSprites[ 0 ] );
	
	SDL_FreeSurface( pistolSprites.weaponSprites[ 0 ] );
	SDL_FreeSurface( pistolSprites.shootSprites[ 0 ] );
	SDL_FreeSurface( pistolSprites.shootSprites[ 1 ] );
	SDL_FreeSurface( pistolSprites.shootSprites[ 2 ] );
	
	SDL_FreeSurface( pickupSprites.bulletbox );
	SDL_FreeSurface( pickupSprites.shellbox );
	SDL_FreeSurface( pickupSprites.cells );
	SDL_FreeSurface( pickupSprites.smallmedkit );
	SDL_FreeSurface( pickupSprites.largemedkit );
	SDL_FreeSurface( pickupSprites.machinegun );
	SDL_FreeSurface( pickupSprites.shotgun );
	SDL_FreeSurface( pickupSprites.plasmagun );
	
	SDL_FreeSurface( puffSprites.sprites[ 0 ] );
	SDL_FreeSurface( puffSprites.sprites[ 1 ] );
	SDL_FreeSurface( puffSprites.sprites[ 2 ] );
	SDL_FreeSurface( puffSprites.sprites[ 3 ] );
	
	SDL_FreeSurface( plasmaPuffSprites.sprites[ 0 ] );
	SDL_FreeSurface( plasmaPuffSprites.sprites[ 1 ] );
	SDL_FreeSurface( plasmaPuffSprites.sprites[ 2 ] );
	SDL_FreeSurface( plasmaPuffSprites.sprites[ 3 ] );
	
	SDL_FreeSurface( hudElements.plate1 );
	SDL_FreeSurface( hudElements.plate2 );
	
	SDL_FreeSurface( background );
	SDL_FreeSurface( menubg );
	
	delete [] shotgunSprites.weaponSprites;
	delete [] shotgunSprites.shootSprites;
	
	delete [] machinegunSprites.weaponSprites;
	delete [] machinegunSprites.shootSprites;
	
	delete [] plasmagunSprites.weaponSprites;
	delete [] plasmagunSprites.shootSprites;
	
	delete [] pistolSprites.weaponSprites;
	delete [] pistolSprites.shootSprites;
	
	SDL_DestroyWindow( this->window );
	
	delete [] this->zbuffer;
	
	this->fontRenderer->Quit();
	
	delete this->fontRenderer;
	
	SDL_Quit();
}

void Renderer::Init( int width, int height, bool fullscreen, bool renderFlats, bool forceAspectRatio )
{
	this->screenWidth = width;
	this->screenHeight = height;
	this->fullscreen = fullscreen;
	this->renderFlats = renderFlats;
	
	if( forceAspectRatio )
		this->forceAspectRatio = ( float ) width / ( float ) height;
	
	SDL_Init( SDL_INIT_VIDEO );
	
	int screenMode = ( this->fullscreen ) ? SDL_WINDOW_FULLSCREEN : 0;
	
	window = SDL_CreateWindow( "rc", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, this->screenWidth, this->screenHeight, screenMode );
	
	if( window == NULL )
	{
		printf( "Could not create window: %s\n", SDL_GetError() );
		exit( -1 );
	}
	else
	{
		screen = SDL_GetWindowSurface( window );
		
		if( screen == NULL )
		{
			printf( "Could not get surface from window: %s\n", SDL_GetError() );
			exit( -1 );
		}
	}
	
	healthBar.YPOS = screenHeight - HealthBar::HEIGHT - 24;
	
	walltextures[ 0 ] = LoadTexture( "data/textures/wall1.bmp" );
	walltextures[ 1 ] = LoadTexture( "data/textures/wall2.bmp" );
	walltextures[ 2 ] = LoadTexture( "data/textures/wall3.bmp" );
	walltextures[ 3 ] = LoadTexture( "data/textures/wall4.bmp" );
	walltextures[ 4 ] = LoadTexture( "data/textures/wall5.bmp" );
	walltextures[ 5 ] = LoadTexture( "data/textures/wall6.bmp" );
	
	flattextures[ 0 ] = LoadTexture( "data/textures/flat1.bmp" );
	flattextures[ 1 ] = LoadTexture( "data/textures/flat2.bmp" );
	flattextures[ 2 ] = LoadTexture( "data/textures/flat3.bmp" );
	flattextures[ 3 ] = LoadTexture( "data/textures/flat4.bmp" );
	flattextures[ 4 ] = LoadTexture( "data/textures/flat5.bmp" );
	flattextures[ 5 ] = LoadTexture( "data/textures/flat6.bmp" );
	
	thingtextures[ 0 ] = LoadTexture( "data/sprites/things/1.bmp" );
	
	smarineSprites.walk[ 0 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_w1_1.bmp" );
	smarineSprites.walk[ 0 ][ 1 ] = LoadTexture( "data/sprites/smarine/smarine_w2_1.bmp" );
	smarineSprites.walk[ 0 ][ 2 ] = LoadTexture( "data/sprites/smarine/smarine_w3_1.bmp" );
	smarineSprites.walk[ 0 ][ 3 ] = LoadTexture( "data/sprites/smarine/smarine_w4_1.bmp" );
	
	smarineSprites.walk[ 1 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_w1_2.bmp" );
	smarineSprites.walk[ 1 ][ 1 ] = LoadTexture( "data/sprites/smarine/smarine_w2_2.bmp" );
	smarineSprites.walk[ 1 ][ 2 ] = LoadTexture( "data/sprites/smarine/smarine_w3_2.bmp" );
	smarineSprites.walk[ 1 ][ 3 ] = LoadTexture( "data/sprites/smarine/smarine_w4_2.bmp" );
	
	smarineSprites.walk[ 2 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_w1_3.bmp" );
	smarineSprites.walk[ 2 ][ 1 ] = LoadTexture( "data/sprites/smarine/smarine_w2_3.bmp" );
	smarineSprites.walk[ 2 ][ 2 ] = LoadTexture( "data/sprites/smarine/smarine_w3_3.bmp" );
	smarineSprites.walk[ 2 ][ 3 ] = LoadTexture( "data/sprites/smarine/smarine_w4_3.bmp" );
	
	smarineSprites.walk[ 3 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_w1_4.bmp" );
	smarineSprites.walk[ 3 ][ 1 ] = LoadTexture( "data/sprites/smarine/smarine_w2_4.bmp" );
	smarineSprites.walk[ 3 ][ 2 ] = LoadTexture( "data/sprites/smarine/smarine_w3_4.bmp" );
	smarineSprites.walk[ 3 ][ 3 ] = LoadTexture( "data/sprites/smarine/smarine_w4_4.bmp" );
	
	smarineSprites.walk[ 4 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_w1_5.bmp" );
	smarineSprites.walk[ 4 ][ 1 ] = LoadTexture( "data/sprites/smarine/smarine_w2_5.bmp" );
	smarineSprites.walk[ 4 ][ 2 ] = LoadTexture( "data/sprites/smarine/smarine_w3_5.bmp" );
	smarineSprites.walk[ 4 ][ 3 ] = LoadTexture( "data/sprites/smarine/smarine_w4_5.bmp" );
	
	smarineSprites.walk[ 5 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_w1_6.bmp" );
	smarineSprites.walk[ 5 ][ 1 ] = LoadTexture( "data/sprites/smarine/smarine_w2_6.bmp" );
	smarineSprites.walk[ 5 ][ 2 ] = LoadTexture( "data/sprites/smarine/smarine_w3_6.bmp" );
	smarineSprites.walk[ 5 ][ 3 ] = LoadTexture( "data/sprites/smarine/smarine_w4_6.bmp" );
	
	smarineSprites.walk[ 6 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_w1_7.bmp" );
	smarineSprites.walk[ 6 ][ 1 ] = LoadTexture( "data/sprites/smarine/smarine_w2_7.bmp" );
	smarineSprites.walk[ 6 ][ 2 ] = LoadTexture( "data/sprites/smarine/smarine_w3_7.bmp" );
	smarineSprites.walk[ 6 ][ 3 ] = LoadTexture( "data/sprites/smarine/smarine_w4_7.bmp" );
	
	smarineSprites.walk[ 7 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_w1_8.bmp" );
	smarineSprites.walk[ 7 ][ 1 ] = LoadTexture( "data/sprites/smarine/smarine_w2_8.bmp" );
	smarineSprites.walk[ 7 ][ 2 ] = LoadTexture( "data/sprites/smarine/smarine_w3_8.bmp" );
	smarineSprites.walk[ 7 ][ 3 ] = LoadTexture( "data/sprites/smarine/smarine_w4_8.bmp" );
	
	smarineSprites.idle[ 0 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_s_1.bmp" );
	smarineSprites.idle[ 1 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_s_2.bmp" );
	smarineSprites.idle[ 2 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_s_3.bmp" );
	smarineSprites.idle[ 3 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_s_4.bmp" );
	smarineSprites.idle[ 4 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_s_5.bmp" );
	smarineSprites.idle[ 5 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_s_6.bmp" );
	smarineSprites.idle[ 6 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_s_7.bmp" );
	smarineSprites.idle[ 7 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_s_8.bmp" );
	
	smarineSprites.die[ 0 ][ 0 ] = LoadTexture( "data/sprites/smarine/smarine_die1.bmp" );
	smarineSprites.die[ 0 ][ 1 ] = LoadTexture( "data/sprites/smarine/smarine_die2.bmp" );
	smarineSprites.die[ 0 ][ 2 ] = LoadTexture( "data/sprites/smarine/smarine_die3.bmp" );
	smarineSprites.die[ 0 ][ 3 ] = LoadTexture( "data/sprites/smarine/smarine_die4.bmp" );
	
	plasmaSprites.sprites[ 0 ] = LoadTexture( "data/sprites/plasma/0.bmp" );
	plasmaSprites.sprites[ 1 ] = LoadTexture( "data/sprites/plasma/1.bmp" );
	plasmaSprites.sprites[ 2 ] = LoadTexture( "data/sprites/plasma/2.bmp" );
	plasmaSprites.sprites[ 3 ] = LoadTexture( "data/sprites/plasma/3.bmp" );
	
	shotgunSprites.numFrames = 8;
	shotgunSprites.numWeaponFrames = 1;
	shotgunSprites.numShootFrames = 7;
	shotgunSprites.frameLength = 1050 / ( shotgunSprites.numFrames - 1 );
	shotgunSprites.shift = 0;
	shotgunSprites.scale = 0.85f;
	
	shotgunSprites.weaponSprites = new SDL_Surface * [ shotgunSprites.numWeaponFrames ];
	shotgunSprites.shootSprites = new SDL_Surface * [ shotgunSprites.numShootFrames ];
	
	shotgunSprites.weaponSprites[ 0 ] = LoadTexture( "data/sprites/shotgun/0.bmp" );
	shotgunSprites.shootSprites[ 0 ] = LoadTexture( "data/sprites/shotgun/1.bmp" );
	shotgunSprites.shootSprites[ 1 ] = LoadTexture( "data/sprites/shotgun/2.bmp" );
	shotgunSprites.shootSprites[ 2 ] = LoadTexture( "data/sprites/shotgun/3.bmp" );
	shotgunSprites.shootSprites[ 3 ] = LoadTexture( "data/sprites/shotgun/4.bmp" );
	shotgunSprites.shootSprites[ 4 ] = LoadTexture( "data/sprites/shotgun/5.bmp" );
	shotgunSprites.shootSprites[ 5 ] = LoadTexture( "data/sprites/shotgun/6.bmp" );
	shotgunSprites.shootSprites[ 6 ] = LoadTexture( "data/sprites/shotgun/7.bmp" );
	
	machinegunSprites.numFrames = 3;
	machinegunSprites.numWeaponFrames = 1;
	machinegunSprites.numShootFrames = 2;
	machinegunSprites.frameLength = 150 / ( machinegunSprites.numFrames - 1 );
	machinegunSprites.shift = 0;
	machinegunSprites.scale = 1.5f;
	
	machinegunSprites.weaponSprites = new SDL_Surface * [ machinegunSprites.numWeaponFrames ];
	machinegunSprites.shootSprites = new SDL_Surface * [ machinegunSprites.numShootFrames ];
	
	machinegunSprites.weaponSprites[ 0 ] = LoadTexture( "data/sprites/machinegun/0.bmp" );
	machinegunSprites.shootSprites[ 0 ] = LoadTexture( "data/sprites/machinegun/1.bmp" );
	machinegunSprites.shootSprites[ 1 ] = LoadTexture( "data/sprites/machinegun/2.bmp" );
	
	plasmagunSprites.numFrames = 2;
	plasmagunSprites.numWeaponFrames = 1;
	plasmagunSprites.numShootFrames = 1;
	plasmagunSprites.frameLength = 50 / ( plasmagunSprites.numFrames - 1 );
	plasmagunSprites.shift = 0;
	plasmagunSprites.scale = 1.25f;
	
	plasmagunSprites.weaponSprites = new SDL_Surface * [ plasmagunSprites.numWeaponFrames ];
	plasmagunSprites.shootSprites = new SDL_Surface * [ plasmagunSprites.numShootFrames ];
	
	plasmagunSprites.weaponSprites[ 0 ] = LoadTexture( "data/sprites/plasmagun/0.bmp" );
	plasmagunSprites.shootSprites[ 0 ] = LoadTexture( "data/sprites/plasmagun/1.bmp" );
	
	pistolSprites.numFrames = 4;
	pistolSprites.numWeaponFrames = 1;
	pistolSprites.numShootFrames = 3;
	pistolSprites.frameLength = 500 / ( pistolSprites.numFrames - 1 );
	pistolSprites.shift = 0;
	pistolSprites.scale = 1.4f;
	
	pistolSprites.weaponSprites = new SDL_Surface * [ pistolSprites.numWeaponFrames ];
	pistolSprites.shootSprites = new SDL_Surface * [ pistolSprites.numShootFrames ];
	
	pistolSprites.weaponSprites[ 0 ] = LoadTexture( "data/sprites/pistol/0.bmp" );
	pistolSprites.shootSprites[ 0 ] = LoadTexture( "data/sprites/pistol/1.bmp" );
	pistolSprites.shootSprites[ 1 ] = LoadTexture( "data/sprites/pistol/2.bmp" );
	pistolSprites.shootSprites[ 2 ] = LoadTexture( "data/sprites/pistol/3.bmp" );
	
	pickupSprites.bulletbox = LoadTexture( "data/sprites/pickups/bulletbox.bmp" );
	pickupSprites.shellbox = LoadTexture( "data/sprites/pickups/shellbox.bmp" );
	pickupSprites.cells = LoadTexture( "data/sprites/pickups/cells.bmp" );
	pickupSprites.smallmedkit = LoadTexture( "data/sprites/pickups/smallmedkit.bmp" );
	pickupSprites.largemedkit = LoadTexture( "data/sprites/pickups/largemedkit.bmp" );
	pickupSprites.machinegun = LoadTexture( "data/sprites/pickups/machinegun.bmp" );
	pickupSprites.shotgun = LoadTexture( "data/sprites/pickups/shotgun.bmp" );
	pickupSprites.plasmagun = LoadTexture( "data/sprites/pickups/plasmagun.bmp" );
	
	puffSprites.sprites[ 0 ] = LoadTexture( "data/sprites/puff/puff0.bmp" );
	puffSprites.sprites[ 1 ] = LoadTexture( "data/sprites/puff/puff1.bmp" );
	puffSprites.sprites[ 2 ] = LoadTexture( "data/sprites/puff/puff2.bmp" );
	puffSprites.sprites[ 3 ] = LoadTexture( "data/sprites/puff/puff3.bmp" );
	
	plasmaPuffSprites.sprites[ 0 ] = LoadTexture( "data/sprites/puff/plasma0.bmp" );
	plasmaPuffSprites.sprites[ 1 ] = LoadTexture( "data/sprites/puff/plasma1.bmp" );
	plasmaPuffSprites.sprites[ 2 ] = LoadTexture( "data/sprites/puff/plasma2.bmp" );
	plasmaPuffSprites.sprites[ 3 ] = LoadTexture( "data/sprites/puff/plasma3.bmp" );
	
	hudElements.plate1 = LoadTexture( "data/hud/plate1.bmp" );
	hudElements.plate2 = LoadTexture( "data/hud/plate2.bmp" );
	
	background = LoadTexture( "data/textures/bg.bmp" );
	menubg = LoadTexture( "data/menu/menubg.bmp" );
	
	SDL_ShowCursor( SDL_DISABLE );
	SDL_SetRelativeMouseMode( SDL_TRUE );
	
	int mx, my;
	
	zbuffer = new float[ this->screenWidth ];//( float* ) malloc( sizeof( float ) * this->screenWidth );
	int i;
	for( i = 0; i < this->screenWidth; i++ )
		zbuffer[ i ] = 0.0f;
	
	SDL_GetRelativeMouseState( &mx, &my );
	
	SDL_SetSurfaceBlendMode( this->screen, SDL_BLENDMODE_BLEND );
	
	Uint32 *pixels = ( Uint32* ) this->screen->pixels;
	for( int i = 0; i < this->screenWidth; i++ )
		for( int j = 0; j < this->screenHeight; j++ )
			pixels[ j * this->screenWidth + i ] = 255 << 24;
	
	this->fontRenderer = new FontRenderer( this->screen );
	this->fontRenderer->Init();
}

void Renderer::Render( MenuState *state )
{
	// Render main menu background if no other game state is playing
	if( state->GetPlayingState() == NULL )
	{
		Uint32 *pixels = ( Uint32* ) this->screen->pixels;
		Uint32 *tex = ( Uint32* ) this->menubg->pixels;
		
		for( int i = 0; i < this->screenWidth; i++ )
		{
			for( int j = 0; j < this->screenHeight; j++ )
			{
				int texcolumn = ( float ) i * ( float ) this->menubg->w / ( float ) this->screenWidth;
				int texrow = ( float ) j * ( float ) this->menubg->h / ( float ) this->screenHeight;
				
				pixels[ j * this->screenWidth + i ] = tex[ texrow * this->menubg->w + texcolumn ];
			}
		}
	}
	
	
	// Render main menu options texts
	Uint32 time = SDL_GetTicks() - state->GetStartTime();
	Uint8 val = 0xaa + 0x33 * sin( ( float ) time / 100.0f );
	
	SDL_Color white = { val, val, val, 254 };
	SDL_Color gray = { 0xaa, 0xaa, 0xaa, 254 };
	
	std::vector< const char* > menu = state->GetMenu();
	unsigned char menuIndex = state->GetMenuIndex();
	
	for( int i = 0; i < ( int ) menu.size(); i++ )
	{
		const char *str = menu[ i ];
		int len = strlen( str );
		if( i == menuIndex )
			this->fontRenderer->RenderText( str, this->screenWidth / 2 - len / 2, i * 50 + 150, white, true );
		else
			this->fontRenderer->RenderText( str, this->screenWidth / 2 - len / 2, i * 50 + 150, gray, true );
	}
}

void Renderer::Render( LevelState *state )
{
	Thing *observer = state->GetCamera()->GetObserver();
	Thing *player = state->GetPlayer();
	std::vector< Thing* > *things = state->getThings();
	Map *map = state->getMap();
	
	Uint32 *pixels = ( Uint32* ) screen->pixels;
	
	// Sort renderable things to perform painter's algorithm
	// Sort by depth - furthest things to camera are rendered first
	std::sort(	things->begin(),
				things->end(),
				[]( Thing *a, Thing *b ) -> bool { return a->GetCameraDistance() > b->GetCameraDistance(); } );
	
	// If no floors are to be rendered, fill bottom half of the screen with floor color
	if( !this->renderFlats )
		for( int column = 0; column < this->screenWidth; column++ )
			for( int row = this->screenHeight / 2; row < this->screenHeight; row++ )
				pixels[ row * this->screenWidth + column ] = 0x00888888;
	
	// Render screen columns (walls, floors, ceiling) using raycasting technique
	Ray ray( map );
	int column;
	for( column = 0; column < this->screenWidth; column++ )
	{
		float angle = observer->GetAngle() + LookupTables::fastatan( ( float ) ( column - this->screenWidth / 2 ) / PLANE_DISTANCE ) * 180.0f / PI;
		
		auto pos = observer->GetPosition();
		float px = std::get< 0 >( pos ), py = std::get< 1 >( pos );
		ray.Cast( px, py, angle, observer );
		DrawColumn( this->screenWidth - column - 1, observer, map, ray );
	}
	
	std::vector< Puff* > *puffs = state->GetPuffs();
	
	for( int i = 0; i < ( int ) puffs->size(); i++ )
		this->RenderPuff( ( *puffs )[ i ], observer );
	
	for( int i = 0; i < ( int ) things->size(); i++ )
		this->RenderThing( ( *things )[ i ], observer );
	
	if( observer != player )
		this->RenderThing( player, observer );
	
	RenderWeapon( observer );
	
	// Render message channel
	std::vector< MessageItem > *msgs = state->GetMessageChannel()->GetMessages();
	
	for( int i = 0; i < ( int ) msgs->size(); i++ )
	{
		MessageItem item = ( *msgs )[ i ];
		
		Uint32 timeElapsed = SDL_GetTicks() - item.arriveTime;
		
		Uint8 alpha;
		
		if( timeElapsed >= MessageChannel::FADEAWAY_TIME )
			alpha = ( Uint8 ) ( ( float ) ( MessageChannel::DURATION - timeElapsed )
				/ ( float ) ( MessageChannel::DURATION - MessageChannel::FADEAWAY_TIME ) * 254.0f );
		else
			alpha = 254;
		
		SDL_Color clr = { 255, 255, 255, alpha };
		
		this->fontRenderer->RenderText( item.msg, 5, 16 * i, clr );
	}
	
	RenderHUD( observer );
	
	if( state->RequestsShowStats() )
		RenderStatsTable( state );
}

void Renderer::RenderHUD( Thing *player )
{
	// Render ammo count
	SDL_Color clr = { 255, 255, 255, 254 };
	Weapon *weapon = player->GetWeapon();
	int ammo = weapon->GetAmmo();
	char buff[ 50 ];
	
	if( ammo != Weapon::INFINITE_AMMO )
		sprintf( buff, "%d", ammo );
	else
		sprintf( buff, " " );
	
	RenderHudElement( hudElements.plate1, this->screenWidth - 100, this->screenHeight - 40, true );
	this->fontRenderer->RenderText( buff, this->screenWidth - 100, this->screenHeight - 40, clr, true );
	
	// Update and render health bar
	int health = player->GetHealth();
	
	if( health > healthBar.health )
		healthBar.health += ( healthBar.health + 2 <= health ) ? 2 : 1;
	else if( health < healthBar.health )
		healthBar.health -= ( healthBar.health - 2 >= health ) ? 2 : 1;
	
	int	hbx = HealthBar::XPOS,
		hby = healthBar.YPOS,
		hbw = HealthBar::WIDTH,
		hbh = HealthBar::HEIGHT,
		hbb = HealthBar::BORDER;
	
	Uint32 *pixels = ( Uint32* ) this->screen->pixels;
	for( int i = hbx; i < hbx + hbw; i++ )
	{
		for( int j = hby; j < hby + hbh; j++ )
		{
			if(	i >= hbx + hbb && i < hbx + hbw - hbb
				&& j > hby + hbb && j < hby + hbh - hbb )
			{
				float c = ( float ) healthBar.health / ( float ) HealthBar::MAX_HEALTH;
				float d = ( float ) health / ( float ) HealthBar::MAX_HEALTH;
				int yh = hby + hbb + ( hbh - hbb - ( hbh - hbb ) * c );
				int yhbh = hby + hbb + ( hbh - hbb - ( hbh - hbb ) * d );
				if( j >= yh && j < yhbh && health < healthBar.health )
				{
					pixels[ j * this->screenWidth + i ] = 0x00440000;
				}
				else if( j >= yhbh )
				{
					pixels[ j * this->screenWidth + i ] =	0x00550000 + 0x00010000 * ( healthBar.HEIGHT - ( j - hby ) )
														+( ( health > healthBar.health ) ? 0x00110000 : 0 );
				}
				else
				{
					pixels[ j * this->screenWidth + i ] = 0x00000000;
				}
			}
			else
			{
				pixels[ j * this->screenWidth + i ] = 0x00000000;
			}
		}
	}
	
	// Render health count
	RenderHudElement( hudElements.plate1, 100, this->screenHeight - 40, true );
	SDL_Color clr2 = { 200, 0, 0, 254 };
	sprintf( buff, "%d", health );
	this->fontRenderer->RenderText( buff, 100, this->screenHeight - 40, clr2, true );
	
	RenderCrosshair();
}

void Renderer::RenderStatsTable( LevelState *state )
{
	std::vector< std::pair< const char*, int > > stats = state->GetStats();
	
	// Sort players by their score
	std::sort(	stats.begin(),
				stats.end(),
				[]( std::pair< const char*, int > a, std::pair< const char*, int > b ) -> bool { return a.second > b.second; } );
	
	RenderHudElement( hudElements.plate2, this->screenWidth / 2, this->screenHeight / 2, true );
	
	// Render (name, score) entries
	SDL_Color clr = { 0, 100, 250, 254 };
	
	char buff[ 100 ];
	
	sprintf( buff, "Name" );
	this->fontRenderer->RenderText
	( 
		buff,
		this->screenWidth / 2 - hudElements.plate2->w / 2 + 10,
		this->screenHeight / 2 - hudElements.plate2->h / 2 + 10,
		clr
	);
	
	sprintf( buff, "Kills" );
	this->fontRenderer->RenderText
	( 
		buff,
		this->screenWidth / 2 + hudElements.plate2->w / 2 - 50,
		this->screenHeight / 2 - hudElements.plate2->h / 2 + 10,
		clr
	);
	
	for( int i = 0; i < ( int ) stats.size(); i++ )
	{
		sprintf( buff, "%s", stats[ i ].first );
		this->fontRenderer->RenderText
		(
			buff,
			this->screenWidth / 2 - hudElements.plate2->w / 2 + 10,
			this->screenHeight / 2 - hudElements.plate2->h / 2 + 50 * ( i + 2 ) + 10,
			clr 
		);
		sprintf( buff, "%d", stats[ i ].second );
		this->fontRenderer->RenderText
		( 
			buff,
			this->screenWidth / 2 + hudElements.plate2->w / 2 - 50,
			this->screenHeight / 2 - hudElements.plate2->h / 2 + 50 * ( i + 2 ) + 10,
			clr
		);
	}
}

void Renderer::Render( MeltingScreenState *state )
{
	// Start frame is foreground, end frame is background
	Uint32 *startFrame = state->GetStartFrame();
	Uint32 *endFrame = state->GetEndFrame();
	
	int *columns = state->GetColumns();
	
	Uint32 *frame;
	Uint32 *pixels = ( Uint32* ) this->screen->pixels;
	
	for( int i = 0; i < this->screenWidth; i++ )
		for( int j = 0; j < this->screenHeight; j++ )
		{
			int index;
			if( j < columns[ i ] )
			{
				frame = endFrame;
				index = j * this->screenWidth + i;
			}
			else
			{
				frame = startFrame;
				
				int column = ( columns[ i ] >= 0 ) ? j - columns[ i ] : j;
				if( column < 0 )
					column = 0;
				if( column >= this->screenHeight )
					column = this->screenHeight - 1;
				index = column * this->screenWidth + i;
			}
			
			pixels[ j * this->screenWidth + i ] = frame[ index ];
		}
}

Uint32 * Renderer::GetFrame()
{
	Uint32 *frame = new Uint32[ this->screenWidth * this->screenHeight ];
	memcpy( frame, this->screen->pixels, sizeof ( Uint32 ) * this->screenWidth * this->screenHeight );
	
	return frame;
}

SDL_Surface * Renderer::LoadTexture( const char *path )
{
	SDL_Surface *data = SDL_LoadBMP( path );
	size_t width = data->w;
	size_t height = data->h;
	Uint8 *texture = ( Uint8* ) data->pixels;
	
	SDL_Surface *surf = SDL_CreateRGBSurface( 0, width, height, 32, 0, 0, 0, 0 );
	
	Uint32 *surfdata = ( Uint32* ) surf->pixels;
	
	// Convert to rgba format
	int i;
	for( i = 0; i < ( int ) ( width * height ); i++ )
	{
		Uint32 color = texture[ 3 * i + 2 ];
		color <<= 8;
		color |= texture[ 3 * i + 1 ];
		color <<= 8;
		color |= texture[ 3 * i ];
		
		surfdata[ i ] = color;
	}
	
	return surf;
}

void Renderer::DrawColumn( int column, Thing *player, Map *map, Ray ray )
{
	float raydistance = ray.GetProjectedDistance();
	// Wall column height
	int length = ( float ) this->screenHeight / raydistance; length *= ( float ) PLANE_DISTANCE / ( float ) this->screenHeight * this->forceAspectRatio;
	if( length % 2 != 0 ) length++;
	// Top of the wall column
	int start = this->screenHeight / 2 - length / 2;
	// Bottom of the wall column
	int end = start + length;
	
	zbuffer[ column ] = ray.GetDistance();
	
	if( start < 0 )
		start = 0;
	if( end > this->screenHeight )
		end = this->screenHeight;
	
	Uint32 *pixels = ( Uint32* ) screen->pixels;
	Uint32 *texture;
	
	int raytype = ray.GetType();
	
	if( raytype > 0 && raytype <= NUM_WALL_TEXTURES )
		texture = ( Uint32* ) walltextures[ raytype - 1 ]->pixels;
	else if( -raytype <= NUM_WALL_TEXTURES )
		texture = ( Uint32* ) walltextures[ raytype - 1 ]->pixels;
	else
		texture = ( Uint32* ) walltextures[ 0 ]->pixels;
	
	float texrow = 0;
	// Wall texture row advance
	float nextrow = ( float ) TEXTURE_SIZE / ( float ) length;
	
	// Wall texture column
	int texcolumn;
	
	enum Ray::HIT_SIDES hitside = ray.GetHitSide();
	std::pair< float, float > raypos = ray.GetHitPosition();
	float rayx = std::get< 0 >( raypos ), rayy = std::get< 1 >( raypos );
	
	// If ray hit vertical sides of wall, wall texture column is based on ray's y coordinate
	// ray's x coordinate is taken into account otherwise
	if( hitside == Ray::HIT_SIDES::HIT_SIDE_VERTICAL )
		texcolumn = ( rayy - ( int ) rayy / GRID_SIZE * GRID_SIZE ) / GRID_SIZE * TEXTURE_SIZE;
	else
		texcolumn = ( rayx - ( int ) rayx / GRID_SIZE * GRID_SIZE ) / GRID_SIZE * TEXTURE_SIZE;
	
	// Don't render wall texture from the top if column's height is greater than screen height
	if( length > this->screenHeight )
	{
		texrow += ( length - this->screenHeight ) / 2 * nextrow;
	}
	
	// Render background sky
	Uint32 *bgtex = ( Uint32* ) this->background->pixels;
	for( int row = start - 1; row >= 0; row-- )
	{
		int index = row * this->screenWidth + column;
		int bgtexrow = this->background->h * ( float ) row / ( this->screenHeight / 2 );
		int bgtexcol = ( int ) ( ( ray.GetAngle() / 360.0f ) * ( float ) this->background->w );
		int texindx = bgtexrow * this->background->w + bgtexcol;
		
		pixels[ index ] = bgtex[ texindx ];
	}
	
	// Render wall
	int row;
	for( row = start; row < end; row++, texrow += nextrow )
	{
		int index = row * this->screenWidth + column;
		int texindx = ( int ) texrow * TEXTURE_SIZE + texcolumn;
		
		pixels[ index ] = texture[ texindx ];
	}
	
	float rayangle = ray.GetAngle();
	
	// Render floors and ceilings
	// Ceilings are floors mirrored, though textures may be different
	if( this->renderFlats )
	{
		float cosdiff = LookupTables::fastcos( Radians( rayangle - player->GetAngle() ) );
		
		// Avoid division by zero
		if( cosdiff == 0.0f )
			cosdiff = 0.00001f;
		
		float sina = LookupTables::fastsin( Radians( rayangle ) );
		float cosa = LookupTables::fastcos( Radians( rayangle ) );
		
		auto pos = player->GetPosition();
		float x = std::get< 0 >( pos ), y = std::get< 1 >( pos );
		
		if( row > this->screenHeight / 2 )
		for( ; row < this->screenHeight; row++ )
		{
			// Distance to rendered floor/ceiling pixel
			float dist = ( this->screenHeight / 2.0f ) / ( row + 1 - this->screenHeight / 2.0f ) / cosdiff;
			dist *= ( float ) PLANE_DISTANCE / ( float ) this->screenHeight * this->forceAspectRatio;
			
			// Rendered floor pixel coordinates on x/y plane
			float px = cosa * dist + x;
			float py = -sina * dist + y;
			
			// Texture coordinates
			int texx = ( px - ( int ) px / GRID_SIZE * GRID_SIZE ) / GRID_SIZE * TEXTURE_SIZE;
			int texy = ( py - ( int ) py / GRID_SIZE * GRID_SIZE ) / GRID_SIZE * TEXTURE_SIZE;
			
			int index = row * this->screenWidth + column;
			int ceilindex = ( this->screenHeight - row - 1 ) * this->screenWidth + column;
			int texindx = texy * TEXTURE_SIZE + texx;
			
			char type = map->Inspect( ( int ) px / GRID_SIZE, ( int ) py / GRID_SIZE );
			
			// Negative tile type sign means is floor texture
			if( -type <= NUM_FLAT_TEXTURES && type < 0 )
				texture = ( Uint32* ) flattextures[ -type - 1 ]->pixels;
			else
				texture = ( Uint32* ) flattextures[ 1 ]->pixels;
			
			pixels[ index ] = texture[ texindx ];
			
			if( type )
				pixels[ ceilindex ] = texture[ texindx ];
		}
	}
}

void Renderer::RenderThing( Thing *thing, Thing *player )
{
	SDL_Surface *sprite = GetThingSprite( thing, player );
	
	if( sprite == NULL )
		return;
	
	Uint32 *texture = ( Uint32* ) sprite->pixels;
	
	auto tpos = thing->GetPosition();
	auto ppos = player->GetPosition();
	float tx = std::get< 0 >( tpos ), ty = std::get< 1 >( tpos );
	float px = std::get< 0 >( ppos ), py = std::get< 1 >( ppos );
	
	// Don't render thing with same position as observer
	if( tx == px && ty == py )
		return;
	
	// Relative thing angle to observer
	float angle = atan2( ty - py, tx - px ) + Radians( player->GetAngle() );
	int width = PLANE_DISTANCE / thing->GetCameraDistance() / LookupTables::fastcos( angle ) * thing->GetSpriteWidth();
	int height =  this->forceAspectRatio * PLANE_DISTANCE / thing->GetCameraDistance() / LookupTables::fastcos( angle ) * thing->GetSpriteHeight();
	// Sprite vertical center
	int vcenter = tan( angle ) * PLANE_DISTANCE;
	
	Uint32 *pixels = ( Uint32* ) screen->pixels;
	
	// Starting row
	int	rowb = this->screenHeight / 2 + this->forceAspectRatio * ( PLANE_DISTANCE / thing->GetCameraDistance() * ( 0.5f - thing->GetSpriteHeight() - thing->GetZ() ) ) / LookupTables::fastcos( angle ) + 1,
		column = this->screenWidth / 2 + vcenter - width / 2;
	
	// Ending column/row
	int colend = this->screenWidth / 2 + vcenter + width / 2;
	int rowend = rowb + ( float ) height;
	
	// Texture horizontal/vertical advance
	float texwa = ( float ) sprite->w / ( float ) width;
	float texha = ( float ) sprite->h / ( float ) height;
	
	float texcol = 0.0f;
	if( this->screenWidth / 2 + vcenter - width / 2 < 0 )
		texcol = -( this->screenWidth / 2 + vcenter - width / 2 ) * texwa;
	// Starting texture row
	float texrowb = 0.0f;
	
	// Dealing with screen boundaries
	if( colend < 0 )
		colend = 0;
	if( colend > this->screenWidth )
		colend = this->screenWidth;
	if( rowend < 0 )
		rowend = 0;
	if( rowend > this->screenHeight )
		rowend = this->screenHeight;
	
	if( column < 0 )
		column = 0;
	if( column >= this->screenWidth )
		return;
	if( rowb < 0 )
	{
		texrowb = -rowb * texha;
		rowb = 0;
	}
	if( rowb >= this->screenHeight )
		return;
	
	int transparentclr = ( thing->GetType() == Thing::THING_TYPES::THING_TYPE_ENEMY ) ? 0x00980088 : 0x0000ffff;
	
	// Rendering
	int c = rowb * this->screenWidth;
	for( ; column < colend; column++ )
	{
		// If no wall ocludes
		if( zbuffer[ column ] >= thing->GetCameraDistance() )
		{
			int row;
			float texrow = texrowb;
			int index = c + column;
			for( row = rowb; row < rowend; row++ )
			{
				int texindx = ( int ) texrow * sprite->w + ( int ) texcol;
				
				if( ( int ) texture[ texindx ] != transparentclr && texture[ texindx ] != 0x0000ffff )
					pixels[ index ] = texture[ texindx ];
				
				texrow += texha;
				
				// Next row
				index += this->screenWidth;
			}
		}
		
		texcol += texwa;
	}
}

void Renderer::RenderPuff( Puff *puff, Thing *player )
{
	Uint32 time = SDL_GetTicks() - puff->time;
	// Animation frame index
	unsigned int index = time / PuffSprites::FRAME_LENGTH;
	if( index < 0 || index >= PuffSprites::NUM_FRAMES )
		return;
	
	SDL_Surface *sprite;
	enum Puff::PUFF_TYPES type = puff->type;
	
	switch( type )
	{
		case Puff::PUFF_TYPES::PUFF_TYPE_BULLET:
			sprite = puffSprites.sprites[ index ];
			break;
		
		case Puff::PUFF_TYPES::PUFF_TYPE_PLASMA:
			sprite = plasmaPuffSprites.sprites[ index ];
			break;
		
		default:
			sprite = puffSprites.sprites[ index ];
			break;
	}
	
	Uint32 *texture = ( Uint32* ) sprite->pixels;
	
	auto ppos = player->GetPosition();
	float pfx = puff->x, pfy = puff->y;
	float px = std::get< 0 >( ppos ), py = std::get< 1 >( ppos );
	
	float dist = sqrt( ( px - pfx ) * ( px - pfx ) + ( py - pfy ) * ( py - pfy ) );
	
	// Relative puff angle to observer
	float angle = LookupTables::fastatan2( pfy - py, pfx - px ) +  Radians( player->GetAngle() );
	int width = PLANE_DISTANCE / dist / LookupTables::fastcos( angle ) * PUFF_WIDTH;
	int height =  this->forceAspectRatio * PLANE_DISTANCE / dist / cos( angle ) * PUFF_HEIGHT;
	// Sprite's vertical center
	int vcenter = tan( angle ) * PLANE_DISTANCE;
	
	Uint32 *pixels = ( Uint32* ) screen->pixels;
	
	// Starting row
	int	rowb = this->screenHeight / 2 + this->forceAspectRatio * ( PLANE_DISTANCE / dist * ( 0.5f - PUFF_HEIGHT / 2 - puff->z ) ) / LookupTables::fastcos( angle ) + 1,
		column = this->screenWidth / 2 + vcenter - width / 2;
	
	// Ending column/row
	int colend = this->screenWidth / 2 + vcenter + width / 2;
	int rowend = rowb + ( float ) height;
	
	// Texture horizontal/vertical advance
	float texwa = ( float ) sprite->w / ( float ) width;
	float texha = ( float ) sprite->h / ( float ) height;
	
	float texcol = 0.0f;
	if( this->screenWidth / 2 + vcenter - width / 2 < 0 )
		texcol = -( this->screenWidth / 2 + vcenter - width / 2 ) * texwa;
	// Texture starting row
	float texrowb = 0.0f;
	
	// Dealing with screen boundaries
	if( colend < 0 )
		colend = 0;
	if( colend > this->screenWidth )
		colend = this->screenWidth;
	if( rowend < 0 )
		rowend = 0;
	if( rowend > this->screenHeight )
		rowend = this->screenHeight;
	
	if( column < 0 )
		column = 0;
	if( column >= this->screenWidth )
		return;
	if( rowb < 0 )
	{
		texrowb = -rowb * texha;
		rowb = 0;
	}
	if( rowb >= this->screenHeight )
		return;
	
	int transparentclr = 0;
	
	const float EPS = 0.02f;
	
	int c = rowb * this->screenWidth;
	for( ; column < colend; column++ )
	{
		// If no wall ocludes
		if( zbuffer[ column ] + EPS >= dist )
		{
			int row;
			float texrow = texrowb;
			int index = c + column;
			for( row = rowb; row < rowend; row++ )
			{
				int texindx = ( int ) texrow * sprite->w + ( int ) texcol;
				
				if( ( int ) texture[ texindx ] != transparentclr )
					pixels[ index ] = texture[ texindx ];
				
				texrow += texha;
				
				// Next row
				index += this->screenWidth;
			}
		}
		
		texcol += texwa;
	}
}

void Renderer::RenderFPS()
{
	SDL_Color clr = { 250, 250, 250, 254 };
	char buff[ 100 ];
	sprintf( buff, "%d (%dms)", this->fps, this->millis );
	this->fontRenderer->RenderText( buff, this->screenWidth - 100, 5, clr );
}

SDL_Surface * Renderer::GetThingSprite( Thing *thing, Thing *player )
{
	if( thing->GetType() == Thing::THING_TYPES::THING_TYPE_ENEMY || thing->GetType() == Thing::THING_TYPES::THING_TYPE_PLAYER )
	{
		int eangle = ( int ) thing->GetAngle();
		int relangle = ( int ) thing->GetRelativeAngle();
		
		// Animation frame angle index
		int aindex = ( ( relangle + 270 + 22 + ( 360 - eangle ) ) / 45 ) % 8;
		
		enum Thing::ACTIONS action = thing->GetAction();
		
		Uint32 time = SDL_GetTicks() - thing->GetActionTime();
		
		int numframe;
		
		SDL_Surface *sprite;
		
		switch( action )
		{
			case Thing::ACTIONS::ACTION_IDLE:
				numframe = 0;
				sprite = smarineSprites.idle[ aindex ][ numframe ];
				break;
			
			case Thing::ACTIONS::ACTION_WALK:
				numframe = time / SmarineSprites::WALK_FRAME_LENGTH % SmarineSprites::NUM_WALK_FRAMES;
				sprite = smarineSprites.walk[ aindex ][ numframe ];
				break;
			
			case Thing::ACTIONS::ACTION_DIE:
				if( time >= SmarineSprites::DIE_FRAME_LENGTH )
					numframe = SmarineSprites::NUM_DIE_FRAMES - 1;
				else
					numframe = time / SmarineSprites::DIE_FRAME_LENGTH % SmarineSprites::NUM_DIE_FRAMES;
				
				sprite = smarineSprites.die[ 0 ][ numframe ];
				break;
			
			default:
				numframe = time / SmarineSprites::WALK_FRAME_LENGTH % SmarineSprites::NUM_WALK_FRAMES;
				sprite = smarineSprites.walk[ aindex ][ numframe ];
				break;
		}
		
		return sprite;
	}
	else if( thing->GetType() == Thing::THING_TYPES::THING_TYPE_PLANT )
	{
		return thingtextures[ 0 ];
	}
	else if( thing->GetType() == Thing::THING_TYPES::THING_TYPE_PICKUP )
	{
		if( thing->GetAction() == Thing::ACTIONS::ACTION_RESPAWN )
			return NULL;
		
		switch( thing->WhichPickup() )
		{
			case Thing::PICKUPS::BULLETBOX:
				return pickupSprites.bulletbox;
				break;
			
			case Thing::PICKUPS::SHELLBOX:
				return pickupSprites.shellbox;
				break;
			
			case Thing::PICKUPS::CELLS:
				return pickupSprites.cells;
				break;
			
			case Thing::PICKUPS::SMALLMEDKIT:
				return pickupSprites.smallmedkit;
				break;
			
			case Thing::PICKUPS::LARGEMEDKIT:
				return pickupSprites.largemedkit;
				break;
			
			case Thing::PICKUPS::MACHINEGUN:
				return pickupSprites.machinegun;
				break;
			
			case Thing::PICKUPS::SHOTGUN:
				return pickupSprites.shotgun;
				break;
			
			case Thing::PICKUPS::PLASMAGUN:
				return pickupSprites.plasmagun;
				break;
			
			default:
				return thingtextures[ 0 ];
				break;
		}
	}
	else if( thing->GetType() == Thing::THING_TYPES::THING_TYPE_PROJECTILE )
	{
		Uint32 time = SDL_GetTicks() - thing->GetActionTime();
		unsigned int frt = time / PlasmaSprites::FRAME_LENGTH;
		if( frt >= PlasmaSprites::NUM_FRAMES )
			frt = PlasmaSprites::NUM_FRAMES - 1;
		unsigned int numframe = frt % PlasmaSprites::NUM_FRAMES;
		return plasmaSprites.sprites[ numframe ];
	}
	else
		return thingtextures[ 0 ];
}

void Renderer::RenderWeapon( Thing *player )
{
	if( player->IsDead() )
		return;
	
	SDL_Surface *surf;
	WeaponSprites *weaponSprites;
	
	switch( player->GetWeapon()->Which() )
	{
		case Weapon::WEAPONS::SHOTGUN:
			weaponSprites = &shotgunSprites;
			break;
		
		case Weapon::WEAPONS::MACHINEGUN:
			weaponSprites = &machinegunSprites;
			break;
		
		case Weapon::WEAPONS::PISTOL:
			weaponSprites = &pistolSprites;
			break;
		
		default:
			weaponSprites = &plasmagunSprites;
			break;
	}
	
	// Weapon is bobbing while player is moving
	if( player->IsMoving() )
	{
		if( !weaponSprites->bobs )
		{
			weaponSprites->bobs = true;
			weaponSprites->bobTime = SDL_GetTicks();
		}
		else
		{
			weaponSprites->vbob2 += 2;
			if( weaponSprites->vbob2 > 15 )
				weaponSprites->vbob2 = 15;
			
			float time = ( float ) ( SDL_GetTicks() - weaponSprites->bobTime );
			weaponSprites->vbob = 7 * cos( 2 * time * PI / 180.0f / 2 );
			weaponSprites->hbob = 9 * cos( time * PI / 180.0f / 2 );
		}
	}
	else
	{
		if( weaponSprites->vbob2 > 0 )
			weaponSprites->vbob2 -= 2;
		weaponSprites->vbob = 0.0f;
		weaponSprites->hbob = 0.0f;
		weaponSprites->bobs = false;
	}
	
	if( weaponSprites->vbob + weaponSprites->vbob2 < 0.0f )
		weaponSprites->vbob2 -= weaponSprites->vbob + weaponSprites->vbob2;
	
	if( player->GetAction() == Thing::ACTIONS::ACTION_SHOOT )
	{
		Uint32 time = SDL_GetTicks() - player->GetActionTime();
		surf = weaponSprites->shootSprites[ time / weaponSprites->frameLength % weaponSprites->numShootFrames ];
		weaponSprites->vbob = weaponSprites->vbob2 = weaponSprites->hbob = 0.0f;
	}
	else
	{
		surf = weaponSprites->weaponSprites[ 0 ];
	}
	
	weaponSprites->hpos = ( ( this->screenWidth / 2 ) - weaponSprites->scale * ( surf->w / 2 * this->screenWidth / 800.0f - weaponSprites->shift ) * this->forceAspectRatio );
	weaponSprites->vpos = ( this->screenHeight - weaponSprites->scale * surf->h * this->forceAspectRatio * this->screenHeight / 600.0f + 5 );
	
	if( player->GetAction() == Thing::ACTIONS::ACTION_HIDE_WEAPON )
	{
		if( SDL_GetTicks() - player->GetActionTime() < Player::ACTION_TIME_HIDE_WEAPON )
			weaponSprites->vpos +=	weaponSprites->scale * surf->h
									* ( SDL_GetTicks() - player->GetActionTime() ) / Player::ACTION_TIME_HIDE_WEAPON;
		else
			return;
	}
	else if( player->GetAction() == Thing::ACTIONS::ACTION_DRAW_WEAPON )
	{
		if( SDL_GetTicks() - player->GetActionTime() < Player::ACTION_TIME_DRAW_WEAPON )
			weaponSprites->vpos +=	 ( weaponSprites->scale * surf->h -  weaponSprites->scale * surf->h
									* ( SDL_GetTicks() - player->GetActionTime() ) / Player::ACTION_TIME_DRAW_WEAPON );
	}
	
	Uint32 *sprite = ( Uint32* ) surf->pixels;
	Uint32 *pixels = ( Uint32* ) screen->pixels;
	
	int width = weaponSprites->scale * surf->w * this->forceAspectRatio;
	int height = weaponSprites->scale * surf->h * this->forceAspectRatio;
	
	// Scale size with resolution
	width *= this->screenWidth / 800.0f;
	height *= this->screenHeight / 600.0f;
	
	// Texture horizontal/vertical advance
	float texha = ( float ) surf->w / ( float ) width;
	float texva = ( float ) surf->h / ( float ) height;
	
	float texrow = 0.0f;
	float texcol = 0.0f;
	
	for( int column = 0; column < width; column++ )
	{
		texrow = 0.0f;
		
		for( int row = 0; row < height; row++ )
		{
			if( weaponSprites->vpos + row + ( int ) weaponSprites->vbob2 + ( int ) weaponSprites->vbob >= this->screenHeight )
				break;
			
			int index =	( weaponSprites->vpos + row + ( int ) weaponSprites->vbob2 + ( int ) weaponSprites->vbob )
						* this->screenWidth + weaponSprites->hpos + column + weaponSprites->hbob;
			int texindx = ( int ) texrow * surf->w + ( int ) texcol;
			
			if( sprite[ texindx ] != 0x0000ffff )
				pixels[ index ] = sprite[ texindx ];
			
			texrow += texva;
		}
		
		texcol += texha;
	}
}

void Renderer::RenderCrosshair()
{
	Uint32 *pixels = ( Uint32* ) this->screen->pixels;
	
	int x1 = this->screenWidth / 2 - 1, x2 = x1 + 1;
	int y1 = this->screenHeight / 2 - 1, y2 = y1 + 1;
	
	int	i1 = y1 * this->screenWidth + x1,
		i2 = y1 * this->screenWidth + x2,
		i3 = y2 * this->screenWidth + x1,
		i4 = y2 * this->screenWidth + x2;
	
	pixels[ i1 ] = 0xffffffff - pixels[ i1 ];
	pixels[ i2 ] = 0xffffffff - pixels[ i2 ];
	pixels[ i3 ] = 0xffffffff - pixels[ i3 ];
	pixels[ i4 ] = 0xffffffff - pixels[ i4 ];
}

void Renderer::RenderHudElement( SDL_Surface *surf, int x, int y, bool centered = false )
{
	Uint32 *tex = ( Uint32* ) surf->pixels;
	Uint32 *pixels = ( Uint32* ) this->screen->pixels;
	
	// Textures position
	int x0 = x, y0 = y;
	
	if( centered )
	{
		x0 = x - surf->w / 2;
		y0 = y - surf->h / 2;
	}
	
	for( int row = 0; row < surf->h; row++ )
	{
		if( row + y0 >= this->screenHeight )
			break;
		
		for( int column = 0; column < surf->w; column++ )
		{
			if( column + x0 >= this->screenWidth )
				continue;
			
			pixels[ ( row + y0 ) * this->screenWidth + column + x0 ] = tex[ row * surf->w + column ];
		}
	}
}

void Renderer::SwapBuffers()
{
	SDL_UpdateWindowSurface( window );
}
