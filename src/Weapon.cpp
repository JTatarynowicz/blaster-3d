/*
	Author: Jan Tatarynowicz
*/

#include "Weapon.h"
#include "Thing.h"
#include "Projectile.h"

constexpr int Weapon::STARTING_AMMO[ NUM_WEAPONS ];

Weapon::Weapon( enum WEAPONS which, bool gotWeapon )
{
	this->which = which;
	this->shooting = false;
	this->gotWeapon = gotWeapon;
	this->ammo = Weapon::STARTING_AMMO[ this->which ];
}

std::vector< Ray > * Weapon::ShootRays( Map *map, float ox, float oy, float angle, Thing *thing )
{
	if( this->shooting )
		return NULL;
	
	if( this->ammo <= 0 )
		return NULL;
	
	if( this->ammo != INFINITE_AMMO )
		this->ammo--;
	
	this->shotStartTime = SDL_GetTicks();
	
	std::vector< Ray > *rays = new std::vector< Ray >();
	
	auto tpos = thing->GetPosition();
	float tx = std::get< 0 >( tpos ), ty = std::get< 1 >( tpos );
	float tangle = thing->GetAngle();
	
	switch( this->which )
	{
		case SHOTGUN:
			for( int i = -NUM_SHOTGUN_PELLETS / 2; i < NUM_SHOTGUN_PELLETS / 2; i++ )
			{
				float angle = tangle + i * SHOTGUN_ANGLE_DIFF;
				
				if( angle < 0 )
					angle = 360 + angle;
				else if( angle > 360 )
					angle = angle - 360;
				
				Ray ray( map );
				ray.Cast( tx, ty, angle, thing );
				rays->push_back( ray );
			}
			break;
		
		case PISTOL:
			{
				Ray ray( map );
				ray.Cast( tx, ty, tangle, thing );
				rays->push_back( ray );
			}
			break;
		
		case MACHINEGUN:
			{
				Ray ray( map );
				ray.Cast( tx, ty, tangle, thing );
				rays->push_back( ray );
			}
			break;
		
		default:
			return NULL;
			break;
	}
	
	return rays;
}

std::vector< Projectile* > * Weapon::ShootProjectiles( Map *map, float ox, float oy, float angle )
{
	if( this->shooting )
		return NULL;
	
	if( this->ammo <= 0 )
		return NULL;
	
	if( this->ammo != INFINITE_AMMO )
		this->ammo--;
	
	this->shotStartTime = SDL_GetTicks();
	
	std::vector< Projectile* > *projectiles = new std::vector< Projectile* >();
	
	switch( this->which )
	{
		case PLASMAGUN:
			{
				float rangle = angle;
				
				if( rangle < 0.0f )
					rangle = 360.0f + rangle;
				if( rangle >= 360.0f )
					rangle = rangle - 360.0f;
				float dx = cos( Radians( rangle ) ) * 0.175f;
				float dy = -sin( Radians( rangle ) ) * 0.175f;
				
				Projectile *projectile = new Projectile(	map,
															Projectile::PROJECTILE_TYPES::PROJECTILE_TYPE_FLAME,
															ox + 0 * dx,
															oy + 0 * dy,
															dx,
															dy,
															rangle,
															0.25f,	// width
															0.25f,	// height
															100000, 	// lifetime
															this->DAMAGES[ PLASMAGUN ] );
				projectiles->push_back( projectile );
			}
			break;
		
		default:
			break;
	}
	
	return projectiles;
}

void Weapon::GiveAmmo( int ammo )
{
	this->ammo += ammo;
	
	if( this->ammo > MAX_AMMO[ this->which ] )
		this->ammo = MAX_AMMO[ this->which ];
}

void Weapon::Update()
{
	if( this->shooting )
	{
		if( SDL_GetTicks() - this->shotStartTime >= SHOT_TIMES[ this->which ] )
			this->shooting = false;
	}
}