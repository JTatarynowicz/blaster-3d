/*
	Contains methods which control gameplay state.
	Here is the gameplay. Map is being loaded, players and things are spawned,
	shots are fired and death takes it's toll.
	Author: Jan Tatarynowicz
*/

#ifndef _LEVEL_STATE_H_
#define _LEVEL_STATE_H_

#include "Def.h"
#include "GameState.h"
#include "Map.h"
#include "MessageChannel.h"
#include "Camera.h"

class LevelState : public GameState
{
	public:
		// Bots' names
		static const int NUM_NAMES = 17;
		const char *NAMES[ NUM_NAMES ] =
		{
			"Bot1",
			"Bot2",
			"Bot3",
			"Bot4",
			"Bot5",
			"Bot6",
			"Bot7",
			"Bot8",
			"Bot9",
			"Bot0",
			"Bot-1",
			"Bot-2",
			"Bot-3",
			"Bot-4",
			"Bot-5",
			"Bot-6",
			"Bot-7"
		};
		
		LevelState( GameEngine *game );
		~LevelState();
		
		void Init();
		
		/*
			Handles polled SDL events (mainly input).
			Gets pointer to a SDL event.
		*/
		void HandleEvent( SDL_Event *event );
		
		/*
			Handles player input.
			Gets pointer to input handler.
		*/
		void HandleInput( Input *input );
		
		/*
			Updates all level entities: player, enemies, things and projectiles.
		*/
		void Update();
		
		/*
			Requests renderer to render level state scene, based on Visitor OOP pattern.
			Gets pointer to renderer.
		*/
		void Render( Renderer *renderer );
		
		/*
			Returns:
				- True if stats table has to be shown,
				- False otherwise.
		*/
		bool RequestsShowStats() { return RequestShowStats; }
		
		Player *GetPlayer() { return this->player; }
		std::vector< Thing* > *getThings() { return &( this->things ); }
		std::vector< Enemy* > *getEnemies() { return &( this->enemies ); }
		std::vector< Projectile* > *GetProjectiles() { return &( this->projectiles ); }
		std::vector< Puff* > *GetPuffs() { return &( this->puffs ); }
		
		/*
			Returns vector of pairs (name, frags).
		*/
		std::vector< std::pair< const char*, int > > GetStats();
		
		Map *getMap() { return &( this->map ); }
		Camera *GetCamera() { return &( this->camera ); }
		
		MessageChannel *GetMessageChannel() { return &( this->msgChannel ); }
	
	private:
		void InitData();
		void UpdateThings();
		
		void SwitchObserver();
		
		/*
			Calculates whether a shooting player/enemy has hit anyone/who.
			Gets pointer to a shooting thing.
		*/
		void HandleShot( Thing *shooter );
		
		/*
			Respawns a thing (player or enemy) - resets it's health
			and sets position equal to randomly chosen spawner.
			Gets pointer to a thing to respawn.
		*/
		void Respawn( Thing *thing );
		
		Player *player;
		std::vector< Thing* > things;
		std::vector< Enemy* > enemies;
		// All the things that can be shot
		std::vector< Thing* > shootables;
		std::vector< Projectile* > projectiles;
		std::vector< Thing* > pickups;
		std::vector< Puff* > puffs;
		std::vector< Thing* > observers;
		// Places where player and enemies may spawn/respawn
		std::vector< Thing* > spawners;
		
		Map map;
		Camera camera;
		CheatHandler cheatHandler;
		
		int observer = 0;
		
		MessageChannel msgChannel;
		
		int numthings = 0;
		int numenemies = 0;
		int numspawners = 0;
		
		bool RequestShowStats = false;
};

#endif // _LEVEL_STATE_H_
