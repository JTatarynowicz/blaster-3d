/*
	Contains look up tables for trigonometric functions
	used for fast sine, cosine, tg calculations.
	Author: Jan Tatarynowicz
*/

#ifndef _LOOKUP_TABLES_H_
#define _LOOKUP_TABLES_H_

#include "Def.h"

class LookupTables
{
	public:
		static const int NUM_LOOKUP_SAMPLES = 100000;
		static const int NUM_ATAN_LOOKUP_SAMPLES = 1000000;
		static constexpr float ANGLE_STEP = FULL_ANGLE / ( float ) NUM_LOOKUP_SAMPLES;
		static const int MAX_ATAN_ARG = 1000;
		static constexpr float ATAN_ARG_STEP = ( float ) MAX_ATAN_ARG / ( float ) NUM_ATAN_LOOKUP_SAMPLES;
		
		static float table_atan[ NUM_ATAN_LOOKUP_SAMPLES ];
		static float table_sin[ NUM_LOOKUP_SAMPLES ];
		static float table_cos[ NUM_LOOKUP_SAMPLES ];
		
		LookupTables(){};
		
		static int LookupIndex( float x );
		static int AtanLookupIndex( float x );
		
		static void InitLookupTables();

		static float fastsin( float a );
		static float fastcos( float a );
		static float fastatan( float a );
		static float fastatan2( float y, float x );
};

#endif	// _LOOKUP_TABLES_H_