/*
	Author: Jan Tatarynowicz
*/

#include "GameEngine.h"
#include "Renderer.h"
#include "Input.h"
#include "MenuState.h"
#include "LevelState.h"
#include "MeltingScreenState.h"
#include "LookupTables.h"

GameEngine::GameEngine()
{
}

void GameEngine::PushNewState( enum GameState::GAME_STATES s )
{
	GameState *state;
	
	switch( s )
	{
		case GameState::GAME_STATES::MENU_STATE:
			state = new MenuState( this, this->stateStack.top() );
			break;
		
		case GameState::GAME_STATES::LEVEL_STATE:
			state = new LevelState( this );
			break;
		
		case GameState::GAME_STATES::MELTING_SCREEN_STATE:
			state = new MeltingScreenState( this );
			break;
		
		default:
			state = new MenuState( this );
			break;
	}
	
	state->Init();
	
	PushState( state );
}

void GameEngine::PushState( GameState *state )
{
	this->stateStack.push( state );
}

void GameEngine::PopState()
{
	if( this->stateStack.empty() )
	{
		// Error
	}
	else
	{
		GameState *state = this->stateStack.top();
		delete state;
		this->stateStack.pop();
	}
}

void GameEngine::PlaySound( enum Mixer::SOUNDS sound, float distance )
{
	this->mixer.PlaySound( sound, distance );
}

void GameEngine::Init()
{
	srand( time( NULL ) );
	LookupTables::InitLookupTables();
	LoadConfig( Config.CONFIG_FILE_PATH );
	renderer.Init
	(
		Config.screenWidth,
		Config.screenHeight,
		Config.fullscreen,
		Config.renderFlats,
		Config.forceAspectRatio
	);
	mixer.Init( Config.soundVolume, Config.musicVolume );
	input.Init();
	MenuState *state = new MenuState( this );
	state->Init();
	PushState( state );
	mixer.LoadMIDITrack( "data/music/intro.mid" );
	mixer.PlayMusic();
}

void GameEngine::LoadConfig( const char *path )
{
	if( access( path, F_OK | R_OK ) != -1 )
	{
		FILE *file = fopen( path, "rb" );
		
		int screenWidth;
		fscanf( file, "screen_width=%d\n", &screenWidth );
		int screenHeight;
		fscanf( file, "screen_height=%d\n", &screenHeight );
		int fullscreen;
		fscanf( file, "fullscreen=%d\n", &fullscreen );
		int renderFlats;
		fscanf( file, "render_flats=%d\n", &renderFlats );
		int forceAspectRatio;
		fscanf( file, "force_aspect_ratio=%d\n", &forceAspectRatio );
		int soundVolume;
		fscanf( file, "sound_volume=%d\n", &soundVolume );
		int musicVolume;
		fscanf( file, "music_volume=%d\n", &musicVolume );
		
		fclose( file );
		
		this->Config.screenWidth = screenWidth;
		this->Config.screenHeight = screenHeight;
		this->Config.fullscreen = ( fullscreen == 1 ) ? true : false;
		this->Config.renderFlats = ( renderFlats == 1 ) ? true : false;
		this->Config.forceAspectRatio = ( forceAspectRatio == 1 ) ? true : false;
		this->Config.soundVolume = soundVolume;
		this->Config.musicVolume = musicVolume;
	}
	else
	{
		SetDefaultConfig();
		SaveConfig( path );
	}
}

void GameEngine::SaveConfig( const char *path )
{
	FILE *file = fopen( path, "wb" );
	
	fprintf( file, "screen_width=%d\n", this->Config.screenWidth );
	fprintf( file, "screen_height=%d\n", this->Config.screenHeight );
	fprintf( file, "fullscreen=%d\n", this->Config.fullscreen );
	fprintf( file, "render_flats=%d\n", this->Config.renderFlats );
	fprintf( file, "force_aspect_ratio=%d\n", this->Config.forceAspectRatio );
	fprintf( file, "sound_volume=%d\n", this->Config.soundVolume );
	fprintf( file, "music_volume=%d\n", this->Config.musicVolume );
	
	fclose( file );
}

void GameEngine::SetDefaultConfig()
{
	this->Config.screenWidth = Config.DEFAULT_SCREEN_WIDTH;
	this->Config.screenHeight = Config.DEFAULT_SCREEN_HEIGHT;
	this->Config.fullscreen = Config.DEFAULT_FULLSCREEN;
	this->Config.renderFlats = Config.DEFAULT_RENDER_FLATS;
	this->Config.forceAspectRatio = Config.DEFAULT_FORCE_ASPECT_RATIO;
	this->Config.soundVolume = Config.DEFAULT_SOUND_VOLUME;
	this->Config.musicVolume = Config.DEFAULT_MUSIC_VOLUME;
}

void GameEngine::UpdateStateStack()
{
	if( this->stateStack.empty() )
		PushNewState( GameState::GAME_STATES::MENU_STATE );
	
	GameState *state = this->stateStack.top();
	enum GameState::MESSAGES msg = state->PollMessage();
	switch( msg )
	{
		case GameState::MESSAGES::NO_MESSAGE:
			break;
		
		case GameState::MESSAGES::MESSAGE_POP:
			if( this->stateStack.size() > 1 )
				PopState();
			break;
		
		case GameState::MESSAGES::MESSAGE_PUSH:
			PushNewState( state->GetTargetState() );
			break;
		
		case GameState::MESSAGES::MESSAGE_SWITCH:
			PopState();
			PushNewState( state->GetTargetState() );
			break;
		
		case GameState::MESSAGES::MESSAGE_MELTING:
			DoMeltingScreen( state->GetTargetState() );
			break;
		
		default:
			break;
	}
}

void GameEngine::DoMeltingScreen( enum GameState::GAME_STATES state )
{
	this->stateStack.top()->Update();
	this->stateStack.top()->Render( &( this->renderer ) );
	Uint32 *startFrame = this->renderer.GetFrame();
	
	PopState();
	PushNewState( state );
	
	this->stateStack.top()->Update();
	this->stateStack.top()->Render( &( this->renderer ) );
	Uint32 *endFrame = this->renderer.GetFrame();
	
	MeltingScreenState *meltstate = new MeltingScreenState( this );
	
	meltstate->Init( startFrame, endFrame, this->Config.screenWidth, this->Config.screenHeight );
	
	PushState( meltstate );
}

void GameEngine::Run()
{
	this->running = true;
	
	while( running )
	{
		Uint32 start = SDL_GetTicks();
		
		UpdateStateStack();
		
		GameState *state = this->stateStack.top();
		
		this->input.Update();
		
		SDL_Event event;
		while( SDL_PollEvent( &event ) )
			state->HandleEvent( &event );
		
		state->HandleInput( &( this->input ) );
		state->Update();
		state->Render( &( this->renderer ) );
		
		this->renderer.RenderFPS();
		this->renderer.SwapBuffers();
		
		Uint32 elapsed = SDL_GetTicks() - start;
		
		if( elapsed < MILLIS_PER_TICK )
			SDL_Delay( MILLIS_PER_TICK - elapsed );
		
		this->FPSCounter.Update( elapsed );
		
		float avgmillis = this->FPSCounter.GetAverageMillis();
		
		this->renderer.SetFPS( 1000 / avgmillis, avgmillis );
	}
	
	this->mixer.Quit();
	this->renderer.Quit();
	
	while( !this->stateStack.empty() )
	{
		GameState *state = this->stateStack.top();
		this->stateStack.pop();
		delete state;
	}
}

void GameEngine::Exit()
{
	this->running = false;
}
