/*
	Game state interface, based on State OOP pattern.
	Author: Jan Tatarynowicz
*/

#ifndef _GAME_STATE_H_
#define _GAME_STATE_H_

#include "Def.h"

class GameState
{
	public:
		enum GAME_STATES
		{
			MENU_STATE,
			LEVEL_STATE,
			MELTING_SCREEN_STATE,
			
			NUM_GAME_STATES
		};
		
		// Messages to the game state machine
		// any state may request pop, push, etc.
		enum MESSAGES
		{
			NO_MESSAGE,
			MESSAGE_POP,
			MESSAGE_PUSH,
			MESSAGE_SWITCH,
			MESSAGE_MELTING,
			
			NUM_MESSAGES
		};
		
		GameState( GameEngine *game ){ this->game = game; }
		virtual ~GameState(){};
		
		virtual void Init(){};
		
		virtual void HandleEvent( SDL_Event *event ){};
		virtual void HandleInput( Input *input ){};
		virtual void Update(){};
		
		/*
			Renders game state, based on Visitor OOP pattern.
			Gets pointer to renderer.
		*/
		virtual void Render( Renderer *renderer ){};
		
		/*
			Returns message code to game state machine.
		*/
		enum MESSAGES PollMessage() { enum MESSAGES msg = this->message; this->message = NO_MESSAGE; return msg; }
		enum GAME_STATES GetTargetState() { return this->targetState; }
	
	protected:
		GameEngine *game;
		
		enum MESSAGES message = NO_MESSAGE;
		
		// Requested state to be pushed/switched to
		// used with MESSAGE_PUSH, MESSAGE_SWITCH and MESSAGE_MELTING messages
		enum GAME_STATES targetState = MENU_STATE;
};

#endif // _GAME_STATE_H_
