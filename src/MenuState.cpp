/*
	Author: Jan Tatarynowicz
*/

#include "MenuState.h"
#include "Renderer.h"

MenuState::MenuState( GameEngine *game ) : GameState( game )
{
	this->playingState = NULL;
}

MenuState::MenuState( GameEngine *game, GameState *playingState ) : GameState( game )
{
	this->playingState = playingState;
}

void MenuState::Init()
{
	this->menuIndex = 0;
	
	for( int i = 0; i < NUM_OPTIONS; i++ )
		this->menu.push_back( OPTIONS_NAMES[ i ] );
	
	this->startTime = SDL_GetTicks();
}

void MenuState::MenuSelect()
{
	switch( this->menuIndex )
	{
		case MENU_OPTIONS::OPTION_NEW_GAME:
			this->message = MESSAGE_MELTING;
			this->targetState = LEVEL_STATE;
			break;
		
		case MENU_OPTIONS::OPTION_QUIT:
			this->game->Exit();
			break;
		
		default:
			break;
	}
}

void MenuState::HandleEvent( SDL_Event *event )
{
	if( event->type == SDL_KEYDOWN )
	{
		if( event->key.keysym.sym == SDLK_ESCAPE )
			this->message = MESSAGE_POP;
		else if( event->key.keysym.sym == SDLK_UP )
		{
			if( this->menuIndex == 0 )
				this->menuIndex = MENU_OPTIONS::NUM_OPTIONS - 1;
			else
				this->menuIndex--;
		}
		else if( event->key.keysym.sym == SDLK_DOWN )
		{
			if( this->menuIndex == MENU_OPTIONS::NUM_OPTIONS - 1 )
				this->menuIndex = 0;
			else
				this->menuIndex++;
		}
	}
}

void MenuState::HandleInput( Input *input )
{
	if( input->ActionPerformed( Input::ACTIONS::ACTION_MENU_SELECT ) )
		MenuSelect();
	
	if( this->playingState != NULL )
		this->playingState->HandleInput( input );
}

void MenuState::Update()
{
	if( this->playingState != NULL )
		this->playingState->Update();
}

void MenuState::Render( Renderer *renderer )
{
	if( this->playingState != NULL )
		this->playingState->Render( renderer );
	renderer->Render( this );
}

MenuState::~MenuState()
{
	this->menu.clear();
}