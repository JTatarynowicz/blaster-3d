/*
	Author: Jan Tatarynowicz
*/

#include "Enemy.h"
#include "Player.h"

#include <cmath>

#include "Def.h"
#include "Map.h"

#include "AIStrategyPatrol.h"

Enemy::Enemy(	enum THING_TYPES type,
				enum ENEMY_TYPES enemyType,
				enum PICKUPS pickup,
				Map *map,
				float x,
				float y,
				float angle,
				float width,
				float height ) :
Thing( THING_TYPES::THING_TYPE_ENEMY, enemyType, pickup, map, x, y, 0.0f, 0.0f, 0.075f, angle, width, height )
{
	this->enemyType = enemyType;
	this->pickup = pickup;
	this->action = ACTIONS::ACTION_WALK;
	this->actionTime = SDL_GetTicks();
	
	this->width = this->spriteWidth = 0.5f;
	this->height = this->spriteHeight = 0.65f;
	
	this->health = MAX_HEALTH;

	this->strategy = new AIStrategyPatrol( this );
}

Enemy::~Enemy()
{
	if( this->strategy != NULL )
		delete this->strategy;
}

void Enemy::PerformAction()
{
	switch( this->action )
	{
		case ACTIONS::ACTION_IDLE:
			break;
		
		case ACTIONS::ACTION_WALK:
			break;
		
		case ACTIONS::ACTION_SHOOT:
			if( ( int ) SDL_GetTicks() - ( int ) this->actionTime >= ( int ) this->weapons[ this->weapon ]->GetShotTime() )
			{
				this->action = ACTIONS::ACTION_WALK;
				this->actionTime = SDL_GetTicks();
				this->shot = false;
			}
			break;
		
		case ACTIONS::ACTION_HIDE_WEAPON:
			if( SDL_GetTicks() - this->actionTime >= ACTION_TIME_HIDE_WEAPON )
			{
				DrawWeapon();
			}
			break;
		
		case ACTIONS::ACTION_DRAW_WEAPON:
			if( SDL_GetTicks() - this->actionTime >= ACTION_TIME_DRAW_WEAPON )
			{
				this->action = ACTIONS::ACTION_NONE;
				this->actionTime = SDL_GetTicks();
			}
			break;
		
		case ACTIONS::ACTION_DIE:
			break;
		
		default:
			break;
	}
}

int Enemy::CheckCollision()
{
	// Collision box is a square of size 2 * COLLISION_DISTANCE
	// Checks both axis separately, if new position including velocity results in collision
	// velocity in this axis is zeroed
	
	int collides = 0;
	
	// Right side
	float rx = this->x + COLLISION_DISTANCE;
	// Left side
	float lx = this->x - COLLISION_DISTANCE;
	// Upper side
	float uy = this->y - COLLISION_DISTANCE;
	// Down (bottom) side
	float dy = this->y + COLLISION_DISTANCE;
	
	if( this->dy < 0.0f )
	{
		if
		(		this->map->Inspect( lx / GRID_SIZE, ( uy + this->dy ) / GRID_SIZE ) > 0
			||	this->map->Inspect( rx / GRID_SIZE, ( uy + this->dy ) / GRID_SIZE ) > 0
		)
		{
			this->dy = 0.0f;
			collides = 1;
		}
	}
	else if( this->dy > 0.0f )
	{
		if
		(		this->map->Inspect( lx / GRID_SIZE, ( dy + this->dy ) / GRID_SIZE ) > 0
			||	this->map->Inspect( rx / GRID_SIZE, ( dy + this->dy ) / GRID_SIZE ) > 0
		)
		{
			this->dy = 0.0f;
			collides = 1;
		}
	}
	
	if( this->dx < 0.0f )
	{
		if
		(		this->map->Inspect( ( lx + this->dx ) / GRID_SIZE, uy / GRID_SIZE ) > 0
			||	this->map->Inspect( ( lx + this->dx ) / GRID_SIZE, dy / GRID_SIZE ) > 0
		)
		{
			this->dx = 0.0f;
			collides = 1;
		}
	}
	else if( this->dx > 0.0f )
	{
		if
		(		this->map->Inspect( ( rx + this->dx ) / GRID_SIZE, uy / GRID_SIZE ) > 0
			||	this->map->Inspect( ( rx + this->dx ) / GRID_SIZE, dy / GRID_SIZE ) > 0
		)
		{
			this->dx = 0.0f;
			collides = 1;
		}
	}
	
	return collides;
}

void Enemy::GiveWeapon( enum Weapon::WEAPONS weapon )
{
	Thing::GiveWeapon( weapon );
	HideWeapon( weapon );
}

void Enemy::Respawn( float x, float y, float angle )
{
	this->x = x;
	this->y = y;
	this->angle = angle;
	this->dx = this-> dy = 0.0f;
	
	this->health = MAX_HEALTH;
	
	this->speed = 0.075f;//0.1f;
	
	this->action = ACTIONS::ACTION_WALK;
	this->actionTime = SDL_GetTicks();
	
	ResetWeapons();
	
	if( this->strategy != NULL )
	{
		delete this->strategy;
		this->strategy = new AIStrategyPatrol( this );
	}
}

void Enemy::Update( Player *player, Thing *observer, std::vector< Thing* > *things )
{
	Thing::Update( player, observer, things );
	
	PerformAction();
	
	this->strategy->Update( player, things );
	
	float dx = cos( Radians( this->moveAngle ) );
	float dy = -sin( Radians( this->moveAngle ) );
	
	if( health > 0 )
	{
		this->dx = dx * this->speed / ( FPS / 30.0f );
		this->dy = dy * this->speed / ( FPS / 30.0f );
	}
	else
	{
		float friction = 0.95f;
		
		this->dy = this->dy * friction + dy * this->speed / ( FPS / 30.0f );
		this->dx = this->dx * friction + dx * this->speed / ( FPS / 30.0f );
	}
	
	if( !this->move )
		this->dx = this->dy = 0.0f;
	
	if( this->moveAngle < 0.0f )
		this->moveAngle = 360.0f + this->moveAngle;
	else if( this->moveAngle >= 360.0f )
		this->moveAngle = 360.0f - this->moveAngle;
	
	if( this->angle < 0.0f )
		this->angle = 360.0f + this->angle;
	else if( this->angle >= 360.0f )
		this->angle = 360.0f - this->angle;
	
	CheckCollision();
	
	if( this->health <= 0 && this->action != ACTION_DIE )
	{
		this->action = ACTIONS::ACTION_DIE;
		this->actionTime = SDL_GetTicks();
	}
	
	this->x += this->dx;
	this->y += this->dy;
}
