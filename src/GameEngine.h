/*
	A core class which handles main game mechanisms:
	renderer, game loop, sound mixer, input handler and game state machine.
	Author: Jan Tatarynowicz
*/

#ifndef _GAME_ENGINE_H_
#define _GAME_ENGINE_H_

#include "Def.h"
#include "Renderer.h"
#include "Mixer.h"
#include "GameState.h"
#include "Input.h"

//#define FPS 60//30
#define MILLIS_PER_TICK 1000 / FPS

class GameEngine
{
	public:
		GameEngine();
		
		/*
			Requests the sound mixer to play a sound.
			Gets:
				- Sound code,
				- Distance between observer (listener) and sound source.
		*/
		void PlaySound( enum Mixer::SOUNDS sound, float distance = 0.0f );
		
		/*
			Initializes game engine and it's mechanisms.
			Reads configuration file.
		*/
		void Init();
		
		/*
			Starts the game loop.
		*/
		void Run();
		
		/*
			Closes the game.
		*/
		void Exit();
	
	private:
		/*
			Opens config file and loads configuration.
			Sets and saves a default configuration
			if there is no config file.
			Gets path to the file.
		*/
		void LoadConfig( const char *path );
		
		/*
			Opens config file and saves current configuration there.
			Gets path to the file.
		*/
		void SaveConfig( const char *path );
		
		void SetDefaultConfig();
		
		/*
			Polls messages from current game state and pops/pushes/switches states
			if necessary.
		*/
		void UpdateStateStack();
		
		/*
			Pushes new game state to the state stack making it current state.
			Gets a game state code.
		*/
		void PushNewState( enum GameState::GAME_STATES state );
		
		/*
			Pushes an existing game state to the state stack making it current state.
			Gets a pointer to the game state.
		*/
		void PushState( GameState *state );
		
		/*
			Pops a game state from tge top of stack.
			Puts new main menu state on top of the stack if it's empty.
		*/
		void PopState();
		
		/*
			Pops current game state from the state stack and pushes new game state.
			Also pushes melting screen state which is temporal and introduces
			graphical effect of melting screen.
			Gets a code to the new state.
		*/
		void DoMeltingScreen( enum GameState::GAME_STATES state );
		
		Renderer renderer;
		Mixer mixer;
		Input input;
		std::stack< GameState* > stateStack;
		
		// Game loop running conditional
		bool running;
		
		// Game configuration
		struct Config_t
		{
			static const int	DEFAULT_SCREEN_WIDTH = 640,
								DEFAULT_SCREEN_HEIGHT = 480;
			
			static const bool	DEFAULT_FULLSCREEN = false;
			static const bool	DEFAULT_RENDER_FLATS = true;
			static const bool	DEFAULT_FORCE_ASPECT_RATIO = true;
			
			static const int	DEFAULT_SOUND_VOLUME = 128;
			static const int	DEFAULT_MUSIC_VOLUME = 0;
			
			const char *		CONFIG_FILE_PATH = "config.ini";
			
			int					screenWidth = DEFAULT_SCREEN_WIDTH,
								screenHeight = DEFAULT_SCREEN_HEIGHT;
			
			bool				fullscreen = DEFAULT_FULLSCREEN;
			bool				renderFlats = DEFAULT_RENDER_FLATS;
			bool				forceAspectRatio = DEFAULT_FORCE_ASPECT_RATIO;
			
			int					soundVolume = DEFAULT_SOUND_VOLUME;
			int					musicVolume = DEFAULT_MUSIC_VOLUME;
		} Config;
		
		// Measures game performance in average fps/ms per frame
		struct FPSCounter_t
		{
			static const int NUM_SAMPLES = 60;
			
			void Init()
			{
				for( int i = 0; i < NUM_SAMPLES; i++ )
					samples[ i ] = 0;
				
				index = 0;
			}
			
			void Update( int millis )
			{
				samples[ index++ ] = millis;
				
				if( index >= NUM_SAMPLES )
					index = 0;
			}
			
			float GetAverageMillis()
			{
				float millis = 0.0f;
				
				for( int i = 0; i < NUM_SAMPLES; i++ )
					millis += samples[ i ];
				
				return millis / NUM_SAMPLES;
			}
			
			private:
				int samples[ NUM_SAMPLES ];
				int index = 0;
		} FPSCounter;
};

#endif // _GAME_ENGINE_H_