/*
	Declares and defines global types and classes.
	Also includes needed standard and additional C/C++ libraries.
	Author: Jan Tatarynowicz
*/

#ifndef _DEF_H_
#define _DEF_H_

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <cmath>
#include <vector>
#include <string>
#include <ctime>
#include <tuple>
#include <stack>
#include <set>
#include <queue>
#include <functional>

#include <SDL.h>
//#include <SDL2/SDL.h>

#include <SDL2/SDL_ttf.h>

#include <SDL_mixer.h>

#define PI 3.141594f
#define FULL_ANGLE ( 2 * PI )

#define Radians( x ) ( ( x ) * ( PI / 180.0f ) )

#define COLLISION_DISTANCE 0.2f

#define GRID_SIZE 1

#define MOUSE_SENSITIVITY 0.15f

#define FPS 60

class Input;
class Renderer;
class GameEngine;
class Player;
class Thing;
class Enemy;
class Projectile;
class AIStrategy;
class AIStrategyPatrol;
class Map;

class GameState;
class MenuState;
class LevelState;
class MeltingScreenState;
class FontRenderer;
class Ray;
class LookupTables;

/*
	Bullets and plasmas create this visual effect when
	collided with wall or enemies/player
*/
typedef struct Puff_t
{
	enum PUFF_TYPES
	{
		PUFF_TYPE_BULLET,
		PUFF_TYPE_PLASMA,
		
		NUM_PUFF_TYPES
	};
	
	Puff_t( float x, float y, float z, Uint32 time, enum PUFF_TYPES type )
	{
		this->x = x;
		this->y = y;
		this->z = z;
		this->time = time;
		this->type = type;
	}
	
	enum PUFF_TYPES type;
	
	static const unsigned int DURATION = 300;
	
	float x, y, z;
	
	Uint32 time;
} Puff;

/*
	Rectangle geometry is used mainly for collision/intersection calculations.
*/
class Rect
{
	public:
		float x, y;
		float w, h;
	
		Rect( float x1, float y1, float x2, float y2 )
		{
			this->x = ( x1 < x2 ) ? x1 : x2;
			this->y = ( y1 < y2 ) ? y1 : y2;
			this->w = abs( x1 - x2 );
			this->h = abs( y1 - y2 );
		}
		
		bool Intersects( const Rect *r )
		{
			return
			(
				// intersection in x axis
				(
					( ( this->x >= r->x ) && ( this->x <= ( r->x + r->w ) ) )
					|| ( ( r->x >= this->x ) && ( r->x <= ( this->x + this->w ) ) )
				)
				&&
				// intersection in y axis
				(
					( ( this->y >= r->y ) && ( this->y <= ( r->y + r->h ) ) )
					|| ( ( r->y >= this->y ) && ( r->y <= ( this->y + this->h ) ) )
				)
			);
		}
};

/*
	Nodes used for A* (A star) algorithm for finding shortest path.
*/
typedef struct AStarNode_t
{
	// Map's square coords
	int x, y;
	// A* costs: overall cost, "move to adjacent square" cost, heuristic
	// c = m + h
	int c, m, h;
	// Parent's coords
	int px, py;
	// Tells whether this node is in closed/open list
	bool closed, open;
} AStarNode;

class CheatHandler
{
	public:
		enum CHEAT_CODES
		{
			GOD_MODE,
			GIVE_ALL,
			
			NONE,
			
			NUM_CHEAT_CODES
		};
		
		const char *CHEAT_NAMES[ NUM_CHEAT_CODES ] =
		{
			"iddqd",
			"idkfa",
			"zzzzz"
		};
		
		CheatHandler(){};
		
		void Init()
		{
			for( int i = 0; i < NUM_CHEAT_CODES; i++ )
			{
				cheatStates[ i ] = 0;
				cheatCodeLengths[ i ] = strlen( CHEAT_NAMES[ i ] );
			}
		}
		
		enum CHEAT_CODES HandleEvent( SDL_Event *event )
		{
			enum CHEAT_CODES code = NONE;
			
			if( event->type == SDL_KEYDOWN )
			{
				if( event->key.keysym.sym >= SDLK_a && event->key.keysym.sym <= SDLK_z )
				{
					char key = 'a' + event->key.keysym.sym - SDLK_a;
					
					for( int i = 0; i < NUM_CHEAT_CODES; i++ )
					{
						const char *name = CHEAT_NAMES[ i ];
						
						if( key == name[ cheatStates[ i ] ] )
						{
							cheatStates[ i ] = cheatStates[ i ] + 1;
						}
						else
						{
							cheatStates[ i ] = 0;
						}
					}
					
					for( int i = 0; i < NUM_CHEAT_CODES; i++ )
					{
						if( cheatStates[ i ] == cheatCodeLengths[ i ] )
						{
							for( int j = 0; j < NUM_CHEAT_CODES; j++ )
							{
								cheatStates[ j ] = 0;
							}
							
							return ( enum CHEAT_CODES ) i;
						}
					}
				}
			}
			
			return code;
		}
	
	private:
		int cheatStates[ NUM_CHEAT_CODES ];
		int cheatCodeLengths[ NUM_CHEAT_CODES ];
};

#endif // _DEF_H_
