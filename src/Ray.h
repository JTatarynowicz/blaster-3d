/*
	Implementation of raycasting technique.
	Used for rendering, ai (visibility, intersections) and hitscan shooting.
	Author: Jan Tatarynowicz
*/

#ifndef _RAY_H_
#define _RAY_H_

#include "Def.h"

class Ray
{
	public:
		enum HIT_SIDES
		{
				HIT_SIDE_HORIZONTAL,
				HIT_SIDE_VERTICAL,
				
				NUM_HIT_SIDES
		};
		
		Ray( Map *map );
		
		/*
			Casts a ray from source Thing with given angle to closest wall
			using ray casting algorithm.
			Gets:
				- Starting x coordinate,
				- Starting y coordinate,
				- Given angle,
				- Thing the ray is cast by.
		*/
		void Cast( float x, float y, float angle, Thing *thing );
		
		/*
			Sets coordinates  of position where the ray hit,
			Gets:
				- X hit coordinate,
				- Y hit coordinate.
		*/
		void SetHitPosition( float x, float y ) { this->hx = x; this->hy = y; }
		
		/*
			Returns pair of coordinates (x, y) - position where ray was being cast from.
		*/
		std::pair< float, float > GetOriginPosition() { return std::make_pair( this->ox, this->oy ); }
		/*
			Returns pair of coordinates (x, y) - position where ray hit wall.
		*/
		std::pair< float, float > GetHitPosition() { return std::make_pair( this->hx, this->hy ); }
		
		float GetDistance() { return this->distance; }
		float GetAngle() { return this->angle; }
		float GetProjectedDistance() { return this->projectedDistance; }
		
		enum HIT_SIDES GetHitSide() { return this->hitside; }
		int GetType() { return this->type; }
	
	private:
		Map *map;
		
		// Type of map tile hit by ray
		int type;
		
		// Origin pos
		float ox, oy;
		// Hit pos
		float hx, hy;
		float angle;
		// Length of ray
		float distance;
		// For rendering
		float projectedDistance;
		
		// Which side of wall was hit by ray
		enum HIT_SIDES hitside;
};

#endif // _RAY_H_