/*
	Defines the view during gameplay in level state.
	Author: Jan Tatarynowicz
*/

#ifndef _CAMERA_H_
#define _CAMERA_H_

#include "Def.h"

class Camera
{
	public:
		Camera(){};
		
		void SetObserver( Thing *observer ) { this->observer = observer; }
		
		Thing *GetObserver() { return this->observer; }
	
	private:
		Thing *observer = NULL;
};

#endif // _CAMERA_H_