/*
	Author: Jan Tatarynowicz
*/

#include "Player.h"
#include "Input.h"
#include "Map.h"
#include "Def.h"

#include <cmath>

Player::Player( Map *map, float x, float y, float angle ) :
Thing( THING_TYPES::THING_TYPE_PLAYER, ENEMY_TYPES::ENEMY_TYPE_NONE, PICKUPS::PICKUP_NONE, map, x, y, 0.0f, 0.0f, PLAYER_SPEED, angle, 0.1f, 0.5f )
{
	this->controls.forward = 0;
	this->controls.backward = 0;
	this->controls.right = 0;
	this->controls.left = 0;
	this->controls.shoot = 0;
	
	this->spriteWidth = 0.5f;
	this->spriteHeight = 0.65f;
	
	this->action = ACTIONS::ACTION_NONE;
	this->actionTime = SDL_GetTicks();
	
	this->health = MAX_HEALTH;
}

void Player::CheckCollision()
{
	float rx = this->x + COLLISION_DISTANCE;
	float lx = this->x - COLLISION_DISTANCE;
	float uy = this->y - COLLISION_DISTANCE;
	float dy = this->y + COLLISION_DISTANCE;
	
	if( this->dy < 0.0f )
	{
		if
		(		this->map->Blocks( lx / GRID_SIZE, ( uy + this->dy ) / GRID_SIZE )
			||	this->map->Blocks( rx / GRID_SIZE, ( uy + this->dy ) / GRID_SIZE )
		)
			this->dy = 0.0f;
	}
	else if( this->dy > 0.0f )
	{
		if
		(		this->map->Blocks( lx / GRID_SIZE, ( dy + this->dy ) / GRID_SIZE )
			||	this->map->Blocks( rx / GRID_SIZE, ( dy + this->dy ) / GRID_SIZE )
		)
			this->dy = 0.0f;
	}
	
	if( this->dx < 0.0f )
	{
		if
		(		this->map->Blocks( ( lx + this->dx ) / GRID_SIZE, uy / GRID_SIZE )
			||	this->map->Blocks( ( lx + this->dx ) / GRID_SIZE, dy / GRID_SIZE )
		)
			this->dx = 0.0f;
	}
	else if( this->dx > 0.0f )
	{
		if
		(		this->map->Blocks( ( rx + this->dx ) / GRID_SIZE, uy / GRID_SIZE )
			||	this->map->Blocks( ( rx + this->dx ) / GRID_SIZE, dy / GRID_SIZE )
		)
			this->dx = 0.0f;
	}
}

void Player::Respawn( float x, float y, float angle )
{
	this->x = x;
	this->y = y;
	this->angle = angle;
	this->dx = this-> dy = 0.0f;
	this->speed = PLAYER_SPEED;
	
	this->health = MAX_HEALTH;
	
	this->action = ACTIONS::ACTION_NONE;
	
	ResetWeapons();
}

void Player::PerformAction()
{
	if( this->action == ACTIONS::ACTION_SHOOT )
	{
		if( ( int ) SDL_GetTicks() - ( int ) this->actionTime >= ( int ) this->weapons[ this->weapon ]->GetShotTime() )
		{
			this->action = ACTIONS::ACTION_NONE;
			this->actionTime = SDL_GetTicks();
			this->shot = false;
		}
	}
	else if( this->action == ACTIONS::ACTION_HIDE_WEAPON )
	{
		if( SDL_GetTicks() - this->actionTime >= ACTION_TIME_HIDE_WEAPON )
		{
			DrawWeapon();
		}
	}
	else if( this->action == ACTIONS::ACTION_DRAW_WEAPON )
	{
		if( SDL_GetTicks() - this->actionTime >= ACTION_TIME_DRAW_WEAPON )
		{
			this->action = ACTIONS::ACTION_NONE;
			this->actionTime = SDL_GetTicks();
		}
	}
}

void Player::HandleInput( Input *input )
{
	int mx = input->GetMouseState();
	
	this->angle -= mx * MOUSE_SENSITIVITY;
	
	if( this->angle < 0.0f )
		this->angle = 360.0f + this->angle;
	if( this->angle >= 360.0f )
		this->angle = this->angle - 360.0f;
	
	this->controls.forward = input->ActionPerformed( Input::ACTIONS::ACTION_MOVE_FORWARD ) ? 1 : 0;
	this->controls.backward = input->ActionPerformed( Input::ACTIONS::ACTION_MOVE_BACKWARD ) ? 1 : 0;
	this->controls.right = input->ActionPerformed( Input::ACTIONS::ACTION_MOVE_RIGHT ) ? 1 : 0;
	this->controls.left = input->ActionPerformed( Input::ACTIONS::ACTION_MOVE_LEFT ) ? 1 : 0;
	this->controls.shoot = input->ActionPerformed( Input::ACTIONS::ACTION_SHOOT ) ? 1 : 0;
	
	if(	input->ActionPerformed( Input::ACTIONS::ACTION_SELECT_WEAPON3 )
		&& this->weapons[ Weapon::WEAPONS::SHOTGUN ]->GotWeapon() )
		HideWeapon( Weapon::WEAPONS::SHOTGUN );
	else if(	input->ActionPerformed( Input::ACTIONS::ACTION_SELECT_WEAPON4 )
				&& this->weapons[ Weapon::WEAPONS::MACHINEGUN ]->GotWeapon() )
		HideWeapon( Weapon::WEAPONS::MACHINEGUN );
	else if(	input->ActionPerformed( Input::ACTIONS::ACTION_SELECT_WEAPON6 )
				&& this->weapons[ Weapon::WEAPONS::PLASMAGUN ]->GotWeapon() )
		HideWeapon( Weapon::WEAPONS::PLASMAGUN );
	else if(	input->ActionPerformed( Input::ACTIONS::ACTION_SELECT_WEAPON1 )
			&& this->weapons[ Weapon::WEAPONS::PISTOL ]->GotWeapon() )
		HideWeapon( Weapon::WEAPONS::PISTOL );
}

void Player::Update( Thing *observer )
{
	PerformAction();
	
	this->weapons[ this->weapon ]->Update();
	
	if( this->controls.shoot )
		Shoot();
	
	if( this->angle < 0.0f )
		this->angle = 360.0f + this->angle;
	if( this->angle >= 360.0f )
		this->angle = this->angle - 360.0f;
	
	float dx = 0.0f, dy = 0.0f;
	
	this->moveAngle = this->angle;
	
	if( controls.forward )
	{
		dx += cos( Radians( this->moveAngle ) );
		dy -= sin( Radians( this->moveAngle ) );
	}
	if( this->controls.backward )
	{
		dx -= cos( Radians( this->moveAngle ) );
		dy += sin( Radians( this->moveAngle ) );
	}
	if( this->controls.right )
	{
		dx += sin( Radians( this->moveAngle ) );
		dy += cos( Radians( this->moveAngle ) );
	}
	if( this->controls.left )
	{
		dx -= sin( Radians( this->moveAngle ) );
		dy -= cos( Radians( this->moveAngle ) );
	}
	
	float friction = 0.85f;
	
	this->dy = this->dy * friction + dy * this->speed / ( FPS / 30.0f );
	this->dx = this->dx * friction + dx * this->speed / ( FPS / 30.0f );
	
	this->CheckCollision();
	
	this->x += this->dx;
	this->y += this->dy;
	
	auto opos = observer->GetPosition();
	float ox = std::get< 0 >( opos ), oy = std::get< 1 >( opos );
	
	this->cdistance = sqrt(	( ox - this->x )
							* ( ox - this->x )
							+ ( oy - this->y )
							* ( oy - this->y )
	);
	
	float relangle = atan2( ox - this->x, oy - this->y ) * 180.0f / PI ;
	if( relangle < 0.0f )
		relangle = 360.0f + relangle;
	else if( relangle >= 360.0f )
		relangle = 360.0f - relangle;
	
	this->relativeAngle = relangle;
}
