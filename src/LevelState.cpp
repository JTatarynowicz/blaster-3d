/*
	Author: Jan Tatarynowicz
*/

#include "LevelState.h"
#include "Renderer.h"
#include "GameEngine.h"
#include "Thing.h"
#include "Player.h"
#include "Enemy.h"
#include "Ray.h"
#include "Projectile.h"
#include "Mixer.h"
#include "LookupTables.h"

#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#include <algorithm>

LevelState::LevelState( GameEngine *game ) : GameState( game )
{
	//this->game = game;
}

void LevelState::InitData()
{
	FILE *file = fopen( "data/maps/map.txt", "rb" );
	
	// Load map size and data
	int width, height;
	
	int numfound = fscanf( file, "%d %d", &width, &height );

    if( numfound != 2 )
    {
        printf( "Could not read map width/height correctly\n" );
        exit( -1 );
    }
    else
    {
        printf( "Map size: %d x %d\n", width, height );
    }
	
	char *data = new char[ width * height ];
	char *blocks = new char[ width * height ];
	
	int column, row;
	for( row = 0; row < height; row++ )
	{
		for( column = 0; column < width; column++ )
		{
			int c;
			numfound = fscanf( file, "%d", &c );
			
			if( numfound != 1 )
            {
                printf( "Could not read map data correctly\n" );
                exit( -1 );
            }
            else
            {
                if( c >= 0 )
                    printf( "0%d ", c );
                else
                    printf( "%d ", c );
            }
            
			data[ row * width + column ] = c;
			
			if( c > 0 )
				blocks[ row * width + column ] = 1;
			else
				blocks[ row * width + column ] = 0;
		}
		
		printf( "\n" );
	}
	
	this->map.width = width;
	this->map.height = height;
	this->map.data = data;
	this->map.blocks = blocks;
	
	AStarNode *nodes = new AStarNode[ width * height ];
	
	for( column = 0; column < width; column++ )
	{
		for( row = 0; row < height; row++ )
		{
			int index = row * width + column;
			nodes[ index ].x = column;
			nodes[ index ].y = row;
		}
	}
	
	this->map.nodes = nodes;
	
	this->player = new Player( &( this->map ), 0.0f, 0.0f, 0.0f );
	this->player->SetName( "Player" );
	this->observers.push_back( ( Thing* ) this->player );
	
	// Load map waypoints
	int numwaypoints;
	
	numfound = fscanf( file, "%d", &numwaypoints );
	
	if( numfound != 1 )
    {
        printf( "Could not read map waypoints number correctly\n" );
        exit( -1 );
    }
    else
    {
        printf( "Number of waypoints: %d\n", numwaypoints );
    }
	
	this->map.numwaypoints = numwaypoints;
	
	std::vector< std::pair< int, int > > wpcoords;
	
	for( int i = 0; i < numwaypoints; i++ )
	{
		int x, y;
		
		numfound = fscanf( file, "%d %d", &x, &y );
		
		if( numfound != 2 )
        {
            printf( "Could not read map waypoints correctly\n" );
            exit( -1 );
        }
		
		wpcoords.push_back( std::make_pair( x, y ) );
	}
	
	// Load things
	numfound = fscanf( file, "%d", &numthings );
	
	if( numfound != 1 )
    {
        printf( "Could not read map number of things correctly\n" );
        exit( -1 );
    }
    else
    {
        printf( "Number of things: %d\n", numthings );
    }
	
	for( int i = 0; i < numthings; i++ )
	{
		float x, y;
		int type;
		
		numfound = fscanf( file, "%d %f %f", &type, &x, &y );
		
		if( numfound != 3 )
        {
            printf( "Could not read map thing correctly\n" );
            exit( -1 );
        }
        else
        {
            printf( "%d %f %f\n", type, x, y );
        }
		
		Thing *thing = new Thing(	( enum Thing::THING_TYPES ) type,
									Enemy::ENEMY_TYPES::ENEMY_TYPE_NONE,
									Thing::PICKUPS::PICKUP_NONE,
									&( this->map ),
									x * GRID_SIZE + GRID_SIZE / 2.0f,
									y * GRID_SIZE + GRID_SIZE / 2.0f,
									0.0f,
									0.0f,
									0.0f,
									0.0f,
									0.75f,
									0.95f
		);
		
		this->map.SetBlocks( x, y );
		
		this->things.push_back( thing );
	}
	
	this->map.InitWaypoints( &wpcoords );
	
	// Load player spawners
	int numspawners;
	
	numfound = fscanf( file, "%d", &numspawners );
	
	this->numspawners = numspawners;
	
	if( numfound != 1 )
    {
        printf( "Could not read map number of spawners correctly\n" );
        exit( -1 );
    }
    else
    {
        printf( "Number of spawners: %d\n", numspawners );
    }
	
	for( int i = 0; i < numspawners; i++ )
	{
		int x, y;
		float angle;
		
		numfound = fscanf( file, "%d %d %f", &x, &y, &angle );
		
		if( numfound != 3 )
        {
            printf( "Could not read map spawner correctly\n" );
            exit( -1 );
        }
		
		Thing *spawner = new Thing(	Thing::THING_TYPES::THING_TYPE_SPAWNER,
									Thing::ENEMY_TYPES::ENEMY_TYPE_NONE,
									Thing::PICKUPS::PICKUP_NONE,
									&( this->map ),
									x * GRID_SIZE + ( float ) GRID_SIZE / 2,	// x
									y * GRID_SIZE + ( float ) GRID_SIZE / 2,	// y
									angle,	// angle
									0.1f,
									0.5f
		);
		
		this->spawners.push_back( spawner );
	}
	
	numenemies = numspawners - 1;
	Thing *spawner = this->spawners[ numspawners - 1 ];
	
	auto pos = spawner->GetPosition();
	float px = std::get< 0 >( pos ), py = std::get< 1 >( pos );
	float angle = spawner->GetAngle();
	
	this->player->SetPosition( px, py );
	this->player->SetAngle( angle );
	
	// Spawn enemies
	for( int i = 0; i < numenemies; i++ )
	{
		Thing *spawner = this->spawners[ i ];
		auto pos = spawner->GetPosition();
		float x = std::get< 0 >( pos ), y = std::get< 1 >( pos );
		float angle = spawner->GetAngle();
		
		Enemy *enemy = new Enemy(	Thing::THING_TYPES::THING_TYPE_ENEMY,
									( enum Enemy::ENEMY_TYPES ) 1,
									Thing::PICKUPS::PICKUP_NONE,
									&( this->map ),
									x,	// x
									y,	// y
									angle,	// angle
									0.1f,
									0.5f
		);
		
		if( i < NUM_NAMES )
			enemy->SetName( this->NAMES[ i ] );
		else
			enemy->SetName( this->NAMES[ 0 ] );
		
		this->things.push_back( enemy );
		this->shootables.push_back( enemy );
		this->enemies.push_back( enemy );
		this->observers.push_back( enemy );
	}
	
	this->shootables.push_back( player ); 
	
	// Load pickups
	int numpickups;
	numfound = fscanf( file, "%d", &numpickups );
	
	if( numfound != 1 )
    {
        printf( "Could not read map number of pickups correctly\n" );
        exit( -1 );
    }
    else
    {
        printf( "Number of pickups: %d\n", numpickups );
    }
	
	for( int i = 0; i < numpickups; i++ )
	{
		float x, y;
		int type;
		
		numfound = fscanf( file, "%d %f %f", &type, &x, &y );
		
		if( numfound != 3 )
        {
            printf( "Could not read map pickup correctly\n" );
            exit( -1 );
        }
		
		auto size = Thing::PICKUP_SIZES[ type ];
		float w = std::get< 0 >( size ), h = std::get< 1 >( size );
		
		Thing *thing = new Thing(	Thing::THING_TYPES::THING_TYPE_PICKUP,
									Enemy::ENEMY_TYPES::ENEMY_TYPE_NONE,
									( enum Thing::PICKUPS ) type,
									&( this->map ),
									x,
									y,
									0.0f,
									0.0f,
									0.0f,
									0.0f,
									w,
									h
		);
		
		this->things.push_back( thing );
		this->pickups.push_back( thing );
	}
	
	fclose( file );
	
	this->observer = 0;
	this->camera.SetObserver( this->observers[ this->observer ] );
	
	this->cheatHandler.Init();
}

void LevelState::Init()
{
	this->InitData();
}

std::vector< std::pair< const char*, int > > LevelState::GetStats()
{
	std::vector< std::pair< const char*, int > > stats;
	
	for( Enemy *enemy : this->enemies )
	{
		stats.push_back( std::make_pair( enemy->GetName(), enemy->GetFrags() ) );
	}
	
	stats.push_back( std::make_pair( this->player->GetName(), this->player->GetFrags() ) );
	
	return stats;
}

void LevelState::UpdateThings()
{
	// Sort things by their distance to observer
	for( int i = 0; i < ( int ) this->things.size(); i++ )
		this->things[ i ]->Update( this->player, this->observers[ this->observer ], &this->shootables );
	
	std::sort(	this->things.begin(),
				this->things.end(),
				[]( Thing *a, Thing *b ) -> bool { return a->GetDistance() > b->GetDistance(); } );
	
	for( int i = 0; i < ( int ) this->things.size(); i++ )
	{
		Thing *thing = this->things[ i ];
		
		if( thing->Dispose() )
		{
			// Projectiles turn to puffs
			if( thing->GetType() == Thing::THING_TYPES::THING_TYPE_PROJECTILE )
			{
				Uint32 time = SDL_GetTicks();
				auto pos = thing->GetPosition();
				float x = std::get< 0 >( pos ), y = std::get< 1 >( pos );
				float z = 0.5f;
				Puff *puff = new Puff( x, y, z, time, Puff::PUFF_TYPES::PUFF_TYPE_PLASMA );
				this->puffs.push_back( puff );
			}
			
			this->things.erase( this->things.begin() + i-- );
			delete thing;
			continue;
		}
		
		// Handle projectile-shootable hit
		if( thing->GetType() == Thing::THING_TYPES::THING_TYPE_PROJECTILE )
		{
			for( int j = 0; j < ( int ) this->shootables.size(); j++ )
			{
				Thing *shootable = this->shootables[ j ];
				
				if( shootable->GetHealth() <= 0
					|| shootable == thing->GetOriginator() )
					continue;
				
				if( thing->Collides( shootable ) )
				{
					shootable->DealDamage( thing->GetDamage() );
					thing->SetHit();
					if( shootable->IsDead() )
					{
						thing->GetOriginator()->GiveFrag();
						
						char buff[ 100 ];
						
						sprintf( buff, "%s killed %s with plasmagun", thing->GetOriginator()->GetName(), shootable->GetName() );
						
						this->msgChannel.PutMessage( buff );
					}
				}
			}
		}
		
		// Handle player picking up items
		if( thing->WhichPickup() != Thing::PICKUPS::PICKUP_NONE && thing->GetAction() != Thing::ACTIONS::ACTION_RESPAWN )
		{
			if( thing->GetDistance() <= 0.5f )
			{
			    enum Mixer::SOUNDS sound = Mixer::SOUNDS::NONE;
			    
				char buff[ 100 ];
				
				switch( thing->WhichPickup() )
				{
					case Thing::PICKUPS::BULLETBOX:
						if( this->player->MaxAmmo( Weapon::WEAPONS::MACHINEGUN ) )
						{
							continue;
						}
						sound = Mixer::SOUNDS::PICKUP;
						this->player->GiveAmmo( Weapon::WEAPONS::MACHINEGUN, Weapon::PICKUP_AMMO[ Weapon::WEAPONS::MACHINEGUN ] );
						sprintf( buff, "Got Bullet box" );
						break;
					
					case Thing::PICKUPS::SHELLBOX:
						if( this->player->MaxAmmo( Weapon::WEAPONS::SHOTGUN ) )
						{
							continue;
						}
						sound = Mixer::SOUNDS::PICKUP;
						this->player->GiveAmmo( Weapon::WEAPONS::SHOTGUN, Weapon::PICKUP_AMMO[ Weapon::WEAPONS::SHOTGUN ] );
						sprintf( buff, "Got Shell box" );
						break;
					
					case Thing::PICKUPS::CELLS:
						if( this->player->MaxAmmo( Weapon::WEAPONS::PLASMAGUN ) )
						{
							continue;
						}
						sound = Mixer::SOUNDS::PICKUP;
						this->player->GiveAmmo( Weapon::WEAPONS::PLASMAGUN, Weapon::PICKUP_AMMO[ Weapon::WEAPONS::PLASMAGUN ] );
						sprintf( buff, "Got Cells" );
						break;
					
					case Thing::PICKUPS::SMALLMEDKIT:
						if( this->player->GetHealth() >= Thing::MAX_HEALTH )
						{
							continue;
						}
						sound = Mixer::SOUNDS::PICKUP;
						this->player->Heal( 20 );
						sprintf( buff, "Got Small medkit" );
						break;
					
					case Thing::PICKUPS::LARGEMEDKIT:
						if( this->player->GetHealth() >= Thing::MAX_HEALTH )
						{
							continue;
						}
						sound = Mixer::SOUNDS::PICKUP;
						this->player->Heal( 50 );
						sprintf( buff, "Got Large medkit" );
						break;
					
					case Thing::PICKUPS::MACHINEGUN:
					    sound = Mixer::SOUNDS::PICKUP_WEAPON;
						this->player->GiveWeapon( Weapon::WEAPONS::MACHINEGUN );
						sprintf( buff, "Got Machinegun" );
						break;
					
					case Thing::PICKUPS::SHOTGUN:
					    sound = Mixer::SOUNDS::PICKUP_WEAPON;
						this->player->GiveWeapon( Weapon::WEAPONS::SHOTGUN );
						sprintf( buff, "Got Shotgun" );
						break;
					
					case Thing::PICKUPS::PLASMAGUN:
					    sound = Mixer::SOUNDS::PICKUP_WEAPON;
						this->player->GiveWeapon( Weapon::WEAPONS::PLASMAGUN );
						sprintf( buff, "Got Plasmagun" );
						break;
					
					default:
						continue;
						break;
				}
				
				if( this->observers[ this->observer ] == this->player )
				{
					this->msgChannel.PutMessage( buff );
					this->game->PlaySound( sound );
				}
				
				thing->PickUp();
			}
		}
	}
	
	for( Thing *enemy : this->enemies )
	{
		if( enemy->GetAction() == Thing::ACTIONS::ACTION_SHOOT )
			HandleShot( enemy );
		
		auto pos = enemy->GetPosition();
		float tx = std::get< 0 >( pos ), ty = std::get< 1 >( pos );
		
		// Handle enemies picking up items
		for( Thing *pickup : this->pickups )
		{
			if( pickup->GetAction() == Thing::ACTIONS::ACTION_RESPAWN
				|| enemy->IsDead() )
				continue;
			
			pickup->SetOpDistance( tx, ty );
			float dist = pickup->GetOpDistance();
			
			if( dist <= 0.5f )
			{
				char buff[ 100 ];
				
				switch( pickup->WhichPickup() )
				{
					case Thing::PICKUPS::BULLETBOX:
						if( enemy->MaxAmmo( Weapon::WEAPONS::MACHINEGUN ) )
						{
							continue;
						}
						enemy->GiveAmmo( Weapon::WEAPONS::MACHINEGUN, Weapon::PICKUP_AMMO[ Weapon::WEAPONS::MACHINEGUN ] );
						sprintf( buff, "Got Bullet box" );
						break;
					
					case Thing::PICKUPS::SHELLBOX:
						if( enemy->MaxAmmo( Weapon::WEAPONS::SHOTGUN ) )
						{
							continue;
						}
						enemy->GiveAmmo( Weapon::WEAPONS::SHOTGUN, Weapon::PICKUP_AMMO[ Weapon::WEAPONS::SHOTGUN ] );
						sprintf( buff, "Got Shell box" );
						break;
					
					case Thing::PICKUPS::CELLS:
						if( enemy->MaxAmmo( Weapon::WEAPONS::PLASMAGUN ) )
						{
							continue;
						}
						enemy->GiveAmmo( Weapon::WEAPONS::PLASMAGUN, Weapon::PICKUP_AMMO[ Weapon::WEAPONS::PLASMAGUN ] );
						sprintf( buff, "Got Cells" );
						break;
					
					case Thing::PICKUPS::SMALLMEDKIT:
						if( enemy->GetHealth() >= Thing::MAX_HEALTH )
						{
							continue;
						}
						enemy->Heal( 20 );
						sprintf( buff, "Got Small medkit" );
						break;
					
					case Thing::PICKUPS::LARGEMEDKIT:
						if( enemy->GetHealth() >= Thing::MAX_HEALTH )
						{
							continue;
						}
						enemy->Heal( 50 );
						sprintf( buff, "Got Large medkit" );
						break;
					
					case Thing::PICKUPS::MACHINEGUN:
						enemy->GiveWeapon( Weapon::WEAPONS::MACHINEGUN );
						sprintf( buff, "Got Machinegun" );
						break;
					
					case Thing::PICKUPS::SHOTGUN:
						enemy->GiveWeapon( Weapon::WEAPONS::SHOTGUN );
						sprintf( buff, "Got Shotgun" );
						break;
					
					case Thing::PICKUPS::PLASMAGUN:
						enemy->GiveWeapon( Weapon::WEAPONS::PLASMAGUN );
						sprintf( buff, "Got Plasmagun" );
						break;
					
					default:
						continue;
						break;
				}
				
				if( this->observers[ this->observer ] == enemy )
					this->msgChannel.PutMessage( buff );
				
				pickup->PickUp();
			}
		}
	}
	
	// Handle deaths
	Uint32 time = SDL_GetTicks();
	for( Enemy *enemy : this->enemies )
	{
		if( enemy->GetAction() == Thing::ACTIONS::ACTION_DIE )
		{
			if( time - enemy->GetActionTime() >= Thing::RESPAWN_TIME )
				Respawn( ( Thing* ) enemy );
		}
	}
	
	if( player->GetAction() == Thing::ACTIONS::ACTION_DIE )
	{
		if( time - player->GetActionTime() >= Thing::RESPAWN_TIME )
			Respawn( ( Thing* ) player );
	}
}

void LevelState::SwitchObserver()
{
	this->observer++;
	
	if( this->observer >= ( int ) this->observers.size() )
	{
		this->observer = 0;
	}
	this->camera.SetObserver( this->observers[ this->observer ] );
}

void LevelState::HandleEvent( SDL_Event *event )
{
	if( event->type == SDL_KEYDOWN )
	{
		if( event->key.keysym.sym == SDLK_ESCAPE )
		{
			this->message = MESSAGE_PUSH;
			this->targetState = MENU_STATE;
		}
		else if( event->key.keysym.sym == SDLK_c )
		{
			SwitchObserver();
		}
		else if( event->key.keysym.sym == SDLK_p )
		{
			this->observer = 0;
			this->camera.SetObserver( this->observers[ this->observer ] );
		}
		else if( event->key.keysym.sym == SDLK_TAB )
		{
			this->RequestShowStats = !this->RequestShowStats;
		}
	}
	
	enum CheatHandler::CHEAT_CODES code = this->cheatHandler.HandleEvent( event );
	
	char buff[ 50 ];
	
	switch( code )
	{
		case CheatHandler::CHEAT_CODES::GOD_MODE:
			sprintf( buff, "iddqd" );
			this->player->SwitchDestructibility();
			this->msgChannel.PutMessage( buff );
			break;
		
		case CheatHandler::CHEAT_CODES::GIVE_ALL:
			sprintf( buff, "idkfa" );
			this->msgChannel.PutMessage( buff );
			break;
		
		default:
			break;
	}
}

void LevelState::Respawn( Thing *thing )
{
	int roll = rand() % this->spawners.size();
	Thing *spawner = this->spawners[ roll ];
	auto pos = spawner->GetPosition();
	float x = std::get< 0 >( pos ), y = std::get< 1 >( pos );
	float angle = spawner->GetAngle();
	thing->Respawn( x, y, angle );
}
		
void LevelState::HandleInput( Input *input )
{
	this->player->HandleInput( input );
}

void LevelState::HandleShot( Thing *shooter )
{
	if( !shooter->HasShot() )
	{
		shooter->SetShot();
		
		enum Mixer::SOUNDS sound = Mixer::SOUNDS::NONE;
		
		auto pos = shooter->GetPosition();
		float x = std::get< 0 >( pos ), y = std::get< 1 >( pos );
		float pangle = shooter->GetAngle();
		
		Weapon *weapon = shooter->GetWeapon();
		
		switch( shooter->GetWeapon()->Which() )
		{
			case Weapon::WEAPONS::PISTOL:
				sound = Mixer::SOUNDS::SHOT_PISTOL;
				break;
			
		    case Weapon::WEAPONS::MACHINEGUN:
				sound = Mixer::SOUNDS::SHOT_MACHINEGUN;
				break;
			
			case Weapon::WEAPONS::SHOTGUN:
				sound = Mixer::SOUNDS::SHOT_SHOTGUN;
				break;
			
			case Weapon::WEAPONS::PLASMAGUN:
				sound = Mixer::SOUNDS::SHOT_PLASMAGUN;
				break;
			
			default:
				sound = Mixer::SOUNDS::NONE;
				break;
		}
		
		enum Weapon::SHOT_TYPES type = weapon->ShotType();
		
		if( type == Weapon::SHOT_TYPES::HITSCAN )
		{
			std::vector< Ray > *rays = weapon->ShootRays( &( this->map ), x, y, pangle, shooter );
			int damage = weapon->GetDamage();
			
			if( rays != NULL )
			{
				Uint32 time = SDL_GetTicks();
				
				// Sort shootables by their distance to shooter
				for( int i = 0; i < ( int ) this->shootables.size(); i++ )
				{
					Thing *thing = this->shootables[ i ];
					thing->SetOpDistance( x, y );
				}
				
				std::sort(	this->shootables.begin(),
							this->shootables.end(),
							[]( Thing *a, Thing *b ) -> bool { return a->GetOpDistance() > b->GetOpDistance(); } );
				
				for( int i = 0; i < ( int ) rays->size(); i++ )
				{
					Ray ray = ( *rays )[ i ];
					for( int j = this->shootables.size() - 1; j >= 0; j-- )
					{
						Thing *thing = this->shootables[ j ];
						
						if(	thing->GetAction() == Thing::ACTIONS::ACTION_DIE
							|| thing == shooter
						)
							continue;
						
						float raydistance = ray.GetDistance();
						if( thing->GetOpDistance() <= raydistance )
						{
							if( thing->Intersects( &ray ) )
							{
								auto tpos = thing->GetPosition();
								float tx = std::get< 0 >( tpos ), ty = std::get< 1 >( tpos );
								ray.SetHitPosition( tx, ty );
								thing->DealDamage( damage );
								
								if( thing->IsDead() )
								{
									char buff[ 100 ];
									switch( shooter->GetWeapon()->Which() )
									{
										case Weapon::WEAPONS::MACHINEGUN:
											shooter->GiveFrag();
											sprintf( buff, "%s killed %s with Machinegun", shooter->GetName(), thing->GetName() );
											break;
										
										case Weapon::WEAPONS::SHOTGUN:
											shooter->GiveFrag();
											sprintf( buff, "%s killed %s with Shotgun", shooter->GetName(), thing->GetName() );
											break;
										
										default:
											shooter->GiveFrag();
											sprintf( buff, "%s killed %s with Pistol", shooter->GetName(), thing->GetName() );
											break;
									}
									
									this->msgChannel.PutMessage( buff );
								}
								
								break;
							}
						}
						
					}
					
					// Make bullet puff
					auto rpos = ray.GetHitPosition();
					float rx = std::get< 0 >( rpos ), ry = std::get< 1 >( rpos );
					float rz = 0.4f + ( float ) rand() / ( float ) RAND_MAX * 0.2f;
					Puff *puff = new Puff( rx, ry, rz, time, Puff::PUFF_TYPES::PUFF_TYPE_BULLET );
					this->puffs.push_back( puff );
				}
			
				delete rays;
			}
		}
		else if( type == Weapon::SHOT_TYPES::PROJECTILE )
		{
			float pangle = shooter->GetAngle();
			std::vector< Projectile* > *projs = weapon->ShootProjectiles( &( this->map ), x, y, pangle );
			
			while( !projs->empty() )
			{
				projs->back()->SetOriginator( shooter );
				projs->back()->Update( this->player, this->observers[ this->observer ], &this->shootables );
				this->things.push_back( ( Thing* ) projs->back() );
				projs->pop_back();
			}
			
			delete projs;
		}
		
		if( shooter == this->observers[ this->observer ] )
		{
			this->game->PlaySound( sound );
		}
		else
		{
			auto opos = this->observers[ this->observer ]->GetPosition();
			float ox = std::get< 0 >( opos ), oy = std::get< 1 >( opos );
			
			float dist = sqrt( ( x - ox ) * ( x - ox ) + ( y - oy ) * ( y - oy ) );
			this->game->PlaySound( sound, dist );
		}
	}
}

void LevelState::Update()
{
	this->player->Update( this->observers[ this->observer ] );
	
	if( this->player->GetAction() == Thing::ACTIONS::ACTION_SHOOT )
		HandleShot( ( Thing* ) this->player );
	
	UpdateThings();
	
	this->msgChannel.Update();
	
	Uint32 time = SDL_GetTicks();
	
	for( int i = 0; i < ( int ) this->puffs.size(); i++ )
	{
		Puff *puff = this->puffs[ i ];
		if( time - puff->time >= Puff::DURATION )
		{
			this->puffs.erase( this->puffs.begin() + i );
			delete puff;
			i--;
		}
	}
}

void LevelState::Render( Renderer *renderer )
{
	renderer->Render( this );
}

LevelState::~LevelState()
{
	delete this->player;
	
	for( int i = 0; i < ( int ) this->things.size(); i++ )
		delete this->things[ i ];
	this->things.clear();
	
	this->enemies.clear();
	
	this->shootables.clear();
	
	for( int i = 0; i < ( int ) this->projectiles.size(); i++ )
		delete this->projectiles[ i ];
	this->projectiles.clear();
	
	this->pickups.clear();
	
	for( int i = 0; i < ( int ) this->puffs.size(); i++ )
		delete this->puffs[ i ];
	this->puffs.clear();
	
	this->observers.clear();
	
	for( int i = 0; i < ( int ) this->spawners.size(); i++ )
		delete this->spawners[ i ];
	this->spawners.clear();
}
