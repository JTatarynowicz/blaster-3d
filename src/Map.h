/*
	Defines the shape and content of level tile map.
	Also contains waypoints and path searching algorithms.
	Author: Jan Tatarynowicz
*/

#ifndef _MAP_H_
#define _MAP_H_

#include "Def.h"

// A* move to adjacent square cost:
// vertical/horizontal, diagonal
#define VH_COST 10
#define DIAG_COST 14

#define MAX_COST 999999

class Map
{
	public:
		Map()
		{
			
		}
		
		~Map();
		
		int width, height;
		// Walls, floor/ceiling data
		char *data;
		// Blocking areas
		char *blocks;
		
		// Nodes used in A star algorithm
		AStarNode *nodes;
		
		int numwaypoints = 0;
		
		// Areas for bots to travel between
		struct Waypoint
		{
			// Coords in tile map
			int x, y;
			
			// Paths to other existing waypoints
			// generated by path searching algorithm
			std::vector< std::pair< int, int > > **paths;
		} *waypoints;
		
		/*
			Generates waypoints and paths between them.
			Gets vector of pairs (x, y) - waypoint coordinates.
		*/
		void InitWaypoints( std::vector< std::pair< int, int > > *wpcoords );
		
		/*
			Gives info about kind of geometry on given map tile.
			Gets:
				- Map tile column number,
				- Map tile row number.
			Returns:
				- Value > 0 - wall code,
				- Value <= 0 - floor/ceiling code.
		*/
		int Inspect( int x, int y );
		
		/*
			Tells whether given map tile blocks any movement.
			Gets:
				- Map tile column number,
				- Map tile row number.
			Returns:
				- 1 if given map tile blocks any movement,
				- 0 otherwise.
		*/
		int Blocks( int x, int y );
		
		/*
			Sets given map tile to the one blocking any movement.
			Gets:
				- Map tile column number,
				- Map tile row number.
		*/
		void SetBlocks( int x, int y );
		
		/*
			Searches for shortest path between two given points, uses A* (A star) algorithm.
			Gets:
				- Map tile column number of starting point,
				- Map tile row number of starting point,
				- Map tile column number of target point,
				- Map tile row number of target point.
			Returns vector of pairs of numbers (column, row) - ordered map tiles.
		*/
		std::vector< std::pair< int, int > > *GetPath( int x1, int y1, int x2, int y2 );
		
		/*
			Gives a precomputed path from given waypoint to another one.
			Gets:
				- Map tile column number of starting waypoint,
				- Map tile row number of starting waypoint.
			Returns vector of pairs of numbers (column, row) - map tiles.
		*/
		std::vector< std::pair< int, int > > *GetRandomWaypointPath( int x, int y );
		
		/*
			Searches for a path to a randomly chosen waypoint.
			Gets:
				- Map tile column number,
				- Map tile row number.
			Returns vector of pairs of numbers (column, row) - map tiles.
		*/
		std::vector< std::pair< int, int > > *GetPathToRandomWaypoint( int x, int y );
	
	private:
		/*
			Calculates heuristic cost for movement between two given map tiles.
			Uses Euclidean metric.
			Gets:
				- Starting map tlle column number,
				- Starting map tile row number,
				- Target map tile column number,
				- Target map tile row number.
			Returns heuristic cost for movement between two given map tiles.
		*/
		int GetHeuristicCost( int x1, int y1, int x2, int y2 );
};

#endif // _MAP_H_
