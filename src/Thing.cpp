/*
	Author: Jan Tatarynowicz
*/

#include "Thing.h"
#include "Player.h"
#include "Def.h"
#include "Ray.h"

#include <cmath>

constexpr std::pair< float, float > Thing::PICKUP_SIZES[ NUM_PICKUPS ];

Thing::Thing(	enum THING_TYPES type,
				enum ENEMY_TYPES enemyType,
				enum PICKUPS pickup,
				Map *map,
				float x,
				float y,
				float dx,
				float dy,
				float speed,
				float angle,
				float width,
				float height )
{
	this->type = type;
	this->enemyType = enemyType;
	this->pickup = pickup;
	this->map = map;
	this->x = x;
	this->y = y;
	this->z = 0.0f;
	this->dx = dx;
	this->dy = dy;
	this->speed = speed;
	this->angle = angle;
	this->width = width;
	this->height = height;
	this->spriteWidth = width;
	this->spriteHeight = height;
	this->action = ACTIONS::ACTION_NONE;
	this->dispose = false;
	
	this->weapons[ Weapon::WEAPONS::SHOTGUN ] = new Weapon( Weapon::WEAPONS::SHOTGUN );
	this->weapons[ Weapon::WEAPONS::MACHINEGUN ] = new Weapon( Weapon::WEAPONS::MACHINEGUN );
	this->weapons[ Weapon::WEAPONS::PISTOL ] = new Weapon( Weapon::WEAPONS::PISTOL, true );
	this->weapons[ Weapon::WEAPONS::PLASMAGUN ] = new Weapon( Weapon::WEAPONS::PLASMAGUN );
	
	this->weapon = Weapon::WEAPONS::PISTOL;
	
	this->frags = 0;
}

bool Thing::Intersects( Ray *ray )
{
	std::pair< float, float > ropos = ray->GetOriginPosition();
	std::pair< float, float > rhpos = ray->GetHitPosition();
	float rox = std::get< 0 >( ropos ), roy = std::get< 1 >( ropos );
	float rhx = std::get< 0 >( rhpos ), rhy = std::get< 1 >( rhpos );
	
	// Checking if the ray even has chance to hit the thing
	// Here: if the thing is on the back of the ray's origin
	// Ray and thing vectors' angles
	float aray = atan2( rhy - roy, rhx - rox );
	float athing = atan2( this->y - roy, this->x - rox );
	
	if( abs( aray - athing ) * 180.0f / PI > 90.0f )
		return false;
	
	// More precise calculations
	// Twice the area of triangle
	float ta = abs(	this->x * ( roy - rhy )
					+ rox * ( rhy - this->y )
					+ rhx * ( this->y - roy )
	);
	
	// Height of the triangle is the point's distance to the ray
	// a = 1/2 * h * b so h = 2*a/b, where b is the ray's length
	float raydist = ray->GetDistance();
	
	float dist = ta / raydist;
	
	return dist <= this->width / 2;
}

bool Thing::Collides( Thing *thing )
{
	float radius = this->width / 2 + thing->GetWidth() / 2;
	
	auto pos = thing->GetPosition();
	float tx = std::get< 0 >( pos ), ty = std::get< 1 >( pos );
	
	float dist = sqrt( ( this->x - tx ) * ( this->x - tx ) + ( this->y - ty ) * ( this->y - ty ) );
	
	return dist <= radius;
}

int Thing::CheckCollision()
{
	int collides = 0;
	
	float rx = this->x + COLLISION_DISTANCE;
	float lx = this->x - COLLISION_DISTANCE;
	float uy = this->y - COLLISION_DISTANCE;
	float dy = this->y + COLLISION_DISTANCE;
	
	if( this->dy < 0.0f )
	{
		if
		(		this->map->Blocks( lx / GRID_SIZE, ( uy + this->dy ) / GRID_SIZE )
			||	this->map->Blocks( rx / GRID_SIZE, ( uy + this->dy ) / GRID_SIZE )
		)
		{
			this->dy = 0.0f;
			collides = 1;
		}
	}
	else if( this->dy > 0.0f )
	{
		if
		(		this->map->Blocks( lx / GRID_SIZE, ( dy + this->dy ) / GRID_SIZE )
			||	this->map->Blocks( rx / GRID_SIZE, ( dy + this->dy ) / GRID_SIZE )
		)
		{
			this->dy = 0.0f;
			collides = 1;
		}
	}
	
	if( this->dx < 0.0f )
	{
		if
		(		this->map->Blocks( ( lx + this->dx ) / GRID_SIZE, uy / GRID_SIZE )
			||	this->map->Blocks( ( lx + this->dx ) / GRID_SIZE, dy / GRID_SIZE )
		)
		{
			this->dx = 0.0f;
			collides = 1;
		}
	}
	else if( this->dx > 0.0f )
	{
		if
		(		this->map->Blocks( ( rx + this->dx ) / GRID_SIZE, uy / GRID_SIZE )
			||	this->map->Blocks( ( rx + this->dx ) / GRID_SIZE, dy / GRID_SIZE )
		)
		{
			this->dx = 0.0f;
			collides = 1;
		}
	}
	
	return collides;
}

void Thing::DealDamage( int damage )
{
	if( this->indestructible )
		return;
	
	this->health -= damage;
	
	if( this->health <= 0 )
	{
		this->speed = 0.0f;
	
		this->action = ACTIONS::ACTION_DIE;
		this->actionTime = SDL_GetTicks();
		
		this->deathTime = SDL_GetTicks();
	}
}

void Thing::PickUp()
{
	if( this->type != THING_TYPE_PICKUP )
		return;
	
	this->action = ACTION_RESPAWN;
	this->actionTime = SDL_GetTicks();
}

void Thing::HideWeapon( enum Weapon::WEAPONS weapon )
{
	if(	this->action == ACTIONS::ACTION_NONE
		|| this->action == ACTIONS::ACTION_WALK )
	{
		this->action = ACTIONS::ACTION_HIDE_WEAPON;
		this->actionTime = SDL_GetTicks();
		this->nextWeapon = weapon;
	}
}

void Thing::DrawWeapon()
{
	this->action = ACTIONS::ACTION_DRAW_WEAPON;
	this->actionTime = SDL_GetTicks();
	this->weapon = this->nextWeapon;
}

void Thing::Shoot()
{
	if( this->weapons[ this->weapon ]->GetAmmo() <= 0 )
		return;
	
	if( this->action == ACTIONS::ACTION_NONE || this->action == ACTIONS::ACTION_WALK )
	{
		this->shot = false;
		this->action = ACTIONS::ACTION_SHOOT;
		this->actionTime = SDL_GetTicks();
	}
}

void Thing::GiveAmmo( enum Weapon::WEAPONS weapon, int ammo )
{
	this->weapons[ weapon ]->GiveAmmo( ammo );
}

void Thing::GiveWeapon( enum Weapon::WEAPONS weapon )
{
	if( !this->weapons[ weapon ]->GotWeapon() )
	{
		this->weapons[ weapon ]->GiveWeapon();
	}
	else
	{
		GiveAmmo( weapon, Weapon::STARTING_AMMO[ weapon ] );
	}
}

void Thing::ResetWeapons()
{
	for( int i = 0; i < Weapon::WEAPONS::NUM_WEAPONS; i++ )
		this->weapons[ i ]->Reset();
	
	this->weapons[ Weapon::WEAPONS::PISTOL ]->GiveWeapon();
	
	this->weapon = Weapon::WEAPONS::PISTOL;
}

void Thing::Update( Player *player, Thing *observer, std::vector< Thing* > *things )
{
	if( this->action == ACTION_RESPAWN )
	{
		if( SDL_GetTicks() - this->actionTime >= RESPAWN_TIME )
		{
			this->action = ACTION_NONE;
			this->actionTime = SDL_GetTicks();
		}
	}
	
	auto pos = player->GetPosition();
	float px = std::get< 0 >( pos ), py = std::get< 1 >( pos );
	
	this->pdistance = sqrt(	( px - this->x )
							* ( px - this->x )
							+ ( py - this->y )
							* ( py - this->y )
	);
	
	auto opos = observer->GetPosition();
	float ox = std::get< 0 >( opos ), oy = std::get< 1 >( opos );
	
	this->cdistance = sqrt(	( ox - this->x )
							* ( ox - this->x )
							+ ( oy - this->y )
							* ( oy - this->y )
	);
	
	float relangle = atan2( ox - this->x, oy - this->y ) * 180.0f / PI ;
	if( relangle < 0.0f )
		relangle = 360.0f + relangle;
	else if( relangle >= 360.0f )
		relangle = 360.0f - relangle;
	
	this->relativeAngle = relangle;
	
	this->weapons[ this->weapon ]->Update();
}

Thing::~Thing()
{
	delete this->name;
	
	for( int i = 0; i < Weapon::WEAPONS::NUM_WEAPONS; i++ )
		delete this->weapons[ i ];
}
