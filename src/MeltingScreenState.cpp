/*
	Author: Jan Tatarynowicz
*/

#include "MeltingScreenState.h"
#include "Renderer.h"

MeltingScreenState::MeltingScreenState( GameEngine *game ) : GameState( game )
{
}

MeltingScreenState::~MeltingScreenState()
{
	if( this->startFrame != NULL )
		delete this->startFrame;
	if( this->endFrame != NULL )
		delete this->endFrame;
	
	delete this->columns;
	delete this->initial;
}

void MeltingScreenState::Init( Uint32 *startFrame, Uint32 *endFrame, int width, int height )
{
	this->startFrame = startFrame;
	this->endFrame = endFrame;
	this->width = width;
	this->height = height;
	
	this->startTime = SDL_GetTicks();
	
	this->columns = new int[ width ];
	this->initial = new int[ width ];
	
	int val = -( rand() % ( -MIN_VALUE ) );
	
	for( int i = 0; i < PIXELS_PER_COLUMN; i++ )
		this->initial[ i ] = val;
	
	int max = 0;
	for( int i = PIXELS_PER_COLUMN; i < width - PIXELS_PER_COLUMN + 1; i += PIXELS_PER_COLUMN )
	{
		val = initial[ i - PIXELS_PER_COLUMN ] + ( rand() % ( MAX_DEVIATION ) ) - MAX_DEVIATION / 2;
		for( int j = 0; j < PIXELS_PER_COLUMN; j++ )
			this->initial[ j + i ] = val;
		
		if( val > max )
			max = val;
	}
	
	if( max > 0 )
		for( int i = 0; i < this->width; i++ )
			this->initial[ i ] -= max;
}

void MeltingScreenState::HandleEvent( SDL_Event *event )
{
}

void MeltingScreenState::Update()
{
	Uint32 timePassed = SDL_GetTicks() - this->startTime;
	
	int min = this->height;
	for( int i = 0; i < this->width; i++ )
	{
		this->columns[ i ] = this->height * ( ( float ) timePassed / ( float ) MELTDOWN_DURATION ) + initial[ i ];
		
		if( this->columns[ i ] < min )
			min = this->columns[ i ];
	}
	
	if( min >= this->height )
		this->message = MESSAGE_POP;
}

void MeltingScreenState::HandleInput( Input *input )
{
}

void MeltingScreenState::Render( Renderer *renderer )
{
	renderer->Render( this );
}