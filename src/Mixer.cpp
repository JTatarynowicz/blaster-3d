/*
	Author: Jan Tatarynowicz
*/

#include "Mixer.h"

#include <stdio.h>

Mixer::Mixer()
{
}

void Mixer::Init( int soundVolume, int musicVolume )
{
	this->soundVolume = soundVolume;
	this->musicVolume = musicVolume;
	
	if( Mix_OpenAudio( FREQUENCY, MIX_DEFAULT_FORMAT, STEREO, SAMPLE_SIZE ) < 0 )
	{
		printf( "SDL_mixer could not initialize: %s\n", Mix_GetError() );
		exit( -1 );
	}
	
	LoadChunks();
}

void Mixer::Quit()
{
	Mix_FreeMusic( this->music );
	Mix_FreeChunk( this->shotBlasterChunk );
	Mix_FreeChunk( this->shotMachinegunChunk );
	Mix_FreeChunk( this->shotShotgunChunk );
	Mix_FreeChunk( this->shotPlasmagunChunk );
	Mix_FreeChunk( this->pickupChunk );
	Mix_FreeChunk( this->pickupGunChunk );
	
	Mix_Quit();
}

void Mixer::LoadMIDITrack( const char *t )
{
	this->music = Mix_LoadMUS( t );
	
	if( this->music == NULL )
	{
		printf( "Could not load track: %s\n", t );
	}
	
	Mix_VolumeMusic( this->musicVolume );
}

void Mixer::PlayMusic()
{
	if( this->music == NULL || this->musicVolume == 0 )
		return;
	
	Mix_PlayMusic( this->music, -1 );	// -1 stands for nearest channel available
}

void Mixer::PauseMusic()
{
	if( this->music == NULL )
		return;
}

void Mixer::StopMusic()
{
	if( this->music == NULL )
		return;
}

void Mixer::LoadChunks()
{
	this->shotBlasterChunk = Mix_LoadWAV( "data/sounds/shot_blaster.wav" );
	
	if( this->shotBlasterChunk == NULL )
	{
		printf( "Could not load chunk: %s\n", Mix_GetError() );
	}
	
	this->shotMachinegunChunk = Mix_LoadWAV( "data/sounds/shot_machinegun.wav" );
	
	if( this->shotMachinegunChunk == NULL )
	{
		printf( "Could not load chunk: %s\n", Mix_GetError() );
	}
	
	this->shotShotgunChunk = Mix_LoadWAV( "data/sounds/shot_shotgun.wav" );
	
	if( this->shotShotgunChunk == NULL )
	{
		printf( "Could not load chunk: %s\n", Mix_GetError() );
	}
	
	this->shotPlasmagunChunk = Mix_LoadWAV( "data/sounds/shot_plasmagun.wav" );
	
	if( this->shotPlasmagunChunk == NULL )
	{
		printf( "Could not load chunk: %s\n", Mix_GetError() );
	}
	
	this->pickupChunk = Mix_LoadWAV( "data/sounds/pickup.wav" );
	
	if( this->pickupChunk == NULL )
	{
		printf( "Could not load chunk: %s\n", Mix_GetError() );
	}
	
	this->pickupGunChunk = Mix_LoadWAV( "data/sounds/gunload.wav" );
	
	if( this->pickupGunChunk == NULL )
	{
		printf( "Could not load chunk: %s\n", Mix_GetError() );
	}
	
	Mix_VolumeChunk( this->shotBlasterChunk, this->soundVolume );
	Mix_VolumeChunk( this->shotMachinegunChunk, this->soundVolume );
	Mix_VolumeChunk( this->shotShotgunChunk, this->soundVolume );
	Mix_VolumeChunk( this->shotPlasmagunChunk, this->soundVolume );
	Mix_VolumeChunk( this->pickupChunk, this->soundVolume );
	Mix_VolumeChunk( this->pickupGunChunk, this->soundVolume );
}

void Mixer::PlaySound( enum Mixer::SOUNDS sound, float distance = 0.0f )
{
	if( distance >= MAX_DISTANCE || distance < 0.0f )
		return;
	
	int volume = ( ( MAX_DISTANCE - distance ) / MAX_DISTANCE ) * ( float ) this->soundVolume;
	
	switch( sound )
	{
		case SHOT_PISTOL:
			if( this->shotBlasterChunk != NULL )
			{
				Mix_VolumeChunk( this->shotBlasterChunk, volume );
				Mix_PlayChannel( -1, this->shotBlasterChunk, 0 );
			}
			break;
		
		case SHOT_MACHINEGUN:
			if( this->shotMachinegunChunk != NULL )
			{
				Mix_VolumeChunk( this->shotMachinegunChunk, volume );
				Mix_PlayChannel( -1, this->shotMachinegunChunk, 0 );
			}
			break;
		
		case SHOT_SHOTGUN:
			if( this->shotShotgunChunk != NULL )
			{
				Mix_VolumeChunk( this->shotShotgunChunk, volume );
				Mix_PlayChannel( -1, this->shotShotgunChunk, 0 );
			}
			break;
		
		case SHOT_PLASMAGUN:
			if( this->shotPlasmagunChunk != NULL )
			{
				Mix_VolumeChunk( this->shotPlasmagunChunk, volume );
				Mix_PlayChannel( -1, this->shotPlasmagunChunk, 0 );
			}
			break;
		
		case PICKUP:
			if( this->pickupChunk != NULL )
			{
				Mix_VolumeChunk( this->pickupChunk, volume );
				Mix_PlayChannel( -1, this->pickupChunk, 1 );
			}
			break;
		
		case PICKUP_WEAPON:
			if( this->pickupGunChunk != NULL )
			{
				Mix_VolumeChunk( this->pickupGunChunk, volume );
				Mix_PlayChannel( -1, this->pickupGunChunk, 0 );
			}
			break;
		
		default:
			break;
	}
}
