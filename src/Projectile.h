/*
	Implementation of projectiles - Things that move in given direction
	and hurt others, i.e. plasma shot with plasmagun.
	Author: Jan Tatarynowicz
*/

#ifndef _PROJECTILE_H_
#define _PROJECTILE_H_

#include "Def.h"
#include "Thing.h"

class Projectile : public Thing
{
	public:
		enum PROJECTILE_TYPES
		{
			PROJECTILE_TYPE_FLAME,
			
			NUM_PROJECTILE_TYPES
		};
		
		Projectile(	Map *map,
					enum PROJECTILE_TYPES type,
					float x,
					float y,
					float dx,
					float dy,
					float angle,
					float width,
					float height,
					Uint32 lifetime,
					int damage );
		
		~Projectile(){};
		
		/*
			Moves the projectile in given direction and decides
		*/
		void Update( Player *player, Thing *observer, std::vector< Thing* > *things );
		
		int CheckCollision();
		
		enum PROJECTILE_TYPES ProjectileType() { return this->projectileType; }
	
	private:
		enum PROJECTILE_TYPES projectileType;
		// Pointer to Thing which created this projectile
		Thing *originator;
		
		Uint32 lifetime;
};

#endif // _PROJECTILE_H_
