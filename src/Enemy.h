/*
	Provides methods for enemies' behaviour.
	Author: Jan Tatarynowicz
*/

#ifndef _ENEMY_H_
#define _ENEMY_H_

#include "Def.h"
#include "Thing.h"

class Enemy : public Thing
{
	// Befriended classes for Strategy pattern implementation
	friend class AIStrategy;
	friend class AIStrategyPatrol;
	
	public:
		Enemy(	enum THING_TYPES type,
				enum ENEMY_TYPES enemyType,
				enum PICKUPS pickup, Map *map,
				float x,
				float y,
				float angle,
				float width,
				float height );
		
		~Enemy();
		
		/*
			Updates enemy's current state: it's position, actions taken, strategy, etc,
			based on current level state.
			Gets:
				- A pointer to player,
				- A pointer to current observer,
				- A pointer to things which an enemy may interact with (i.e. shooting).
		*/
		void Update( Player *player, Thing *observer, std::vector< Thing* > *things );
		
		/*
			Expands enemy's arsenal with a new weapon
			or gives ammo if the weapon is already there.
			Gets a weapon code.
		*/
		void GiveWeapon( enum Weapon::WEAPONS weapon );
		
		/*
			Respawns an enemy at given position - resets health and weapons.
			Gets:
				- X position coordinate,
				- Y position coordinate,
				- An angle for enemy to take.
		*/
		void Respawn( float x, float y, float angle );
	
	private:
		/*
			Handles taken actions and sets new ones.
		*/
		void PerformAction();
		
		/*
			Checks whether there is a collision between an enemy and map's geometry (walls).
			Returns:
				- 1 if there is collision,
				- 0 otherwise.
		*/
		int CheckCollision();
		
		// Current enemy's strategy (Strategy pattern)
		AIStrategy *strategy = NULL;
		
		// Flag telling whether an enemy should move or not
		bool move;
};

#endif // _ENEMY_H_