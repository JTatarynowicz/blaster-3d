/*
	Provides methods for enemies' AI in patrolling mode.
	Patrolling enemies explore the map walking between
	waypoints and shooting other enemies when spotted.
	Author: Jan Tatarynowicz
*/

#ifndef _AI_STRATEGY_PATROL_H_
#define _AI_STRATEGY_PATROL_H_

#include "Def.h"
#include "AIStrategy.h"

class AIStrategyPatrol : public AIStrategy
{
	public:
		AIStrategyPatrol( Enemy *enemy );
		
		void Update( Player *player, std::vector< Thing* > *things );
	
	private:
		/*
			Sets enemy's path to follow.
			Gets:
				- A pointer to path - vector of pairs (column, row)
				which correspond to the map tiles,
				- A boolean flag telling whether delete or not the previous path.
				Precomputed paths between waypoints must not be deleted!
		*/
		void SetPath( std::vector< std::pair< int, int > > *path, bool deletePath );
		
		/*
			Sets enemy's current goal - position on the map (column, row)
			which an enemy will go to.
			Gets a pair (column, row) - map position.
		*/
		void SetGoal( std::pair< int, int > &newGoal );
		
		/*
			Sets moving angle of an enemy based on it's current goal.
		*/
		void Move();
		
		/*
			Selects target an enemy can shoot to.
			Gets:
				- A pointer to things an enemy can shoot to.
		*/
		void SelectTarget( std::vector< Thing* > *things );
		
		// Current target
		Thing *target = NULL;
		
		// Current path to the goal
		std::vector< std::pair< int, int > > *path = NULL;
		bool deletePath = false;
		// Current path's node
		int currentNode;
		
		// Last player's position
		// updated every path search call
		float px, py;
		
		bool pathReached = false;
		
		struct AIGoal
		{
			// Map coordinates
			float x, y;
			bool reached = true;
		} currentGoal;
};

#endif // _AI_STRATEGY_PATROL_H_