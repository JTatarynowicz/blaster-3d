/*
	Methods implementing all the level scene rendering and menu rendering.
	Author: Jan Tatarynowicz
*/

#ifndef _RENDERER_H_
#define _RENDERER_H_

#include "Def.h"
#include "MessageChannel.h"

#define NUM_WALL_TEXTURES 6
#define NUM_FLAT_TEXTURES 6
#define NUM_THING_TEXTURES 1

//#define FORCE_ASPECT_RATIO (800.0f/600.0f)//1//(4.0f/3.0f)//( 1280.0f / 1024.0f )

// Distance to projection plane, affects field of view
#define PLANE_DISTANCE 300//200//300 // this->screenHeight

#define TEXTURE_SIZE 64
#define SPRITE_SIZE 64

class Renderer
{
	public:
		Renderer();
		
		/*
			Initializes renderer, opens window, loads graphical resources and initializes font renderer.
			Gets:
				- Screen width in pixels,
				- Screen height in pixels,
				- Flag telling whether game plays in windowed/fullscreen mode,
				- Flag telling whether floors and ceilings are to be rendered.
		*/
		void Init( int width, int height, bool fullscreen, bool renderFlats, bool forceAspectRatio );
		
		/*
			Frees graphical resources and closes renderer.
		*/
		void Quit();
		
		/*
			Renders main menu.
			Gets pointer to menu state.
		*/
		void Render( MenuState *state );
		
		/*
			Renders level scene in level state.
			Gets pointer to level state.
		*/
		void Render( LevelState *state );
		
		/*
			Renders melting screen state.
			Gets pointer to melting screen state.
		*/
		void Render( MeltingScreenState *state );
		
		/*
			Renders hud element.
			Gets:
				- Pointer to texture surface to be rendered,
				- Position x coordinate,
				- Position y coordinate,
				- Flag telling whether x, y position is the center of texture.
		*/
		void RenderHudElement( SDL_Surface *surf, int x, int y, bool centered );
		
		void RenderFPS();
		
		/*
			Displays rendered buffer frame by switching buffers in double buffer routine.
		*/
		void SwapBuffers();
		
		void SetFPS( int fps, int millis )
		{
			this->fps = fps, this->millis = millis;
		}
		
		Uint32 *GetFrame();
	
	private:
		/*
			Loads texture surface from file.
			Gets path to the file.
		*/
		SDL_Surface *LoadTexture( const char *path );
		
		/*
			Renders screen column of level scene: wall and ceiling/floor fragment.
			Gets:
				- Number of screen column (from 0 to screen width),
				- Pointer to current (observed) player (may be enemy!),
				- Pointer to level tile map,
				- Reference to cast ray.
		*/
		void DrawColumn( int column, Thing *player, Map *map, Ray ray );
		
		/*
			Renders things (enemies, items, pickups, plants, etc.).
			Gets:
				- Poitner to thing to be rendered,
				- Pointer to current player (observer).
		*/
		void RenderThing( Thing *thing, Thing *player );
		
		/*
			Renders graphical effects of bullets hiting walls/enemies.
			Gets:
				- Pointer to puff (contains hit coordinates),
				- Pointer to current observer.
		*/
		void RenderPuff( Puff *puff, Thing *player );
		
		/*
			Renders weapon drawn by current observed player.
			Gets pointer to observer.
		*/
		void RenderWeapon( Thing *player );
		
		void RenderCrosshair();
		
		/*
			Renders HUD - health and ammo of observed player.
			Gets pointer to observer.
		*/
		void RenderHUD( Thing *player );
		
		/*
			Renders stats table - players' names and their kill count.
			Gets pointer to level state.
		*/
		void RenderStatsTable( LevelState *state );
		
		int screenWidth, screenHeight;
		bool fullscreen;
		// Tells whether floors and ceilings are to be rendered.
		// One may consider to disable floor/ceiling rendering
		// for better performance, especially with slower cpus.
		bool renderFlats;
		
		// Coefficient forcing desired resolution aspect ratio
		// (useful for fullscreen rendering)
		float forceAspectRatio = 1.0f;
		
		// For performance measures
		int millis = 0, fps = 0;
		
		/*
			Returns texture surface of given thing.
			Gets:
				- Pointer to thing,
				- Pointer to observer.
		*/
		SDL_Surface *GetThingSprite( Thing *thing, Thing *player );
		
		// Game window
		SDL_Window *window;
		
		SDL_Surface *screen;
		
		FontRenderer *fontRenderer;
		
		SDL_Surface *walltextures[ NUM_WALL_TEXTURES ];
		// Floor and ceiling textures
		SDL_Surface *flattextures[ NUM_FLAT_TEXTURES ];
		SDL_Surface *thingtextures[ NUM_THING_TEXTURES ];
		
		SDL_Surface *background;
		SDL_Surface *menubg;
		
		// Walls depth buffer
		float *zbuffer;
		
		struct WeaponSprites
		{
			int numFrames;
			int numWeaponFrames;
			int numShootFrames;
			int frameLength;
			
			float scale;
			
			// Current weaon bobbing state - weapon sprites bob while walking
			bool bobs;
			float vbob = 0.0f, vbob2 = 0.0f, hbob = 0.0f;
			
			Uint32 bobTime = SDL_GetTicks();
			
			int width, height;
			int vpos, hpos;
			int shift; // horizontal pos shift
			
			SDL_Surface **weaponSprites;
			SDL_Surface **shootSprites;
		} pistolSprites, shotgunSprites, machinegunSprites, plasmagunSprites;
		
		// Space marine sprites
		struct SmarineSprites
		{
			static const unsigned int NUM_DIRECTIONS = 8;
			
			static const unsigned int NUM_WALK_FRAMES = 4;
			static const unsigned int NUM_IDLE_FRAMES = 1;
			static const unsigned int NUM_DIE_FRAMES = 4;
			
			static const unsigned int WALK_FRAME_LENGTH = 150;
			static const unsigned int IDLE_FRAME_LENGTH = 150;
			static const unsigned int DIE_FRAME_LENGTH = 250;
			
			const float SPRITE_WIDTH = 0.3f;
			const float SPRITE_HEIGHT = 0.35f;
			
			SDL_Surface *walk[ NUM_DIRECTIONS ][ NUM_WALK_FRAMES ];
			SDL_Surface *idle[ NUM_DIRECTIONS ][ NUM_IDLE_FRAMES ];
			SDL_Surface *die[ 1 ][ NUM_DIE_FRAMES ];
		} smarineSprites;
		
		struct PlasmaSprites
		{
			static const unsigned int NUM_FRAMES = 4;
			static const unsigned int DURATION = 125;
			static const unsigned int FRAME_LENGTH = DURATION / NUM_FRAMES;
			
			SDL_Surface *sprites[ NUM_FRAMES ];
		} plasmaSprites;
		
		struct PickupSprites
		{
			SDL_Surface *bulletbox;
			SDL_Surface *shellbox;
			SDL_Surface *cells;
			SDL_Surface *smallmedkit;
			SDL_Surface *largemedkit;
			SDL_Surface *machinegun;
			SDL_Surface *shotgun;
			SDL_Surface *plasmagun;
		} pickupSprites;
		
		struct HudElements
		{
			SDL_Surface *plate1;
			SDL_Surface *plate2;
		} hudElements;
		
		struct HealthBar
		{
			static const unsigned int WIDTH = 16, HEIGHT = 150;
			static const unsigned int XPOS = 44;
			unsigned int YPOS = 0;
			static const unsigned int BORDER = 1;
			
			static const int MAX_HEALTH = 100;
			
			int health = 0;
			
			Uint32 data[ WIDTH * HEIGHT ];
		} healthBar;
		
		struct PuffSprites
		{
			static const unsigned int NUM_FRAMES = 4;
			static const unsigned int DURATION = 300;
			static const unsigned int FRAME_LENGTH = DURATION / NUM_FRAMES;
			
			#define PUFF_WIDTH 0.1f
			#define PUFF_HEIGHT 0.1f
			#define PUFF_SIZE 16
			
			SDL_Surface *sprites[ NUM_FRAMES ];
		} puffSprites, plasmaPuffSprites;
};

#endif // _RENDERER_H_
