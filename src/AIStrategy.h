/*
	An interface for artificial intelligence methods
	based on Strategy OOP pattern.
	Author: Jan Tatarynowicz
*/

#ifndef _AI_STRATEGY_H_
#define _AI_STRATEGY_H_

#include "Def.h"

class AIStrategy
{
	public:
		AIStrategy( Enemy *enemy ){ this->enemy = enemy; }
		virtual ~AIStrategy(){};
		
		/*
			Updates enemy's current strategy, goal, target.
			Gets:
				- A pointer to player an enemy may shoot to,
				- A pointer to things an enemy may shoot to.
		*/
		virtual void Update( Player *player, std::vector< Thing* > *things ){};
	
	protected:
		Enemy *enemy;
};

#endif // _AI_STRATEGY_H_